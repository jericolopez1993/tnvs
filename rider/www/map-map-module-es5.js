function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["map-map-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/map/map.page.html":
  /*!*************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/map/map.page.html ***!
    \*************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppMapMapPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"dark\">\n    <ion-searchbar [(ngModel)]=\"autocomplete.input\" (keyup)=\"updateSearchResults()\" placeholder=\"Search for a place\"></ion-searchbar>\n  </ion-toolbar>\n  <ion-list [hidden]=\"autocompleteItems.length == 0\">\n    <ion-item *ngFor=\"let item of autocompleteItems\" tappable (click)=\"selectSearchResult(item)\">\n      {{ item.description }}\n    </ion-item>\n  </ion-list>\n</ion-header>\n\n\n<ion-content>\n  <div #map id=\"map\"></div>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/map/map.module.ts":
  /*!***********************************!*\
    !*** ./src/app/map/map.module.ts ***!
    \***********************************/

  /*! exports provided: MapPageModule */

  /***/
  function srcAppMapMapModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MapPageModule", function () {
      return MapPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _map_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./map.page */
    "./src/app/map/map.page.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");

    var routes = [{
      path: '',
      component: _map_page__WEBPACK_IMPORTED_MODULE_6__["MapPage"]
    }];

    var MapPageModule = function MapPageModule() {
      _classCallCheck(this, MapPageModule);
    };

    MapPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_map_page__WEBPACK_IMPORTED_MODULE_6__["MapPage"]]
    })], MapPageModule);
    /***/
  },

  /***/
  "./src/app/map/map.page.scss":
  /*!***********************************!*\
    !*** ./src/app/map/map.page.scss ***!
    \***********************************/

  /*! exports provided: default */

  /***/
  function srcAppMapMapPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "#map {\n  height: 100%;\n}\n\n.marker {\n  position: fixed;\n  z-index: 1000;\n  top: 45%;\n  left: 45%;\n}\n\n.locationinput {\n  background: #eee;\n  border: 0;\n  outline: 0;\n  width: 100%;\n  padding: 0.5rem;\n  color: #333;\n  margin: 4px;\n  font-size: 14px;\n  border-radius: 4px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFwL0M6XFxVc2Vyc1xcSmVyaWNvIFBhdWxvXFxSdWJ5bWluZVByb2plY3RzXFx0bnZzXFxyaWRlci9zcmNcXGFwcFxcbWFwXFxtYXAucGFnZS5zY3NzIiwic3JjL2FwcC9tYXAvbWFwLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7QUNDSjs7QURFRTtFQUNFLGVBQUE7RUFDQSxhQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7QUNDSjs7QURFRTtFQUNFLGdCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9tYXAvbWFwLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNtYXAge1xuICAgIGhlaWdodDogMTAwJTtcbiAgfVxuXG4gIC5tYXJrZXIge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICB6LWluZGV4OiAxMDAwO1xuICAgIHRvcDo0NSU7XG4gICAgbGVmdDogNDUlOztcbiAgfVxuXG4gIC5sb2NhdGlvbmlucHV0e1xuICAgIGJhY2tncm91bmQ6ICNlZWU7XG4gICAgYm9yZGVyOjA7XG4gICAgb3V0bGluZTowO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmc6IDAuNXJlbTtcbiAgICBjb2xvcjogIzMzMztcbiAgICBtYXJnaW46IDRweDtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICB9IiwiI21hcCB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLm1hcmtlciB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgei1pbmRleDogMTAwMDtcbiAgdG9wOiA0NSU7XG4gIGxlZnQ6IDQ1JTtcbn1cblxuLmxvY2F0aW9uaW5wdXQge1xuICBiYWNrZ3JvdW5kOiAjZWVlO1xuICBib3JkZXI6IDA7XG4gIG91dGxpbmU6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAwLjVyZW07XG4gIGNvbG9yOiAjMzMzO1xuICBtYXJnaW46IDRweDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/map/map.page.ts":
  /*!*********************************!*\
    !*** ./src/app/map/map.page.ts ***!
    \*********************************/

  /*! exports provided: MapPage */

  /***/
  function srcAppMapMapPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MapPage", function () {
      return MapPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_place_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../services/place.service */
    "./src/app/services/place.service.ts");
    /* harmony import */


    var _services_trip_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../services/trip.service */
    "./src/app/services/trip.service.ts");
    /* harmony import */


    var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic-native/geolocation/ngx */
    "./node_modules/@ionic-native/geolocation/ngx/index.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var MapPage =
    /*#__PURE__*/
    function () {
      function MapPage(zone, router, geolocation, chRef, route, placeService, tripService) {
        _classCallCheck(this, MapPage);

        this.zone = zone;
        this.router = router;
        this.geolocation = geolocation;
        this.chRef = chRef;
        this.route = route;
        this.placeService = placeService;
        this.tripService = tripService;
        this.googleAutocomplete = new google.maps.places.AutocompleteService();
        this.autocomplete = {
          input: ''
        };
        this.autocompleteItems = [];
      }

      _createClass(MapPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {} // Load map only after view is initialized

      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          this.loadMap();
        }
      }, {
        key: "updateSearchResults",
        value: function updateSearchResults() {
          var _this = this;

          this.googleAutocomplete.getPlacePredictions({
            input: this.autocomplete.input,
            componentRestrictions: {
              country: "ph"
            }
          }, function (predictions, status) {
            _this.autocompleteItems = [];

            _this.zone.run(function () {
              predictions.forEach(function (prediction) {
                _this.autocompleteItems.push(prediction);
              });
            });
          });
        }
      }, {
        key: "selectSearchResult",
        value: function selectSearchResult(item) {
          console.log(item);
          this.location = item;
          this.placeid = this.location.place_id;
          console.log(this.findPlaceByID(this.placeid)); //   this.lat = this.googleAutocomplete.getPlace().geometry.location.lat();
          //   this.lng = this.googleAutocomplete.getPlace().geometry.location.lng();
          //   this.map.panTo(new google.maps.LatLng(this.lat, this.lng));
          //   this.findPlaceByID(new google.maps.LatLng(this.lat, this.lng));
          // console.log('placeid'+ this.placeid)
        }
      }, {
        key: "loadMap",
        value: function loadMap() {
          var _this2 = this;

          // set current location as map center
          this.geolocation.getCurrentPosition().then(function (resp) {
            var latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
            _this2.map = new google.maps.Map(document.getElementById('map'), {
              zoom: 16,
              center: latLng,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              mapTypeControl: false
            });
            _this2.marker = new google.maps.Marker({
              map: _this2.map,
              position: latLng
            });

            _this2.marker.setMap(_this2.map); // get center's address


            _this2.findPlace(latLng);

            _this2.map.addListener('center_changed', function (event) {
              var center = _this2.map.getCenter();

              _this2.findPlace(center);
            });
          }).catch(function (error) {
            console.log('Error getting location', error);
          }); // var nativeHomeInputBox = document.getElementById('searchbar');
          // var options = {
          //     componentRestrictions: {country: "ph"}
          // };
          //
          // this.googleAutocomplete = new google.maps.places.Autocomplete(nativeHomeInputBox, options);
          // google.maps.event.addListener(this.googleAutocomplete, 'place_changed', () => {
          //   this.lat = this.googleAutocomplete.getPlace().geometry.location.lat();
          //   this.lng = this.googleAutocomplete.getPlace().geometry.location.lng();
          //   this.map.panTo(new google.maps.LatLng(this.lat, this.lng));
          //   this.findPlace(new google.maps.LatLng(this.lat, this.lng));
          // });
        }
      }, {
        key: "findPlaceAndSelect",
        value: function findPlaceAndSelect(placeID) {
          var _this3 = this;

          var geocoder = new google.maps.Geocoder();
          this.marker.setMap(null);
          this.marker = new google.maps.Marker({
            map: this.map,
            placeId: placeID
          });
          this.marker.setMap(this.map);
          geocoder.geocode({
            'placeId': placeID
          }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              _this3.lat = results[0].geometry.location.lat();
              _this3.lng = results[0].geometry.location.lng();

              _this3.map.panTo(new google.maps.LatLng(_this3.lat, _this3.lng));

              _this3.address = results[0];

              _this3.chRef.detectChanges();
            }
          });
        } // find address by LatLng

      }, {
        key: "findPlace",
        value: function findPlace(latLng) {
          var _this4 = this;

          var geocoder = new google.maps.Geocoder();
          this.marker.setMap(null);
          this.marker = new google.maps.Marker({
            map: this.map,
            position: latLng
          });
          this.marker.setMap(this.map);
          geocoder.geocode({
            'latLng': latLng
          }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              _this4.address = results[0];

              _this4.chRef.detectChanges();
            }
          });
        }
      }, {
        key: "findPlaceByID",
        value: function findPlaceByID(placeID) {
          var _this5 = this;

          var geocoder = new google.maps.Geocoder();
          geocoder.geocode({
            'placeId': placeID
          }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              _this5.lat = results[0].geometry.location.lat();
              _this5.lng = results[0].geometry.location.lng();

              _this5.map.panTo(new google.maps.LatLng(_this5.lat, _this5.lng));

              _this5.findPlace(new google.maps.LatLng(_this5.lat, _this5.lng));
            }
          });
        } // choose address and go back to home page

      }, {
        key: "selectPlace",
        value: function selectPlace() {
          var _this6 = this;

          var address = this.placeService.formatAddress(this.address);
          this.route.queryParams.subscribe(function (data) {
            var type = data.type;

            if (type == 'origin') {
              _this6.tripService.setOrigin(address.vicinity, address.location.lat, address.location.lng);
            } else if (type == 'destination') {
              _this6.tripService.setDestination(address.vicinity, address.location.lat, address.location.lng);
            }

            _this6.router.navigateByUrl('home');
          });
        }
      }]);

      return MapPage;
    }();

    MapPage.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
      }, {
        type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__["Geolocation"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]
      }, {
        type: _services_place_service__WEBPACK_IMPORTED_MODULE_2__["PlaceService"]
      }, {
        type: _services_trip_service__WEBPACK_IMPORTED_MODULE_3__["TripService"]
      }];
    };

    MapPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-map',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./map.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/map/map.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./map.page.scss */
      "./src/app/map/map.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgZone"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__["Geolocation"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"], _services_place_service__WEBPACK_IMPORTED_MODULE_2__["PlaceService"], _services_trip_service__WEBPACK_IMPORTED_MODULE_3__["TripService"]])], MapPage);
    /***/
  }
}]);
//# sourceMappingURL=map-map-module-es5.js.map