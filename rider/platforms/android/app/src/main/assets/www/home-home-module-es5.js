function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
  /*!***************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
    \***************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppHomeHomePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Home</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <div class=\"locationinput-holder\">\n    <input class=\"locationinput\" (click)=\"chooseOrigin()\" placeholder=\"Where do you want to pickup?\" type=\"text\"\n      value=\"{{origin ? origin.vicinity : '' }}\">\n    <input class=\"locationinput\" (click)=\"chooseDestination()\" placeholder=\"Where do you want to drop?\" type=\"text\"\n      value=\"{{ destination ? destination.vicinity : '' }}\">\n  </div>\n\n  <div id=\"{{ mapId }}\" [ngStyle]=\"{height: '100%'}\"></div>\n\n  <div class=\"align-bottom\">\n\n    <p class=\"distanceText\">\n      <span *ngIf=\"distanceText!=''\">Distance: {{ distanceText }}</span>\n      <span *ngIf=\"durationText!=''\">&nbsp; Duration: {{durationText}}</span>\n    </p>\n    <!--<ion-row [hidden]=\"!destination\">-->\n      <!--<ion-col (click)=\"choosePaymentMethod1()\">-->\n        <!--<ion-icon name=\"card\" color=\"gray\"></ion-icon>-->\n        <!--<span ion-text color=\"gray\">{{ getPaymentMethod() }}</span>-->\n      <!--</ion-col>-->\n      <!--<ion-col (click)=\"showPromoPopup()\" *ngIf=\"destination\">-->\n        <!--<ion-icon name=\"create\" color=\"gray\"></ion-icon>-->\n        <!--<span ion-text color=\"gray\">{{'PROMO' | translate}}</span>-->\n      <!--</ion-col>-->\n      <!--<ion-col (click)=\"showNotePopup()\">-->\n        <!--<ion-icon name=\"create\" color=\"gray\"></ion-icon>-->\n        <!--<span ion-text color=\"gray\">{{'NOTE' | translate}}</span>-->\n      <!--</ion-col>-->\n    <!--</ion-row>-->\n    <ion-row [hidden]=\"!destination\">\n      <ion-list lines=\"full\" class=\"ion-no-margin ion-no-padding\">\n        <ion-item>\n          <ion-input type=\"text\" [(ngModel)]=\"locationDetails\" placeholder=\"Location Details\" (click)=\"editLocationDetails()\" class=\"form-information-inputs\" readonly></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-input type=\"text\" [(ngModel)]=\"recipientName\"  placeholder=\"Recipient Name\" (click)=\"editRecipientName()\" class=\"form-information-inputs\" readonly></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-input type=\"text\" [(ngModel)]=\"mobileNumber\"  placeholder=\"Mobile Number\" (click)=\"editMobileNumber()\" class=\"form-information-inputs\" readonly></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-input type=\"text\" [(ngModel)]=\"itemCategory\"  placeholder=\"Type of Item\" (click)=\"editItemCategory()\" class=\"form-information-inputs\" readonly></ion-input>\n        </ion-item>\n      </ion-list>\n    </ion-row>\n\n    <ion-row [hidden]=\"!destination\">\n\n      <ion-col *ngFor=\"let vehicle of vehicles; let i = index\" [ngClass]=\"{'active': vehicle.active}\"\n        (click)=\"chooseVehicle(i)\">\n        <img src=\"{{ vehicle.icon }}\">\n        <p>{{ vehicle.name }}</p>\n        <p>{{currency }}{{ vehicle.fee }}</p>\n      </ion-col>\n\n    </ion-row>\n\n\n    <ion-button expand=\"block\" color=\"dark\" [hidden]=\"destination\" (click)=\"chooseDestination()\">\n      BOOK NOW</ion-button>\n    <ion-button expand=\"block\" color=\"dark\" [hidden]=\"!destination\" (click)=\"book()\">\n      {{ locateDriver == false ? 'BOOK NOW':'Locating Drivers'}} <ion-spinner name=\"dots\" color=\"light\"\n        [hidden]=\"!locateDriver\"></ion-spinner>\n    </ion-button>\n    <ion-button expand=\"block\" color=\"danger\" [hidden]=\"!locateDriver\" (click)=\"cancelBook()\">\n      Cancel Book\n    </ion-button>\n  </div>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/home/home.module.ts":
  /*!*************************************!*\
    !*** ./src/app/home/home.module.ts ***!
    \*************************************/

  /*! exports provided: HomePageModule */

  /***/
  function srcAppHomeHomeModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePageModule", function () {
      return HomePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./home.page */
    "./src/app/home/home.page.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");

    var HomePageModule = function HomePageModule() {
      _classCallCheck(this, HomePageModule);
    };

    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([{
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
      }])],
      declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })], HomePageModule);
    /***/
  },

  /***/
  "./src/app/home/home.page.scss":
  /*!*************************************!*\
    !*** ./src/app/home/home.page.scss ***!
    \*************************************/

  /*! exports provided: default */

  /***/
  function srcAppHomeHomePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".locationinput-holder {\n  padding: 0.2rem 1rem;\n  position: absolute;\n  background: transparent;\n  z-index: 999999;\n  top: 0;\n  width: 100%;\n}\n\n.locationinput {\n  background: #fff;\n  border: 1px solid #ccc;\n  outline: 0;\n  width: 100%;\n  padding: 0.5rem;\n  color: #333;\n  margin: 2px 0px;\n  font-size: 14px;\n  border-radius: 4px;\n}\n\n.distanceText {\n  font-size: 14px;\n  color: #555;\n  margin: 0;\n}\n\n.align-bottom {\n  position: fixed;\n  bottom: 0;\n  width: 100%;\n  padding: 5px;\n}\n\n.align-bottom p {\n  font-size: 14p;\n}\n\n.header-md:after {\n  background-image: none;\n}\n\nion-col {\n  text-align: center;\n}\n\nion-col img {\n  width: 40px;\n  padding: 5px;\n  border-radius: 100%;\n}\n\nion-col p {\n  margin: 0;\n}\n\nion-row {\n  background: #fff;\n  border-radius: 3px;\n  border: 1px solid #eee;\n}\n\n.list-md {\n  margin: -1px 0 0px;\n}\n\n.label-md {\n  margin: 13px -50px 13px 0;\n}\n\n.text-input-md {\n  font-size: 14px;\n  margin: 8px 5px;\n}\n\n#map {\n  width: 100%;\n}\n\n.active img {\n  background: #ffce00;\n}\n\nion-list {\n  width: 100%;\n}\n\nion-input {\n  width: 100%;\n}\n\n.form-information-inputs {\n  background: transparent !important;\n  border: none !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9DOlxcVXNlcnNcXEplcmljbyBQYXVsb1xcUnVieW1pbmVQcm9qZWN0c1xcdG52c1xccmlkZXIvc3JjXFxhcHBcXGhvbWVcXGhvbWUucGFnZS5zY3NzIiwic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usb0JBQUE7RUFDQSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0FDQ0Y7O0FEQ0E7RUFDRSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNFRjs7QURDQTtFQUNFLGVBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtBQ0VGOztBREFBO0VBQ0UsZUFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ0dGOztBREFBO0VBQ0UsY0FBQTtBQ0dGOztBRERBO0VBQ0Usc0JBQUE7QUNJRjs7QURGQTtFQUNFLGtCQUFBO0FDS0Y7O0FESEE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FDTUY7O0FESkE7RUFBWSxTQUFBO0FDUVo7O0FEUEE7RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7QUNVRjs7QURSQTtFQUNFLGtCQUFBO0FDV0Y7O0FEVEE7RUFDRSx5QkFBQTtBQ1lGOztBRFZBO0VBQ0UsZUFBQTtFQUNBLGVBQUE7QUNhRjs7QURYQTtFQUNFLFdBQUE7QUNjRjs7QURYQTtFQUNFLG1CQUFBO0FDY0Y7O0FEWkE7RUFDRSxXQUFBO0FDZUY7O0FEWkE7RUFDRSxXQUFBO0FDZUY7O0FEWkE7RUFDRSxrQ0FBQTtFQUNBLHVCQUFBO0FDZUYiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvY2F0aW9uaW5wdXQtaG9sZGVye1xuICBwYWRkaW5nOiAwLjJyZW0gMXJlbTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgei1pbmRleDogOTk5OTk5O1xuICB0b3A6MDtcbiAgd2lkdGg6MTAwJTtcbn1cbi5sb2NhdGlvbmlucHV0e1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBib3JkZXI6MXB4IHNvbGlkICNjY2M7XG4gIG91dGxpbmU6MDtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDAuNXJlbTtcbiAgY29sb3I6ICMzMzM7XG4gIG1hcmdpbjogMnB4IDBweDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG59XG5cbi5kaXN0YW5jZVRleHR7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICM1NTU7XG4gIG1hcmdpbjogMDtcbn1cbi5hbGlnbi1ib3R0b20ge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGJvdHRvbTogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDVweDtcbn1cblxuLmFsaWduLWJvdHRvbSBwe1xuICBmb250LXNpemU6IDE0cFxufVxuLmhlYWRlci1tZDphZnRlcntcbiAgYmFja2dyb3VuZC1pbWFnZTogbm9uZTtcbn1cbmlvbi1jb2x7XG4gIHRleHQtYWxpZ246IGNlbnRlclxufVxuaW9uLWNvbCBpbWd7XG4gIHdpZHRoOiA0MHB4O1xuICBwYWRkaW5nOiA1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG59XG5pb24tY29sIHAgeyBtYXJnaW46IDB9XG5pb24tcm93e1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlZWU7XG59XG4ubGlzdC1tZCB7XG4gIG1hcmdpbjogLTFweCAwIDBweDtcbn1cbi5sYWJlbC1tZHtcbiAgbWFyZ2luOiAxM3B4IC01MHB4IDEzcHggMDtcbn1cbi50ZXh0LWlucHV0LW1ke1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbjogOHB4IDVweDtcbn1cbiNtYXAge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmFjdGl2ZSBpbWd7XG4gIGJhY2tncm91bmQ6ICNmZmNlMDA7XG59XG5pb24tbGlzdCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5pb24taW5wdXQge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmZvcm0taW5mb3JtYXRpb24taW5wdXRzIHtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbiAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG59IiwiLmxvY2F0aW9uaW5wdXQtaG9sZGVyIHtcbiAgcGFkZGluZzogMC4ycmVtIDFyZW07XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIHotaW5kZXg6IDk5OTk5OTtcbiAgdG9wOiAwO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmxvY2F0aW9uaW5wdXQge1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBvdXRsaW5lOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMC41cmVtO1xuICBjb2xvcjogIzMzMztcbiAgbWFyZ2luOiAycHggMHB4O1xuICBmb250LXNpemU6IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbn1cblxuLmRpc3RhbmNlVGV4dCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICM1NTU7XG4gIG1hcmdpbjogMDtcbn1cblxuLmFsaWduLWJvdHRvbSB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgYm90dG9tOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogNXB4O1xufVxuXG4uYWxpZ24tYm90dG9tIHAge1xuICBmb250LXNpemU6IDE0cDtcbn1cblxuLmhlYWRlci1tZDphZnRlciB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IG5vbmU7XG59XG5cbmlvbi1jb2wge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbmlvbi1jb2wgaW1nIHtcbiAgd2lkdGg6IDQwcHg7XG4gIHBhZGRpbmc6IDVweDtcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcbn1cblxuaW9uLWNvbCBwIHtcbiAgbWFyZ2luOiAwO1xufVxuXG5pb24tcm93IHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZWVlO1xufVxuXG4ubGlzdC1tZCB7XG4gIG1hcmdpbjogLTFweCAwIDBweDtcbn1cblxuLmxhYmVsLW1kIHtcbiAgbWFyZ2luOiAxM3B4IC01MHB4IDEzcHggMDtcbn1cblxuLnRleHQtaW5wdXQtbWQge1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbjogOHB4IDVweDtcbn1cblxuI21hcCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uYWN0aXZlIGltZyB7XG4gIGJhY2tncm91bmQ6ICNmZmNlMDA7XG59XG5cbmlvbi1saXN0IHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbmlvbi1pbnB1dCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uZm9ybS1pbmZvcm1hdGlvbi1pbnB1dHMge1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xuICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/home/home.page.ts":
  /*!***********************************!*\
    !*** ./src/app/home/home.page.ts ***!
    \***********************************/

  /*! exports provided: HomePage */

  /***/
  function srcAppHomeHomePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePage", function () {
      return HomePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_place_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../services/place.service */
    "./src/app/services/place.service.ts");
    /* harmony import */


    var _services_setting_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../services/setting.service */
    "./src/app/services/setting.service.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var _services_deal_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../services/deal.service */
    "./src/app/services/deal.service.ts");
    /* harmony import */


    var _services_driver_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../services/driver.service */
    "./src/app/services/driver.service.ts");
    /* harmony import */


    var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/fire/auth */
    "./node_modules/@angular/fire/auth/es2015/index.js");
    /* harmony import */


    var _services_trip_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../services/trip.service */
    "./src/app/services/trip.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @ionic-native/geolocation/ngx */
    "./node_modules/@ionic-native/geolocation/ngx/index.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! src/environments/environment.prod */
    "./src/environments/environment.prod.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _services_common_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ../services/common.service */
    "./src/app/services/common.service.ts");
    /* harmony import */


    var firebase__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! firebase */
    "./node_modules/firebase/dist/index.cjs.js");
    /* harmony import */


    var firebase__WEBPACK_IMPORTED_MODULE_16___default =
    /*#__PURE__*/
    __webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_16__);

    var HomePage =
    /*#__PURE__*/
    function () {
      function HomePage(router, alertCtrl, placeService, geolocation, chRef, settingService, tripService, driverService, afAuth, authService, translate, dealService, common, menuCtrl) {
        _classCallCheck(this, HomePage);

        this.router = router;
        this.alertCtrl = alertCtrl;
        this.placeService = placeService;
        this.geolocation = geolocation;
        this.chRef = chRef;
        this.settingService = settingService;
        this.tripService = tripService;
        this.driverService = driverService;
        this.afAuth = afAuth;
        this.authService = authService;
        this.translate = translate;
        this.dealService = dealService;
        this.common = common;
        this.menuCtrl = menuCtrl;
        this.mapId = Math.random() + 'map';
        this.mapHeight = 480;
        this.showModalBg = false;
        this.showVehicles = false;
        this.vehicles = [];
        this.note = '';
        this.promocode = '';
        this.distance = 0;
        this.duration = 0;
        this.paymentMethod = 'cash';
        this.activeDrivers = [];
        this.driverMarkers = [];
        this.locateDriver = false;
        this.user = {};
        this.isTrackDriverEnabled = true;
        this.discount = 0;
        this.distanceText = '';
        this.durationText = '';
      }

      _createClass(HomePage, [{
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          var _this = this;

          // this.common.showLoader('Loading...');
          this.menuCtrl.enable(true);
          this.afAuth.authState.subscribe(function (authData) {
            if (authData) {
              _this.user = _this.authService.getUserData();
            }
          });
          this.origin = this.tripService.getOrigin();
          this.destination = this.tripService.getDestination();
          this.loadMap(); // this.common.hideLoader();
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          console.log("calling");
        }
      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          clearInterval(this.driverTracking);
        } // get current payment method from service

      }, {
        key: "getPaymentMethod",
        value: function getPaymentMethod() {
          this.paymentMethod = this.tripService.getPaymentMethod();
          return this.paymentMethod;
        }
      }, {
        key: "choosePaymentMethod1",
        value: function choosePaymentMethod1() {
          var _this2 = this;

          this.alertCtrl.create({
            header: "Choose Payments",
            inputs: [{
              type: 'radio',
              label: "Cash",
              value: 'cash'
            }, {
              type: 'radio',
              label: "Card",
              value: 'card'
            }],
            buttons: [{
              text: "Cancel"
            }, {
              text: "Choose",
              handler: function handler(data) {
                if (data == 'card') {
                  _this2.authService.getCardSetting().valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["take"])(1)).subscribe(function (res) {
                    if (res != null) {
                      _this2.tripService.setPaymentMethod(data);

                      _this2.paymentMethod = data;
                      var exp = res.exp.split('/');
                      Stripe.card.createToken({
                        number: res.number,
                        exp_month: exp[0],
                        exp_year: exp[1],
                        cvc: res.cvv
                      }, function (status, response) {
                        if (status == 200) {
                          console.log("Card Ready");

                          _this2.authService.updateCardSetting(res.number, res.exp, res.cvv, response.id);
                        } else {
                          _this2.common.showToast(response.error.message);
                        }
                      });
                    } else _this2.common.showAlert("Invalid Card");
                  });
                } else if (data == 'cash') {
                  _this2.paymentMethod = data;

                  _this2.tripService.setPaymentMethod(data);
                }
              }
            }]
          }).then(function (res) {
            return res.present();
          });
        } // toggle active vehicle

      }, {
        key: "chooseVehicle",
        value: function chooseVehicle(index) {
          for (var i = 0; i < this.vehicles.length; i++) {
            this.vehicles[i].active = i == index; // choose this vehicle type

            if (i == index) {
              this.tripService.setVehicle(this.vehicles[i]);
              this.currentVehicle = this.vehicles[i];
            }
          } // start tracking new driver type


          this.trackDrivers();
          this.toggleVehicles();
        }
      }, {
        key: "loadMap",
        value: function loadMap() {
          var _this3 = this;

          // this.common.showLoader("Loading..");
          // get current location
          return this.geolocation.getCurrentPosition().then(function (resp) {
            if (_this3.origin) _this3.startLatLng = new google.maps.LatLng(_this3.origin.location.lat, _this3.origin.location.lng);else _this3.startLatLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
            var directionsDisplay;
            var directionsService = new google.maps.DirectionsService();
            directionsDisplay = new google.maps.DirectionsRenderer();
            _this3.map = new google.maps.Map(document.getElementById(_this3.mapId), {
              zoom: 15,
              center: _this3.startLatLng,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              mapTypeControl: false,
              zoomControl: false,
              streetViewControl: false,
              fullscreenControl: false
            });
            var mapx = _this3.map;
            directionsDisplay.setMap(mapx); // find map center address

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
              'latLng': _this3.map.getCenter()
            }, function (results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                if (!_this3.origin) {
                  // set map center as origin
                  _this3.origin = _this3.placeService.formatAddress(results[0]);

                  _this3.tripService.setOrigin(_this3.origin.vicinity, _this3.origin.location.lat, _this3.origin.location.lng);

                  _this3.setOrigin();

                  _this3.chRef.detectChanges();
                } else {
                  _this3.setOrigin();
                } // save locality


                var locality = _this3.placeService.setLocalityFromGeocoder(results);

                console.log('locality', locality); // load list vehicles

                _this3.settingService.getPrices().valueChanges().subscribe(function (snapshot) {
                  _this3.vehicles = [];
                  var obj = snapshot[locality] ? snapshot[locality] : snapshot.default;
                  _this3.currency = obj.currency;

                  _this3.tripService.setCurrency(_this3.currency); // calculate price


                  Object.keys(obj.vehicles).forEach(function (id) {
                    obj.vehicles[id].id = id;

                    _this3.vehicles.push(obj.vehicles[id]);
                  }); // calculate distance between origin adn destination

                  if (_this3.destination) {
                    directionsService.route(request, function (result, status) {
                      if (status == google.maps.DirectionsStatus.OK && result.routes.length != 0) {
                        _this3.distance = result.routes[0].legs[0].distance.value;
                        _this3.distanceText = result.routes[0].legs[0].distance.text;
                        _this3.durationText = result.routes[0].legs[0].duration.text;

                        for (var i = 0; i < _this3.vehicles.length; i++) {
                          var currentDay = new Date(); // var nextDay = new Date(Date.now()+(1*24*60*60*1000));

                          var startTime1 = new Date(currentDay.getFullYear(), currentDay.getMonth(), currentDay.getDate(), 21).getTime();
                          var endTime1 = new Date(currentDay.getFullYear(), currentDay.getMonth(), currentDay.getDate(), 23, 59, 60).getTime();
                          var startTime2 = new Date(currentDay.getFullYear(), currentDay.getMonth(), currentDay.getDate(), 0).getTime();
                          var endTime2 = new Date(currentDay.getFullYear(), currentDay.getMonth(), currentDay.getDate(), 4, 59, 60).getTime();
                          _this3.vehicles[i].fee = _this3.distance * _this3.vehicles[i].price / 1000 + _this3.vehicles[i].first_fee;

                          if (_this3.distance >= 15000 && _this3.distance <= 24999) {
                            _this3.vehicles[i].fee = _this3.vehicles[i].fee + _this3.vehicles[i].second_fee;
                          } else if (_this3.distance >= 25000 && _this3.distance <= 49999) {
                            _this3.vehicles[i].fee = _this3.vehicles[i].fee + _this3.vehicles[i].third_fee;
                          } else if (_this3.distance >= 50000) {
                            _this3.vehicles[i].fee = _this3.vehicles[i].fee + _this3.vehicles[i].fourth_fee;
                          }

                          if (currentDay.getTime() >= startTime1 && currentDay.getTime() <= endTime1 || currentDay.getTime() >= startTime2 && currentDay.getTime() <= endTime2) {
                            _this3.vehicles[i].fee = _this3.vehicles[i].fee + _this3.vehicles[i].night_fee;
                          }

                          _this3.vehicles[i].fee = Math.floor(_this3.vehicles[i].fee);
                        }
                      } else {
                        console.log("error");
                      }
                    });
                  } // set first device as default


                  _this3.vehicles[0].active = true;
                  _this3.currentVehicle = _this3.vehicles[0];
                  _this3.locality = locality;
                  if (_this3.isTrackDriverEnabled) _this3.trackDrivers();
                });
              }
            }); // add destination to map

            if (_this3.destination) {
              _this3.destLatLng = new google.maps.LatLng(_this3.destination.location.lat, _this3.destination.location.lng);
              var bounds = new google.maps.LatLngBounds();
              bounds.extend(_this3.startLatLng);
              bounds.extend(_this3.destLatLng);
              mapx.fitBounds(bounds);
              var request = {
                origin: _this3.startLatLng,
                destination: _this3.destLatLng,
                travelMode: google.maps.TravelMode.DRIVING
              };
              directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                  directionsDisplay.setDirections(response);
                  directionsDisplay.setMap(mapx);
                } else {
                  console.log("error");
                }
              });
            } // this.common.hideLoader();

          }).catch(function (error) {
            // this.common.hideLoader();
            console.log('Error getting location', error);
          });
        }
      }, {
        key: "showPromoPopup",
        value: function showPromoPopup() {
          var _this4 = this;

          this.alertCtrl.create({
            header: 'Enter Promo code',
            message: "",
            inputs: [{
              name: 'promocode',
              placeholder: 'Enter Promo Code'
            }],
            buttons: [{
              text: 'Cancel',
              handler: function handler(data) {
                console.log('Cancel clicked');
              }
            }, {
              text: 'Apply',
              handler: function handler(data) {
                //verifying promocode
                firebase__WEBPACK_IMPORTED_MODULE_16__["database"]().ref('promocodes').orderByChild("code").equalTo(data.promocode).once('value', function (promocodes) {
                  var tmp = [];
                  promocodes.forEach(function (promo) {
                    tmp.push(Object.assign({
                      key: promo.key
                    }, promo.val()));
                    return false;
                  });
                  tmp = tmp[0];

                  if (promocodes.val() != null || promocodes.val() != undefined) {
                    _this4.promocode = tmp.code;
                    _this4.discount = tmp.discount;

                    _this4.tripService.setPromo(tmp.code);

                    _this4.tripService.setDiscount(tmp.discount);
                  }
                }, function (err) {
                  return console.log(err);
                });
              }
            }]
          }).then(function (prompt) {
            return prompt.present();
          });
        } // Show note popup when click to 'Notes to user'

      }, {
        key: "showNotePopup",
        value: function showNotePopup() {
          var _this5 = this;

          this.alertCtrl.create({
            header: 'Notes to user',
            message: "",
            inputs: [{
              name: 'note',
              placeholder: 'Note'
            }],
            buttons: [{
              text: 'Cancel'
            }, {
              text: 'Save',
              handler: function handler(data) {
                _this5.note = data;

                _this5.tripService.setNote(data);

                console.log('Saved clicked');
              }
            }]
          }).then(function (prompt) {
            return prompt.present();
          });
        }
      }, {
        key: "book",
        // go to next view when the 'Book' button is clicked
        value: function book() {
          this.locateDriver = true; // store detail

          this.tripService.setAvailableDrivers(this.activeDrivers);
          this.tripService.setDistance(this.distance);
          this.tripService.setFee(this.currentVehicle.fee);
          this.tripService.setIcon(this.currentVehicle.icon);
          this.tripService.setNote(this.note);
          this.tripService.setPromo(this.promocode);
          this.tripService.setDiscount(this.discount);
          this.tripService.setLocationDetails(this.locationDetails);
          this.tripService.setRecipientName(this.recipientName);
          this.tripService.setMobileNumber(this.mobileNumber);
          this.tripService.setItemCategory(this.itemCategory); // this.tripService.setPaymentMethod('');

          this.drivers = this.tripService.getAvailableDrivers(); // sort by driver distance and rating

          this.drivers = this.dealService.sortDriversList(this.drivers);

          if (this.drivers && this.locationDetails && this.recipientName && this.mobileNumber && this.itemCategory) {
            this.retryDeal(0);
          } else {
            this.locateDriver = false;
            this.common.showAlert("Please input the textboxes. For Driver to understand more about the delivery.");
          }
        }
      }, {
        key: "cancelBook",
        value: function cancelBook() {
          var driver = this.drivers[0];

          if (driver) {
            this.dealService.removeDeal(driver.key);
          }

          this.locateDriver = false;
        }
      }, {
        key: "sleep",
        value: function sleep(ms) {
          return new Promise(function (resolve) {
            return setTimeout(resolve, ms);
          });
        }
      }, {
        key: "retryDeal",
        value: function retryDeal(index) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee() {
            var i;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    i = 0;

                  case 1:
                    this.makeDeal(index);
                    i = i + 1;
                    _context.next = 5;
                    return this.sleep(5000);

                  case 5:
                    if (!(this.drivers[index] && this.dealAccepted)) {
                      _context.next = 7;
                      break;
                    }

                    return _context.abrupt("break", 11);

                  case 7:
                    if (this.locateDriver) {
                      _context.next = 9;
                      break;
                    }

                    return _context.abrupt("break", 11);

                  case 9:
                    if (i == 5) {
                      this.locateDriver = false;
                      this.common.showAlert("No Driver Found");
                    }

                  case 10:
                    if (i < 5) {
                      _context.next = 1;
                      break;
                    }

                  case 11:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "makeDeal",
        value: function makeDeal(index) {
          var _this6 = this;

          var driver = this.drivers[index];
          this.dealAccepted = false;

          if (driver) {
            driver.status = 'Bidding';
            this.dealService.getDriverDeal(driver.key).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["take"])(1)).subscribe(function (snapshot) {
              // if user is available
              if (snapshot == null) {
                // create a record
                _this6.dealService.makeDeal(driver.key, _this6.tripService.getOrigin(), _this6.tripService.getDestination(), _this6.tripService.getDistance(), _this6.tripService.getFee(), _this6.tripService.getCurrency(), _this6.tripService.getNote(), _this6.tripService.getPaymentMethod(), _this6.tripService.getPromo(), _this6.tripService.getDiscount(), _this6.tripService.getLocationDetails(), _this6.tripService.getRecipientName(), _this6.tripService.getMobileNumber(), _this6.tripService.getItemCategory()).then(function () {
                  var sub = _this6.dealService.getDriverDeal(driver.key).valueChanges().subscribe(function (snap) {
                    // if record doesn't exist or is accepted
                    if (snap === null || snap.status != src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__["DEAL_STATUS_PENDING"]) {
                      sub.unsubscribe(); // if deal has been cancelled

                      if (snap === null) {
                        _this6.nextDriver(index);
                      } else if (snap.status == src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__["DEAL_STATUS_ACCEPTED"]) {
                        // if deal is accepted
                        _this6.dealAccepted = true;
                        _this6.drivers = [];

                        _this6.tripService.setId(snap.tripId); // go to user page


                        _this6.locateDriver = false;

                        _this6.router.navigateByUrl('tracking');
                      }
                    }
                  });
                });
              } else {
                _this6.nextDriver(index);
              }
            });
          }
        } // make deal to next driver

      }, {
        key: "nextDriver",
        value: function nextDriver(index) {
          this.drivers.splice(index, 1);
          this.makeDeal(index);
        } // choose origin place

      }, {
        key: "chooseOrigin",
        value: function chooseOrigin() {
          this.router.navigate(['map'], {
            queryParams: {
              type: 'origin'
            }
          });
        } // choose destination place

      }, {
        key: "chooseDestination",
        value: function chooseDestination() {
          this.router.navigate(['map'], {
            queryParams: {
              type: 'destination'
            }
          });
        }
      }, {
        key: "choosePaymentMethod",
        value: function choosePaymentMethod() {
          this.router.navigateByUrl('payments');
        } // add origin marker to map

      }, {
        key: "setOrigin",
        value: function setOrigin() {
          // add origin and destination marker
          var latLng = new google.maps.LatLng(this.origin.location.lat, this.origin.location.lng);
          var startMarker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: latLng
          });
          startMarker.setMap(this.map);
          if (this.destination) startMarker.setMap(null); // set map center to origin address

          this.map.setCenter(latLng);
        } // show or hide vehicles

      }, {
        key: "toggleVehicles",
        value: function toggleVehicles() {
          this.showVehicles = !this.showVehicles;
          this.showModalBg = this.showVehicles == true;
        } // track drivers

      }, {
        key: "trackDrivers",
        value: function trackDrivers() {
          var _this7 = this;

          this.showDriverOnMap(this.locality);
          clearInterval(this.driverTracking);
          this.driverTracking = setInterval(function () {
            _this7.showDriverOnMap(_this7.locality);
          }, src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__["POSITION_INTERVAL"]);
        } // show drivers on map

      }, {
        key: "showDriverOnMap",
        value: function showDriverOnMap(locality) {
          var _this8 = this;

          // get active drivers
          this.driverService.getActiveDriver(locality, this.currentVehicle.id).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["take"])(1)).subscribe(function (snapshot) {
            // clear vehicles
            _this8.clearDrivers(); // only show near vehicle


            snapshot.forEach(function (vehicle) {
              // only show vehicle which has last active < 30 secs & distance < 5km
              var distance = _this8.placeService.calcCrow(vehicle.lat, vehicle.lng, _this8.origin.location.lat, _this8.origin.location.lng); // checking last active time and distance


              if (distance < src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__["SHOW_VEHICLES_WITHIN"] && Date.now() - vehicle.last_active < src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__["VEHICLE_LAST_ACTIVE_LIMIT"]) {
                // create or update
                var latLng = new google.maps.LatLng(vehicle.lat, vehicle.lng);
                var marker = new google.maps.Marker({
                  map: _this8.map,
                  position: latLng,
                  icon: {
                    url: _this8.currentVehicle.map_icon,
                    size: new google.maps.Size(32, 32),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(16, 16),
                    scaledSize: new google.maps.Size(32, 32)
                  }
                }); // add vehicle and marker to the list

                vehicle.distance = distance;

                _this8.driverMarkers.push(marker);

                _this8.activeDrivers.push(vehicle);
              } else {
                console.log('This vehicle is too far');
              }
            });
          });
        } // clear expired drivers on the map

      }, {
        key: "clearDrivers",
        value: function clearDrivers() {
          this.activeDrivers = [];
          this.driverMarkers.forEach(function (vehicle) {
            vehicle.setMap(null);
          });
        }
      }, {
        key: "editLocationDetails",
        value: function editLocationDetails() {
          var _this9 = this;

          this.alertCtrl.create({
            header: 'Location Details',
            inputs: [{
              name: 'locationDetails',
              type: 'text',
              value: this.locationDetails,
              placeholder: 'Gate Color, Number, Etc.'
            }],
            buttons: [{
              text: 'Cancel',
              handler: function handler() {}
            }, {
              text: 'OK',
              handler: function handler(data) {
                _this9.locationDetails = data.locationDetails;
              }
            }]
          }).then(function (r) {
            return r.present();
          });
        }
      }, {
        key: "editRecipientName",
        value: function editRecipientName() {
          var _this10 = this;

          this.alertCtrl.create({
            header: 'Recipient Name',
            inputs: [{
              name: 'recipientName',
              type: 'text',
              value: this.recipientName,
              placeholder: ''
            }],
            buttons: [{
              text: 'Cancel',
              handler: function handler() {}
            }, {
              text: 'OK',
              handler: function handler(data) {
                _this10.recipientName = data.recipientName;
              }
            }]
          }).then(function (r) {
            return r.present();
          });
        }
      }, {
        key: "editMobileNumber",
        value: function editMobileNumber() {
          var _this11 = this;

          this.alertCtrl.create({
            header: 'Mobile Number',
            inputs: [{
              name: 'mobileNumber',
              type: 'text',
              value: this.mobileNumber,
              placeholder: 'Ex. 09xxxxxxxxx'
            }],
            buttons: [{
              text: 'Cancel',
              handler: function handler() {}
            }, {
              text: 'OK',
              handler: function handler(data) {
                _this11.mobileNumber = data.mobileNumber;
              }
            }]
          }).then(function (r) {
            return r.present();
          });
        }
      }, {
        key: "editItemCategory",
        value: function editItemCategory() {
          var _this12 = this;

          this.alertCtrl.create({
            header: 'Type of Item',
            inputs: [{
              name: 'itemCategory',
              type: 'text',
              value: this.itemCategory,
              placeholder: 'Ex. Clothes, Toys, Etc.'
            }],
            buttons: [{
              text: 'Cancel',
              handler: function handler() {}
            }, {
              text: 'OK',
              handler: function handler(data) {
                _this12.itemCategory = data.itemCategory;
              }
            }]
          }).then(function (r) {
            return r.present();
          });
        }
      }]);

      return HomePage;
    }();

    HomePage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_12__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["AlertController"]
      }, {
        type: _services_place_service__WEBPACK_IMPORTED_MODULE_2__["PlaceService"]
      }, {
        type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_11__["Geolocation"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]
      }, {
        type: _services_setting_service__WEBPACK_IMPORTED_MODULE_3__["SettingService"]
      }, {
        type: _services_trip_service__WEBPACK_IMPORTED_MODULE_9__["TripService"]
      }, {
        type: _services_driver_service__WEBPACK_IMPORTED_MODULE_7__["DriverService"]
      }, {
        type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_8__["AngularFireAuth"]
      }, {
        type: _services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"]
      }, {
        type: _services_deal_service__WEBPACK_IMPORTED_MODULE_6__["DealService"]
      }, {
        type: _services_common_service__WEBPACK_IMPORTED_MODULE_15__["CommonService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["MenuController"]
      }];
    };

    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-home',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./home.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./home.page.scss */
      "./src/app/home/home.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_12__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["AlertController"], _services_place_service__WEBPACK_IMPORTED_MODULE_2__["PlaceService"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_11__["Geolocation"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _services_setting_service__WEBPACK_IMPORTED_MODULE_3__["SettingService"], _services_trip_service__WEBPACK_IMPORTED_MODULE_9__["TripService"], _services_driver_service__WEBPACK_IMPORTED_MODULE_7__["DriverService"], _angular_fire_auth__WEBPACK_IMPORTED_MODULE_8__["AngularFireAuth"], _services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"], _services_deal_service__WEBPACK_IMPORTED_MODULE_6__["DealService"], _services_common_service__WEBPACK_IMPORTED_MODULE_15__["CommonService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["MenuController"]])], HomePage);
    /***/
  },

  /***/
  "./src/app/services/deal.service.ts":
  /*!******************************************!*\
    !*** ./src/app/services/deal.service.ts ***!
    \******************************************/

  /*! exports provided: DealService */

  /***/
  function srcAppServicesDealServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DealService", function () {
      return DealService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/environments/environment.prod */
    "./src/environments/environment.prod.ts");
    /* harmony import */


    var _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/fire/database */
    "./node_modules/@angular/fire/database/es2015/index.js");
    /* harmony import */


    var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./auth.service */
    "./src/app/services/auth.service.ts");

    var DealService =
    /*#__PURE__*/
    function () {
      function DealService(db, authService) {
        _classCallCheck(this, DealService);

        this.db = db;
        this.authService = authService;
      } // sort driver by rating & distance


      _createClass(DealService, [{
        key: "sortDriversList",
        value: function sortDriversList(drivers) {
          return drivers.sort(function (a, b) {
            return a.rating - a.distance / 5 - (b.rating - b.distance / 5);
          });
        } // make deal to driver

      }, {
        key: "makeDeal",
        value: function makeDeal(driverId, origin, destination, distance, fee, currency, note, paymentMethod, promocode, discount, locationDetails, recipientName, mobileNumber, itemCategory) {
          var user = this.authService.getUserData();
          return this.db.object('deals/' + driverId).set({
            passengerId: user.uid,
            currency: currency,
            origin: origin,
            destination: destination,
            distance: distance,
            fee: fee,
            note: note,
            paymentMethod: paymentMethod,
            status: src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["DEAL_STATUS_PENDING"],
            createdAt: Date.now(),
            promocode: promocode,
            discount: discount,
            locationDetails: locationDetails,
            recipientName: recipientName,
            mobileNumber: mobileNumber,
            itemCategory: itemCategory
          });
        } // get deal by driverId

      }, {
        key: "getDriverDeal",
        value: function getDriverDeal(driverId) {
          return this.db.object('deals/' + driverId);
        } // remove deal

      }, {
        key: "removeDeal",
        value: function removeDeal(driverId) {
          return this.db.object('deals/' + driverId).remove();
        }
      }]);

      return DealService;
    }();

    DealService.ctorParameters = function () {
      return [{
        type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__["AngularFireDatabase"]
      }, {
        type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
      }];
    };

    DealService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_3__["AngularFireDatabase"], _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])], DealService);
    /***/
  },

  /***/
  "./src/app/services/setting.service.ts":
  /*!*********************************************!*\
    !*** ./src/app/services/setting.service.ts ***!
    \*********************************************/

  /*! exports provided: SettingService */

  /***/
  function srcAppServicesSettingServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SettingService", function () {
      return SettingService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/fire/database */
    "./node_modules/@angular/fire/database/es2015/index.js");

    var SettingService =
    /*#__PURE__*/
    function () {
      function SettingService(db) {
        _classCallCheck(this, SettingService);

        this.db = db;
      }

      _createClass(SettingService, [{
        key: "getPrices",
        value: function getPrices() {
          return this.db.object('master_settings/prices');
        }
      }]);

      return SettingService;
    }();

    SettingService.ctorParameters = function () {
      return [{
        type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]
      }];
    };

    SettingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]])], SettingService);
    /***/
  }
}]);
//# sourceMappingURL=home-home-module-es5.js.map