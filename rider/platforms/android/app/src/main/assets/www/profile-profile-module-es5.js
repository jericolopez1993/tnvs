function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile-profile-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.page.html":
  /*!*********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.page.html ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppProfileProfilePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>{{'BASIC_PROFILE' | translate}}</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"logout()\">Logout</ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <div style=\"text-align:center\">\n    <img src=\"{{ user.photoURL }}\" onError=\"this.src='assets/img/default-dp.png'\" style=\"width:50px;height:50px;border-radius:100px\" (click)=\"chooseFile()\">\n    <form ngNoForm>\n      <input id=\"avatar\" name=\"file\" type=\"file\" (change)=\"upload()\">\n    </form>\n    <h3>{{user.first_name}} {{user.last_name}}</h3>\n  </div>\n  <ion-list lines=\"none\">\n    <ion-item>\n      <ion-label position=\"stacked\" color=\"primary\">{{'FIRST_NAME' | translate}}</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"user.first_name\"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"stacked\" color=\"primary\">{{'MIDDLE_NAME' | translate}}</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"user.middle_name\"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"stacked\" color=\"primary\">{{'LAST_NAME' | translate}}</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"user.last_name\"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"stacked\" color=\"primary\">{{'BIRTH_DATE' | translate}}</ion-label>\n      <ion-datetime displayFormat=\"MM/DD/YYYY\" placeholder=\"Select Date\" [(ngModel)]=\"user.birth_date\"></ion-datetime>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"stacked\" color=\"primary\">{{'GENDER' | translate}}</ion-label>\n      <ion-select placeholder=\" Select gender\" [(ngModel)]=\"user.gender\">\n        <ion-select-option value=\"female\">Female</ion-select-option>\n        <ion-select-option value=\"male\">Male</ion-select-option>\n      </ion-select>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"stacked\" color=\"primary\">{{'ADDRESS' | translate}}</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"user.address\"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"stacked\" color=\"primary\">{{'PHONE_NUMBER' | translate}}</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"user.phoneNumber\" [disabled]=\"user.isPhoneVerified\"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"stacked\" color=\"primary\">{{'EMAIL_ADDRESS' | translate}}</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"user.email\" disabled></ion-input>\n      <ion-button *ngIf=\"!user.isEmailVerified\" (click)=\"verifyEmail()\">\n        {{'VERIFY' | translate}}</ion-button>\n    </ion-item>\n  </ion-list>\n  <div padding>\n    <ion-button color=\"dark\" expand=\"block\" (click)=\"save()\">{{'SAVE' | translate}}</ion-button>\n  </div>\n\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/profile/profile.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/profile/profile.module.ts ***!
    \*******************************************/

  /*! exports provided: ProfilePageModule */

  /***/
  function srcAppProfileProfileModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function () {
      return ProfilePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./profile.page */
    "./src/app/profile/profile.page.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");

    var routes = [{
      path: '',
      component: _profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]
    }];

    var ProfilePageModule = function ProfilePageModule() {
      _classCallCheck(this, ProfilePageModule);
    };

    ProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]]
    })], ProfilePageModule);
    /***/
  },

  /***/
  "./src/app/profile/profile.page.scss":
  /*!*******************************************!*\
    !*** ./src/app/profile/profile.page.scss ***!
    \*******************************************/

  /*! exports provided: default */

  /***/
  function srcAppProfileProfilePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "#avatar {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZmlsZS9DOlxcVXNlcnNcXEplcmljbyBQYXVsb1xcUnVieW1pbmVQcm9qZWN0c1xcdG52c1xccmlkZXIvc3JjXFxhcHBcXHByb2ZpbGVcXHByb2ZpbGUucGFnZS5zY3NzIiwic3JjL2FwcC9wcm9maWxlL3Byb2ZpbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvcHJvZmlsZS9wcm9maWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNhdmF0YXIge1xuICAgIGRpc3BsYXk6IG5vbmU7XG4gIH0iLCIjYXZhdGFyIHtcbiAgZGlzcGxheTogbm9uZTtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/profile/profile.page.ts":
  /*!*****************************************!*\
    !*** ./src/app/profile/profile.page.ts ***!
    \*****************************************/

  /*! exports provided: ProfilePage */

  /***/
  function srcAppProfileProfilePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProfilePage", function () {
      return ProfilePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../services/auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var _services_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../services/common.service */
    "./src/app/services/common.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/fire/storage */
    "./node_modules/@angular/fire/storage/es2015/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var ProfilePage =
    /*#__PURE__*/
    function () {
      function ProfilePage(authService, common, router, afStorage, alertCtrl, platform) {
        _classCallCheck(this, ProfilePage);

        this.authService = authService;
        this.common = common;
        this.router = router;
        this.afStorage = afStorage;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.user = {}; //setting default image, if user dont have images
      }

      _createClass(ProfilePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.authService.getUser(this.authService.getUserData().uid).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1)).subscribe(function (snapshot) {
            _this.user = Object.assign({
              uid: snapshot.key
            }, snapshot.payload.val());
            _this.user.isEmailVerified = _this.authService.getUserData().emailVerified;
          });
        } // save user info

      }, {
        key: "save",
        value: function save() {
          this.authService.updateUserProfile(this.user);
          this.common.showToast("Updated");
        } // choose file for upload

      }, {
        key: "chooseFile",
        value: function chooseFile() {
          document.getElementById('avatar').click();
        }
      }, {
        key: "upload",
        value: function upload() {
          var _this2 = this;

          // Create a root reference
          this.common.showLoader('Uploading..');

          var _loop = function _loop() {
            var selectedFile = _arr[_i];
            var path = '/users/' + Date.now() + "_".concat(selectedFile.name);

            var ref = _this2.afStorage.ref(path);

            ref.put(selectedFile).then(function () {
              ref.getDownloadURL().subscribe(function (data) {
                _this2.user.photoURL = data;
              });

              _this2.common.hideLoader();
            }).catch(function (err) {
              _this2.common.hideLoader();

              console.log(err);
            });
          };

          for (var _i = 0, _arr = [document.getElementById('avatar').files[0]]; _i < _arr.length; _i++) {
            _loop();
          }
        }
      }, {
        key: "verifyEmail",
        value: function verifyEmail() {
          var _this3 = this;

          this.authService.sendVerificationEmail().then(function (data) {
            _this3.common.showToast("Verfied Successfully");
          }).catch(function (err) {
            return console.log(err);
          });
        }
      }, {
        key: "logout",
        value: function logout() {
          var _this4 = this;

          this.alertCtrl.create({
            header: 'Sign out',
            message: "Are you sure?",
            buttons: [{
              text: 'No',
              handler: function handler() {}
            }, {
              text: 'Yes',
              handler: function handler() {
                _this4.authService.logout().then(function () {
                  _this4.router.navigateByUrl('/login');
                });
              }
            }]
          }).then(function (r) {
            return r.present();
          });
        }
      }]);

      return ProfilePage;
    }();

    ProfilePage.ctorParameters = function () {
      return [{
        type: _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
      }, {
        type: _services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }, {
        type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_6__["AngularFireStorage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"]
      }];
    };

    ProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-profile',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./profile.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./profile.page.scss */
      "./src/app/profile/profile.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"], _services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _angular_fire_storage__WEBPACK_IMPORTED_MODULE_6__["AngularFireStorage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"]])], ProfilePage);
    /***/
  },

  /***/
  "./src/app/services/common.service.ts":
  /*!********************************************!*\
    !*** ./src/app/services/common.service.ts ***!
    \********************************************/

  /*! exports provided: CommonService */

  /***/
  function srcAppServicesCommonServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CommonService", function () {
      return CommonService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var CommonService =
    /*#__PURE__*/
    function () {
      function CommonService(toastCtrl, loadCtrl, alertCtrl) {
        _classCallCheck(this, CommonService);

        this.toastCtrl = toastCtrl;
        this.loadCtrl = loadCtrl;
        this.alertCtrl = alertCtrl;
      }

      _createClass(CommonService, [{
        key: "showToast",
        value: function showToast(message) {
          this.toastCtrl.create({
            message: message,
            duration: 3000
          }).then(function (res) {
            return res.present();
          });
        }
      }, {
        key: "showAlert",
        value: function showAlert(message) {
          this.alertCtrl.create({
            message: message,
            buttons: ['ok']
          }).then(function (res) {
            return res.present();
          });
        }
      }, {
        key: "showLoader",
        value: function showLoader(message) {
          var _this5 = this;

          this.loadCtrl.create({
            message: message
          }).then(function (res) {
            _this5.loader = res.present();
            setTimeout(function () {
              return _this5.loadCtrl.dismiss();
            }, 10000);
          });
        }
      }, {
        key: "hideLoader",
        value: function hideLoader() {
          this.loadCtrl.dismiss();
        }
      }]);

      return CommonService;
    }();

    CommonService.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
      }];
    };

    CommonService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])], CommonService);
    /***/
  }
}]);
//# sourceMappingURL=profile-profile-module-es5.js.map