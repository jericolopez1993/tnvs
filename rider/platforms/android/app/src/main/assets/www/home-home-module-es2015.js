(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Home</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <div class=\"locationinput-holder\">\n    <input class=\"locationinput\" (click)=\"chooseOrigin()\" placeholder=\"Where do you want to pickup?\" type=\"text\"\n      value=\"{{origin ? origin.vicinity : '' }}\">\n    <input class=\"locationinput\" (click)=\"chooseDestination()\" placeholder=\"Where do you want to drop?\" type=\"text\"\n      value=\"{{ destination ? destination.vicinity : '' }}\">\n  </div>\n\n  <div id=\"{{ mapId }}\" [ngStyle]=\"{height: '100%'}\"></div>\n\n  <div class=\"align-bottom\">\n\n    <p class=\"distanceText\">\n      <span *ngIf=\"distanceText!=''\">Distance: {{ distanceText }}</span>\n      <span *ngIf=\"durationText!=''\">&nbsp; Duration: {{durationText}}</span>\n    </p>\n    <!--<ion-row [hidden]=\"!destination\">-->\n      <!--<ion-col (click)=\"choosePaymentMethod1()\">-->\n        <!--<ion-icon name=\"card\" color=\"gray\"></ion-icon>-->\n        <!--<span ion-text color=\"gray\">{{ getPaymentMethod() }}</span>-->\n      <!--</ion-col>-->\n      <!--<ion-col (click)=\"showPromoPopup()\" *ngIf=\"destination\">-->\n        <!--<ion-icon name=\"create\" color=\"gray\"></ion-icon>-->\n        <!--<span ion-text color=\"gray\">{{'PROMO' | translate}}</span>-->\n      <!--</ion-col>-->\n      <!--<ion-col (click)=\"showNotePopup()\">-->\n        <!--<ion-icon name=\"create\" color=\"gray\"></ion-icon>-->\n        <!--<span ion-text color=\"gray\">{{'NOTE' | translate}}</span>-->\n      <!--</ion-col>-->\n    <!--</ion-row>-->\n    <ion-row [hidden]=\"!destination\">\n      <ion-list lines=\"full\" class=\"ion-no-margin ion-no-padding\">\n        <ion-item>\n          <ion-input type=\"text\" [(ngModel)]=\"locationDetails\" placeholder=\"Location Details\" (click)=\"editLocationDetails()\" class=\"form-information-inputs\" readonly></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-input type=\"text\" [(ngModel)]=\"recipientName\"  placeholder=\"Recipient Name\" (click)=\"editRecipientName()\" class=\"form-information-inputs\" readonly></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-input type=\"text\" [(ngModel)]=\"mobileNumber\"  placeholder=\"Mobile Number\" (click)=\"editMobileNumber()\" class=\"form-information-inputs\" readonly></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-input type=\"text\" [(ngModel)]=\"itemCategory\"  placeholder=\"Type of Item\" (click)=\"editItemCategory()\" class=\"form-information-inputs\" readonly></ion-input>\n        </ion-item>\n      </ion-list>\n    </ion-row>\n\n    <ion-row [hidden]=\"!destination\">\n\n      <ion-col *ngFor=\"let vehicle of vehicles; let i = index\" [ngClass]=\"{'active': vehicle.active}\"\n        (click)=\"chooseVehicle(i)\">\n        <img src=\"{{ vehicle.icon }}\">\n        <p>{{ vehicle.name }}</p>\n        <p>{{currency }}{{ vehicle.fee }}</p>\n      </ion-col>\n\n    </ion-row>\n\n\n    <ion-button expand=\"block\" color=\"dark\" [hidden]=\"destination\" (click)=\"chooseDestination()\">\n      BOOK NOW</ion-button>\n    <ion-button expand=\"block\" color=\"dark\" [hidden]=\"!destination\" (click)=\"book()\">\n      {{ locateDriver == false ? 'BOOK NOW':'Locating Drivers'}} <ion-spinner name=\"dots\" color=\"light\"\n        [hidden]=\"!locateDriver\"></ion-spinner>\n    </ion-button>\n    <ion-button expand=\"block\" color=\"danger\" [hidden]=\"!locateDriver\" (click)=\"cancelBook()\">\n      Cancel Book\n    </ion-button>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");








let HomePageModule = class HomePageModule {
};
HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                {
                    path: '',
                    component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                }
            ])
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".locationinput-holder {\n  padding: 0.2rem 1rem;\n  position: absolute;\n  background: transparent;\n  z-index: 999999;\n  top: 0;\n  width: 100%;\n}\n\n.locationinput {\n  background: #fff;\n  border: 1px solid #ccc;\n  outline: 0;\n  width: 100%;\n  padding: 0.5rem;\n  color: #333;\n  margin: 2px 0px;\n  font-size: 14px;\n  border-radius: 4px;\n}\n\n.distanceText {\n  font-size: 14px;\n  color: #555;\n  margin: 0;\n}\n\n.align-bottom {\n  position: fixed;\n  bottom: 0;\n  width: 100%;\n  padding: 5px;\n}\n\n.align-bottom p {\n  font-size: 14p;\n}\n\n.header-md:after {\n  background-image: none;\n}\n\nion-col {\n  text-align: center;\n}\n\nion-col img {\n  width: 40px;\n  padding: 5px;\n  border-radius: 100%;\n}\n\nion-col p {\n  margin: 0;\n}\n\nion-row {\n  background: #fff;\n  border-radius: 3px;\n  border: 1px solid #eee;\n}\n\n.list-md {\n  margin: -1px 0 0px;\n}\n\n.label-md {\n  margin: 13px -50px 13px 0;\n}\n\n.text-input-md {\n  font-size: 14px;\n  margin: 8px 5px;\n}\n\n#map {\n  width: 100%;\n}\n\n.active img {\n  background: #ffce00;\n}\n\nion-list {\n  width: 100%;\n}\n\nion-input {\n  width: 100%;\n}\n\n.form-information-inputs {\n  background: transparent !important;\n  border: none !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9DOlxcVXNlcnNcXEplcmljbyBQYXVsb1xcUnVieW1pbmVQcm9qZWN0c1xcdG52c1xccmlkZXIvc3JjXFxhcHBcXGhvbWVcXGhvbWUucGFnZS5zY3NzIiwic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usb0JBQUE7RUFDQSxrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0FDQ0Y7O0FEQ0E7RUFDRSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNFRjs7QURDQTtFQUNFLGVBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtBQ0VGOztBREFBO0VBQ0UsZUFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ0dGOztBREFBO0VBQ0UsY0FBQTtBQ0dGOztBRERBO0VBQ0Usc0JBQUE7QUNJRjs7QURGQTtFQUNFLGtCQUFBO0FDS0Y7O0FESEE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FDTUY7O0FESkE7RUFBWSxTQUFBO0FDUVo7O0FEUEE7RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7QUNVRjs7QURSQTtFQUNFLGtCQUFBO0FDV0Y7O0FEVEE7RUFDRSx5QkFBQTtBQ1lGOztBRFZBO0VBQ0UsZUFBQTtFQUNBLGVBQUE7QUNhRjs7QURYQTtFQUNFLFdBQUE7QUNjRjs7QURYQTtFQUNFLG1CQUFBO0FDY0Y7O0FEWkE7RUFDRSxXQUFBO0FDZUY7O0FEWkE7RUFDRSxXQUFBO0FDZUY7O0FEWkE7RUFDRSxrQ0FBQTtFQUNBLHVCQUFBO0FDZUYiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvY2F0aW9uaW5wdXQtaG9sZGVye1xuICBwYWRkaW5nOiAwLjJyZW0gMXJlbTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgei1pbmRleDogOTk5OTk5O1xuICB0b3A6MDtcbiAgd2lkdGg6MTAwJTtcbn1cbi5sb2NhdGlvbmlucHV0e1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBib3JkZXI6MXB4IHNvbGlkICNjY2M7XG4gIG91dGxpbmU6MDtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDAuNXJlbTtcbiAgY29sb3I6ICMzMzM7XG4gIG1hcmdpbjogMnB4IDBweDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG59XG5cbi5kaXN0YW5jZVRleHR7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICM1NTU7XG4gIG1hcmdpbjogMDtcbn1cbi5hbGlnbi1ib3R0b20ge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGJvdHRvbTogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDVweDtcbn1cblxuLmFsaWduLWJvdHRvbSBwe1xuICBmb250LXNpemU6IDE0cFxufVxuLmhlYWRlci1tZDphZnRlcntcbiAgYmFja2dyb3VuZC1pbWFnZTogbm9uZTtcbn1cbmlvbi1jb2x7XG4gIHRleHQtYWxpZ246IGNlbnRlclxufVxuaW9uLWNvbCBpbWd7XG4gIHdpZHRoOiA0MHB4O1xuICBwYWRkaW5nOiA1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG59XG5pb24tY29sIHAgeyBtYXJnaW46IDB9XG5pb24tcm93e1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlZWU7XG59XG4ubGlzdC1tZCB7XG4gIG1hcmdpbjogLTFweCAwIDBweDtcbn1cbi5sYWJlbC1tZHtcbiAgbWFyZ2luOiAxM3B4IC01MHB4IDEzcHggMDtcbn1cbi50ZXh0LWlucHV0LW1ke1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbjogOHB4IDVweDtcbn1cbiNtYXAge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmFjdGl2ZSBpbWd7XG4gIGJhY2tncm91bmQ6ICNmZmNlMDA7XG59XG5pb24tbGlzdCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5pb24taW5wdXQge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmZvcm0taW5mb3JtYXRpb24taW5wdXRzIHtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQgIWltcG9ydGFudDtcbiAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG59IiwiLmxvY2F0aW9uaW5wdXQtaG9sZGVyIHtcbiAgcGFkZGluZzogMC4ycmVtIDFyZW07XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIHotaW5kZXg6IDk5OTk5OTtcbiAgdG9wOiAwO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmxvY2F0aW9uaW5wdXQge1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xuICBvdXRsaW5lOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMC41cmVtO1xuICBjb2xvcjogIzMzMztcbiAgbWFyZ2luOiAycHggMHB4O1xuICBmb250LXNpemU6IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbn1cblxuLmRpc3RhbmNlVGV4dCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICM1NTU7XG4gIG1hcmdpbjogMDtcbn1cblxuLmFsaWduLWJvdHRvbSB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgYm90dG9tOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogNXB4O1xufVxuXG4uYWxpZ24tYm90dG9tIHAge1xuICBmb250LXNpemU6IDE0cDtcbn1cblxuLmhlYWRlci1tZDphZnRlciB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IG5vbmU7XG59XG5cbmlvbi1jb2wge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbmlvbi1jb2wgaW1nIHtcbiAgd2lkdGg6IDQwcHg7XG4gIHBhZGRpbmc6IDVweDtcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcbn1cblxuaW9uLWNvbCBwIHtcbiAgbWFyZ2luOiAwO1xufVxuXG5pb24tcm93IHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZWVlO1xufVxuXG4ubGlzdC1tZCB7XG4gIG1hcmdpbjogLTFweCAwIDBweDtcbn1cblxuLmxhYmVsLW1kIHtcbiAgbWFyZ2luOiAxM3B4IC01MHB4IDEzcHggMDtcbn1cblxuLnRleHQtaW5wdXQtbWQge1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbjogOHB4IDVweDtcbn1cblxuI21hcCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uYWN0aXZlIGltZyB7XG4gIGJhY2tncm91bmQ6ICNmZmNlMDA7XG59XG5cbmlvbi1saXN0IHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbmlvbi1pbnB1dCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uZm9ybS1pbmZvcm1hdGlvbi1pbnB1dHMge1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xuICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_place_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/place.service */ "./src/app/services/place.service.ts");
/* harmony import */ var _services_setting_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/setting.service */ "./src/app/services/setting.service.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_deal_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/deal.service */ "./src/app/services/deal.service.ts");
/* harmony import */ var _services_driver_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/driver.service */ "./src/app/services/driver.service.ts");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/es2015/index.js");
/* harmony import */ var _services_trip_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../services/trip.service */ "./src/app/services/trip.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! src/environments/environment.prod */ "./src/environments/environment.prod.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_16__);

















let HomePage = class HomePage {
    constructor(router, alertCtrl, placeService, geolocation, chRef, settingService, tripService, driverService, afAuth, authService, translate, dealService, common, menuCtrl) {
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.placeService = placeService;
        this.geolocation = geolocation;
        this.chRef = chRef;
        this.settingService = settingService;
        this.tripService = tripService;
        this.driverService = driverService;
        this.afAuth = afAuth;
        this.authService = authService;
        this.translate = translate;
        this.dealService = dealService;
        this.common = common;
        this.menuCtrl = menuCtrl;
        this.mapId = Math.random() + 'map';
        this.mapHeight = 480;
        this.showModalBg = false;
        this.showVehicles = false;
        this.vehicles = [];
        this.note = '';
        this.promocode = '';
        this.distance = 0;
        this.duration = 0;
        this.paymentMethod = 'cash';
        this.activeDrivers = [];
        this.driverMarkers = [];
        this.locateDriver = false;
        this.user = {};
        this.isTrackDriverEnabled = true;
        this.discount = 0;
        this.distanceText = '';
        this.durationText = '';
    }
    ionViewDidEnter() {
        // this.common.showLoader('Loading...');
        this.menuCtrl.enable(true);
        this.afAuth.authState.subscribe(authData => {
            if (authData) {
                this.user = this.authService.getUserData();
            }
        });
        this.origin = this.tripService.getOrigin();
        this.destination = this.tripService.getDestination();
        this.loadMap();
        // this.common.hideLoader();
    }
    ngOnInit() {
        console.log("calling");
    }
    ionViewWillLeave() {
        clearInterval(this.driverTracking);
    }
    // get current payment method from service
    getPaymentMethod() {
        this.paymentMethod = this.tripService.getPaymentMethod();
        return this.paymentMethod;
    }
    choosePaymentMethod1() {
        this.alertCtrl.create({
            header: "Choose Payments",
            inputs: [
                { type: 'radio', label: "Cash", value: 'cash' },
                { type: 'radio', label: "Card", value: 'card' }
            ],
            buttons: [{
                    text: "Cancel"
                }, {
                    text: "Choose",
                    handler: (data) => {
                        if (data == 'card') {
                            this.authService.getCardSetting().valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["take"])(1)).subscribe((res) => {
                                if (res != null) {
                                    this.tripService.setPaymentMethod(data);
                                    this.paymentMethod = data;
                                    const exp = res.exp.split('/');
                                    Stripe.card.createToken({
                                        number: res.number,
                                        exp_month: exp[0],
                                        exp_year: exp[1],
                                        cvc: res.cvv
                                    }, (status, response) => {
                                        if (status == 200) {
                                            console.log("Card Ready");
                                            this.authService.updateCardSetting(res.number, res.exp, res.cvv, response.id);
                                        }
                                        else {
                                            this.common.showToast(response.error.message);
                                        }
                                    });
                                }
                                else
                                    this.common.showAlert("Invalid Card");
                            });
                        }
                        else if (data == 'cash') {
                            this.paymentMethod = data;
                            this.tripService.setPaymentMethod(data);
                        }
                    }
                }]
        }).then(res => res.present());
    }
    // toggle active vehicle
    chooseVehicle(index) {
        for (var i = 0; i < this.vehicles.length; i++) {
            this.vehicles[i].active = (i == index);
            // choose this vehicle type
            if (i == index) {
                this.tripService.setVehicle(this.vehicles[i]);
                this.currentVehicle = this.vehicles[i];
            }
        }
        // start tracking new driver type
        this.trackDrivers();
        this.toggleVehicles();
    }
    loadMap() {
        // this.common.showLoader("Loading..");
        // get current location
        return this.geolocation.getCurrentPosition().then((resp) => {
            if (this.origin)
                this.startLatLng = new google.maps.LatLng(this.origin.location.lat, this.origin.location.lng);
            else
                this.startLatLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
            let directionsDisplay;
            let directionsService = new google.maps.DirectionsService();
            directionsDisplay = new google.maps.DirectionsRenderer();
            this.map = new google.maps.Map(document.getElementById(this.mapId), {
                zoom: 15,
                center: this.startLatLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                zoomControl: false,
                streetViewControl: false,
                fullscreenControl: false
            });
            let mapx = this.map;
            directionsDisplay.setMap(mapx);
            // find map center address
            let geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': this.map.getCenter() }, (results, status) => {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (!this.origin) {
                        // set map center as origin
                        this.origin = this.placeService.formatAddress(results[0]);
                        this.tripService.setOrigin(this.origin.vicinity, this.origin.location.lat, this.origin.location.lng);
                        this.setOrigin();
                        this.chRef.detectChanges();
                    }
                    else {
                        this.setOrigin();
                    }
                    // save locality
                    let locality = this.placeService.setLocalityFromGeocoder(results);
                    console.log('locality', locality);
                    // load list vehicles
                    this.settingService.getPrices().valueChanges().subscribe((snapshot) => {
                        this.vehicles = [];
                        let obj = snapshot[locality] ? snapshot[locality] : snapshot.default;
                        this.currency = obj.currency;
                        this.tripService.setCurrency(this.currency);
                        // calculate price
                        Object.keys(obj.vehicles).forEach(id => {
                            obj.vehicles[id].id = id;
                            this.vehicles.push(obj.vehicles[id]);
                        });
                        // calculate distance between origin adn destination
                        if (this.destination) {
                            directionsService.route(request, (result, status) => {
                                if (status == google.maps.DirectionsStatus.OK && result.routes.length != 0) {
                                    this.distance = result.routes[0].legs[0].distance.value;
                                    this.distanceText = result.routes[0].legs[0].distance.text;
                                    this.durationText = result.routes[0].legs[0].duration.text;
                                    for (let i = 0; i < this.vehicles.length; i++) {
                                        var currentDay = new Date();
                                        // var nextDay = new Date(Date.now()+(1*24*60*60*1000));
                                        var startTime1 = new Date(currentDay.getFullYear(), currentDay.getMonth(), currentDay.getDate(), 21).getTime();
                                        var endTime1 = new Date(currentDay.getFullYear(), currentDay.getMonth(), currentDay.getDate(), 23, 59, 60).getTime();
                                        var startTime2 = new Date(currentDay.getFullYear(), currentDay.getMonth(), currentDay.getDate(), 0).getTime();
                                        var endTime2 = new Date(currentDay.getFullYear(), currentDay.getMonth(), currentDay.getDate(), 4, 59, 60).getTime();
                                        this.vehicles[i].fee = (this.distance * this.vehicles[i].price / 1000) + this.vehicles[i].first_fee;
                                        if (this.distance >= 15000 && this.distance <= 24999) {
                                            this.vehicles[i].fee = this.vehicles[i].fee + this.vehicles[i].second_fee;
                                        }
                                        else if (this.distance >= 25000 && this.distance <= 49999) {
                                            this.vehicles[i].fee = this.vehicles[i].fee + this.vehicles[i].third_fee;
                                        }
                                        else if (this.distance >= 50000) {
                                            this.vehicles[i].fee = this.vehicles[i].fee + this.vehicles[i].fourth_fee;
                                        }
                                        if ((currentDay.getTime() >= startTime1 && currentDay.getTime() <= endTime1) || (currentDay.getTime() >= startTime2 && currentDay.getTime() <= endTime2)) {
                                            this.vehicles[i].fee = this.vehicles[i].fee + this.vehicles[i].night_fee;
                                        }
                                        this.vehicles[i].fee = Math.floor(this.vehicles[i].fee);
                                    }
                                }
                                else {
                                    console.log("error");
                                }
                            });
                        }
                        // set first device as default
                        this.vehicles[0].active = true;
                        this.currentVehicle = this.vehicles[0];
                        this.locality = locality;
                        if (this.isTrackDriverEnabled)
                            this.trackDrivers();
                    });
                }
            });
            // add destination to map
            if (this.destination) {
                this.destLatLng = new google.maps.LatLng(this.destination.location.lat, this.destination.location.lng);
                var bounds = new google.maps.LatLngBounds();
                bounds.extend(this.startLatLng);
                bounds.extend(this.destLatLng);
                mapx.fitBounds(bounds);
                var request = {
                    origin: this.startLatLng,
                    destination: this.destLatLng,
                    travelMode: google.maps.TravelMode.DRIVING
                };
                directionsService.route(request, function (response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setDirections(response);
                        directionsDisplay.setMap(mapx);
                    }
                    else {
                        console.log("error");
                    }
                });
            }
            // this.common.hideLoader();
        }).catch((error) => {
            // this.common.hideLoader();
            console.log('Error getting location', error);
        });
    }
    showPromoPopup() {
        this.alertCtrl.create({
            header: 'Enter Promo code',
            message: "",
            inputs: [
                {
                    name: 'promocode',
                    placeholder: 'Enter Promo Code'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Apply',
                    handler: data => {
                        //verifying promocode
                        firebase__WEBPACK_IMPORTED_MODULE_16__["database"]().ref('promocodes').orderByChild("code").equalTo(data.promocode).once('value', promocodes => {
                            let tmp = [];
                            promocodes.forEach(promo => {
                                tmp.push(Object.assign({ key: promo.key }, promo.val()));
                                return false;
                            });
                            tmp = tmp[0];
                            if (promocodes.val() != null || promocodes.val() != undefined) {
                                this.promocode = tmp.code;
                                this.discount = tmp.discount;
                                this.tripService.setPromo(tmp.code);
                                this.tripService.setDiscount(tmp.discount);
                            }
                        }, err => console.log(err));
                    }
                }
            ]
        }).then(prompt => prompt.present());
    }
    // Show note popup when click to 'Notes to user'
    showNotePopup() {
        this.alertCtrl.create({
            header: 'Notes to user',
            message: "",
            inputs: [
                { name: 'note', placeholder: 'Note' },
            ],
            buttons: [
                { text: 'Cancel' },
                {
                    text: 'Save',
                    handler: data => {
                        this.note = data;
                        this.tripService.setNote(data);
                        console.log('Saved clicked');
                    }
                }
            ]
        }).then(prompt => prompt.present());
    }
    ;
    // go to next view when the 'Book' button is clicked
    book() {
        this.locateDriver = true;
        // store detail
        this.tripService.setAvailableDrivers(this.activeDrivers);
        this.tripService.setDistance(this.distance);
        this.tripService.setFee(this.currentVehicle.fee);
        this.tripService.setIcon(this.currentVehicle.icon);
        this.tripService.setNote(this.note);
        this.tripService.setPromo(this.promocode);
        this.tripService.setDiscount(this.discount);
        this.tripService.setLocationDetails(this.locationDetails);
        this.tripService.setRecipientName(this.recipientName);
        this.tripService.setMobileNumber(this.mobileNumber);
        this.tripService.setItemCategory(this.itemCategory);
        // this.tripService.setPaymentMethod('');
        this.drivers = this.tripService.getAvailableDrivers();
        // sort by driver distance and rating
        this.drivers = this.dealService.sortDriversList(this.drivers);
        if (this.drivers && this.locationDetails && this.recipientName && this.mobileNumber && this.itemCategory) {
            this.retryDeal(0);
        }
        else {
            this.locateDriver = false;
            this.common.showAlert("Please input the textboxes. For Driver to understand more about the delivery.");
        }
    }
    cancelBook() {
        let driver = this.drivers[0];
        if (driver) {
            this.dealService.removeDeal(driver.key);
        }
        this.locateDriver = false;
    }
    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    retryDeal(index) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let i = 0;
            do {
                this.makeDeal(index);
                i = i + 1;
                yield this.sleep(5000);
                if (this.drivers[index] && this.dealAccepted) {
                    break;
                }
                if (!this.locateDriver) {
                    break;
                }
                if (i == 5) {
                    this.locateDriver = false;
                    this.common.showAlert("No Driver Found");
                }
            } while (i < 5);
        });
    }
    makeDeal(index) {
        let driver = this.drivers[index];
        this.dealAccepted = false;
        if (driver) {
            driver.status = 'Bidding';
            this.dealService.getDriverDeal(driver.key).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["take"])(1)).subscribe((snapshot) => {
                // if user is available
                if (snapshot == null) {
                    // create a record
                    this.dealService.makeDeal(driver.key, this.tripService.getOrigin(), this.tripService.getDestination(), this.tripService.getDistance(), this.tripService.getFee(), this.tripService.getCurrency(), this.tripService.getNote(), this.tripService.getPaymentMethod(), this.tripService.getPromo(), this.tripService.getDiscount(), this.tripService.getLocationDetails(), this.tripService.getRecipientName(), this.tripService.getMobileNumber(), this.tripService.getItemCategory()).then(() => {
                        let sub = this.dealService.getDriverDeal(driver.key).valueChanges().subscribe((snap) => {
                            // if record doesn't exist or is accepted
                            if (snap === null || snap.status != src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__["DEAL_STATUS_PENDING"]) {
                                sub.unsubscribe();
                                // if deal has been cancelled
                                if (snap === null) {
                                    this.nextDriver(index);
                                }
                                else if (snap.status == src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__["DEAL_STATUS_ACCEPTED"]) {
                                    // if deal is accepted
                                    this.dealAccepted = true;
                                    this.drivers = [];
                                    this.tripService.setId(snap.tripId);
                                    // go to user page
                                    this.locateDriver = false;
                                    this.router.navigateByUrl('tracking');
                                }
                            }
                        });
                    });
                }
                else {
                    this.nextDriver(index);
                }
            });
        }
    }
    // make deal to next driver
    nextDriver(index) {
        this.drivers.splice(index, 1);
        this.makeDeal(index);
    }
    // choose origin place
    chooseOrigin() {
        this.router.navigate(['map'], {
            queryParams: {
                type: 'origin'
            }
        });
    }
    // choose destination place
    chooseDestination() {
        this.router.navigate(['map'], {
            queryParams: {
                type: 'destination'
            }
        });
    }
    choosePaymentMethod() {
        this.router.navigateByUrl('payments');
    }
    // add origin marker to map
    setOrigin() {
        // add origin and destination marker
        let latLng = new google.maps.LatLng(this.origin.location.lat, this.origin.location.lng);
        let startMarker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: latLng
        });
        startMarker.setMap(this.map);
        if (this.destination)
            startMarker.setMap(null);
        // set map center to origin address
        this.map.setCenter(latLng);
    }
    // show or hide vehicles
    toggleVehicles() {
        this.showVehicles = !this.showVehicles;
        this.showModalBg = (this.showVehicles == true);
    }
    // track drivers
    trackDrivers() {
        this.showDriverOnMap(this.locality);
        clearInterval(this.driverTracking);
        this.driverTracking = setInterval(() => {
            this.showDriverOnMap(this.locality);
        }, src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__["POSITION_INTERVAL"]);
    }
    // show drivers on map
    showDriverOnMap(locality) {
        // get active drivers
        this.driverService.getActiveDriver(locality, this.currentVehicle.id).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["take"])(1)).subscribe((snapshot) => {
            // clear vehicles
            this.clearDrivers();
            // only show near vehicle
            snapshot.forEach(vehicle => {
                // only show vehicle which has last active < 30 secs & distance < 5km
                let distance = this.placeService.calcCrow(vehicle.lat, vehicle.lng, this.origin.location.lat, this.origin.location.lng);
                // checking last active time and distance
                if (distance < src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__["SHOW_VEHICLES_WITHIN"] && Date.now() - vehicle.last_active < src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__["VEHICLE_LAST_ACTIVE_LIMIT"]) {
                    // create or update
                    let latLng = new google.maps.LatLng(vehicle.lat, vehicle.lng);
                    let marker = new google.maps.Marker({
                        map: this.map,
                        position: latLng,
                        icon: {
                            url: this.currentVehicle.map_icon,
                            size: new google.maps.Size(32, 32),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(16, 16),
                            scaledSize: new google.maps.Size(32, 32)
                        },
                    });
                    // add vehicle and marker to the list
                    vehicle.distance = distance;
                    this.driverMarkers.push(marker);
                    this.activeDrivers.push(vehicle);
                }
                else {
                    console.log('This vehicle is too far');
                }
            });
        });
    }
    // clear expired drivers on the map
    clearDrivers() {
        this.activeDrivers = [];
        this.driverMarkers.forEach((vehicle) => {
            vehicle.setMap(null);
        });
    }
    editLocationDetails() {
        this.alertCtrl.create({
            header: 'Location Details',
            inputs: [
                {
                    name: 'locationDetails',
                    type: 'text',
                    value: this.locationDetails,
                    placeholder: 'Gate Color, Number, Etc.'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'OK',
                    handler: (data) => {
                        this.locationDetails = data.locationDetails;
                    }
                }
            ]
        }).then(r => r.present());
    }
    editRecipientName() {
        this.alertCtrl.create({
            header: 'Recipient Name',
            inputs: [
                {
                    name: 'recipientName',
                    type: 'text',
                    value: this.recipientName,
                    placeholder: ''
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'OK',
                    handler: (data) => {
                        this.recipientName = data.recipientName;
                    }
                }
            ]
        }).then(r => r.present());
    }
    editMobileNumber() {
        this.alertCtrl.create({
            header: 'Mobile Number',
            inputs: [
                {
                    name: 'mobileNumber',
                    type: 'text',
                    value: this.mobileNumber,
                    placeholder: 'Ex. 09xxxxxxxxx'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'OK',
                    handler: (data) => {
                        this.mobileNumber = data.mobileNumber;
                    }
                }
            ]
        }).then(r => r.present());
    }
    editItemCategory() {
        this.alertCtrl.create({
            header: 'Type of Item',
            inputs: [
                {
                    name: 'itemCategory',
                    type: 'text',
                    value: this.itemCategory,
                    placeholder: 'Ex. Clothes, Toys, Etc.'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'OK',
                    handler: (data) => {
                        this.itemCategory = data.itemCategory;
                    }
                }
            ]
        }).then(r => r.present());
    }
};
HomePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_12__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["AlertController"] },
    { type: _services_place_service__WEBPACK_IMPORTED_MODULE_2__["PlaceService"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_11__["Geolocation"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
    { type: _services_setting_service__WEBPACK_IMPORTED_MODULE_3__["SettingService"] },
    { type: _services_trip_service__WEBPACK_IMPORTED_MODULE_9__["TripService"] },
    { type: _services_driver_service__WEBPACK_IMPORTED_MODULE_7__["DriverService"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_8__["AngularFireAuth"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"] },
    { type: _services_deal_service__WEBPACK_IMPORTED_MODULE_6__["DealService"] },
    { type: _services_common_service__WEBPACK_IMPORTED_MODULE_15__["CommonService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["MenuController"] }
];
HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_12__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["AlertController"],
        _services_place_service__WEBPACK_IMPORTED_MODULE_2__["PlaceService"],
        _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_11__["Geolocation"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
        _services_setting_service__WEBPACK_IMPORTED_MODULE_3__["SettingService"],
        _services_trip_service__WEBPACK_IMPORTED_MODULE_9__["TripService"],
        _services_driver_service__WEBPACK_IMPORTED_MODULE_7__["DriverService"],
        _angular_fire_auth__WEBPACK_IMPORTED_MODULE_8__["AngularFireAuth"],
        _services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"],
        _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"],
        _services_deal_service__WEBPACK_IMPORTED_MODULE_6__["DealService"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_15__["CommonService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["MenuController"]])
], HomePage);



/***/ }),

/***/ "./src/app/services/deal.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/deal.service.ts ***!
  \******************************************/
/*! exports provided: DealService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DealService", function() { return DealService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment.prod */ "./src/environments/environment.prod.ts");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/es2015/index.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./auth.service */ "./src/app/services/auth.service.ts");





let DealService = class DealService {
    constructor(db, authService) {
        this.db = db;
        this.authService = authService;
    }
    // sort driver by rating & distance
    sortDriversList(drivers) {
        return drivers.sort((a, b) => {
            return (a.rating - a.distance / 5) - (b.rating - b.distance / 5);
        });
    }
    // make deal to driver
    makeDeal(driverId, origin, destination, distance, fee, currency, note, paymentMethod, promocode, discount, locationDetails, recipientName, mobileNumber, itemCategory) {
        let user = this.authService.getUserData();
        return this.db.object('deals/' + driverId).set({
            passengerId: user.uid,
            currency: currency,
            origin: origin,
            destination: destination,
            distance: distance,
            fee: fee,
            note: note,
            paymentMethod: paymentMethod,
            status: src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["DEAL_STATUS_PENDING"],
            createdAt: Date.now(),
            promocode: promocode,
            discount: discount,
            locationDetails: locationDetails,
            recipientName: recipientName,
            mobileNumber: mobileNumber,
            itemCategory: itemCategory
        });
    }
    // get deal by driverId
    getDriverDeal(driverId) {
        return this.db.object('deals/' + driverId);
    }
    // remove deal
    removeDeal(driverId) {
        return this.db.object('deals/' + driverId).remove();
    }
};
DealService.ctorParameters = () => [
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__["AngularFireDatabase"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] }
];
DealService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_3__["AngularFireDatabase"], _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
], DealService);



/***/ }),

/***/ "./src/app/services/setting.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/setting.service.ts ***!
  \*********************************************/
/*! exports provided: SettingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingService", function() { return SettingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/es2015/index.js");



let SettingService = class SettingService {
    constructor(db) {
        this.db = db;
    }
    getPrices() {
        return this.db.object('master_settings/prices');
    }
};
SettingService.ctorParameters = () => [
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"] }
];
SettingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]])
], SettingService);



/***/ })

}]);
//# sourceMappingURL=home-home-module-es2015.js.map