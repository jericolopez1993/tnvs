function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rating-rating-module"], {
  /***/
  "./node_modules/ionic-rating/fesm2015/ionic-rating.js":
  /*!************************************************************!*\
    !*** ./node_modules/ionic-rating/fesm2015/ionic-rating.js ***!
    \************************************************************/

  /*! exports provided: IonRatingComponent, IonicRatingModule */

  /***/
  function node_modulesIonicRatingFesm2015IonicRatingJs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "IonRatingComponent", function () {
      return IonRatingComponent;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "IonicRatingModule", function () {
      return IonicRatingModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */


    var IonRatingComponent =
    /*#__PURE__*/
    function () {
      /**
       * @param {?} cd
       */
      function IonRatingComponent(cd) {
        _classCallCheck(this, IonRatingComponent);

        this.cd = cd;
        this.hover = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.leave = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.rateChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
      }
      /**
       * @param {?} changes
       * @return {?}
       */


      _createClass(IonRatingComponent, [{
        key: "ngOnChanges",
        value: function ngOnChanges(changes) {
          if (changes.rate) {
            this.update(this.rate);
          }
        }
        /**
         * @private
         * @param {?} value
         * @param {?=} internalChange
         * @return {?}
         */

      }, {
        key: "update",
        value: function update(value) {
          var internalChange = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

          if (!(this.readonly || this.disabled || this.rate === value)) {
            this.rate = value;
            this.rateChange.emit(this.rate);
          }

          if (internalChange) {
            if (this.onChange) {
              this.onChange(this.rate);
            }

            if (this.onTouched) {
              this.onTouched();
            }
          }
        }
        /**
         * @param {?} rate
         * @return {?}
         */

      }, {
        key: "onClick",
        value: function onClick(rate) {
          this.update(this.resettable && this.rate === rate ? 0 : rate);
        }
        /**
         * @param {?} value
         * @return {?}
         */

      }, {
        key: "onMouseEnter",
        value: function onMouseEnter(value) {
          if (!(this.disabled || this.readonly)) {
            this.hoverRate = value;
          }

          this.hover.emit(value);
        }
        /**
         * @return {?}
         */

      }, {
        key: "onMouseLeave",
        value: function onMouseLeave() {
          this.leave.emit(this.hoverRate);
          this.hoverRate = 0;
        }
        /**
         * @return {?}
         */

      }, {
        key: "onBlur",
        value: function onBlur() {
          if (this.onTouched) {
            this.onTouched();
          }
        }
        /**
         * @param {?} value
         * @return {?}
         */

      }, {
        key: "writeValue",
        value: function writeValue(value) {
          this.update(value, false);
          this.cd.markForCheck();
        }
        /**
         * @param {?} fn
         * @return {?}
         */

      }, {
        key: "registerOnChange",
        value: function registerOnChange(fn) {
          this.onChange = fn;
        }
        /**
         * @param {?} fn
         * @return {?}
         */

      }, {
        key: "registerOnTouched",
        value: function registerOnTouched(fn) {
          this.onTouched = fn;
        }
        /**
         * @param {?} isDisabled
         * @return {?}
         */

      }, {
        key: "setDisabledState",
        value: function setDisabledState(isDisabled) {
          this.disabled = isDisabled;
        }
      }]);

      return IonRatingComponent;
    }();

    IonRatingComponent.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
      args: [{
        selector: 'ion-rating',
        template: "<ion-button *ngFor=\"let i of [1, 2, 3, 4, 5]\" type=\"button\" fill=\"clear\" [disabled]=\"disabled || readonly\"\n  (mouseenter)=\"onMouseEnter(i)\" (click)=\"onClick(i)\" [class.filled]=\"i <= hoverRate || (!hoverRate && i <= rate)\"\n  [class.readonly]=\"readonly\">\n  <ion-icon slot=\"icon-only\" name=\"star\" [size]=\"size\">\n  </ion-icon>\n</ion-button>",
        changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
        providers: [{
          provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"],
          useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(
          /**
          * @return {?}
          */
          function () {
            return IonRatingComponent;
          }),
          multi: true
        }],
        styles: [":host{--star-color:gray;--star-color-filled:orange;display:inline-block}ion-button{--padding-start:0;--padding-end:0;--color:var(--star-color);--color-focused:var(--star-color);--color-activated:var(--star-color)}ion-button.filled{--color:var(--star-color-filled);--color-focused:var(--star-color-filled);--color-activated:var(--star-color-filled)}ion-button.readonly{--opacity:1}"]
      }]
    }];
    /** @nocollapse */

    IonRatingComponent.ctorParameters = function () {
      return [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
      }];
    };

    IonRatingComponent.propDecorators = {
      rate: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      readonly: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      resettable: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      size: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
      }],
      hover: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      leave: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      rateChange: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
      }],
      onMouseLeave: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
        args: ['mouseleave', []]
      }],
      onBlur: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
        args: ['blur', []]
      }]
    };
    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    var IonicRatingModule = function IonicRatingModule() {
      _classCallCheck(this, IonicRatingModule);
    };

    IonicRatingModule.decorators = [{
      type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
      args: [{
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"]],
        exports: [IonRatingComponent],
        declarations: [IonRatingComponent]
      }]
    }]; //# sourceMappingURL=ionic-rating.js.map

    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/rating/rating.page.html":
  /*!*******************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rating/rating.page.html ***!
    \*******************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppRatingRatingPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>rating</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div style=\"text-align: center\">\n    <h1 style=\"font-size:5rem;\"> {{ (trip)?.currency }} {{(trip)?.final}}</h1>\n    <p *ngIf=\"trip.promo\">{{ (trip)?.discount}} - {{(trip)?.promo}}</p>\n    <p>Payment Mode: {{(trip)?.paymentMethod}}</p>\n  </div>\n  <div style=\"display: flex; justify-content: center; align-items: center; flex-direction: column; text-align: center \">\n    <img src=\"{{ (driver)?.photoURL }}\" onError=\"this.src='assets/img/default-dp.png'\" style=\"height:80px\" />\n    <h3>{{(driver)?.first_name}} {{(driver)?.last_name}}</h3>\n    <p>{{ (driver)?.plate }} • {{ (driver)?.brand }}</p>\n    <ion-rating [rate]=\"rating\" (rateChange)=\"onRateChange($event)\"></ion-rating>\n  </div>\n</ion-content>\n<ion-footer>\n  <ion-toolbar>\n    <ion-button expand=\"block\" color=\"dark\" (click)=\"rateTrip()\">RATE THIS TRIP</ion-button>\n  </ion-toolbar>\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/rating/rating.module.ts":
  /*!*****************************************!*\
    !*** ./src/app/rating/rating.module.ts ***!
    \*****************************************/

  /*! exports provided: RatingPageModule */

  /***/
  function srcAppRatingRatingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RatingPageModule", function () {
      return RatingPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _rating_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./rating.page */
    "./src/app/rating/rating.page.ts");
    /* harmony import */


    var ionic_rating__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ionic-rating */
    "./node_modules/ionic-rating/fesm2015/ionic-rating.js");

    var routes = [{
      path: '',
      component: _rating_page__WEBPACK_IMPORTED_MODULE_6__["RatingPage"]
    }];

    var RatingPageModule = function RatingPageModule() {
      _classCallCheck(this, RatingPageModule);
    };

    RatingPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], ionic_rating__WEBPACK_IMPORTED_MODULE_7__["IonicRatingModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_rating_page__WEBPACK_IMPORTED_MODULE_6__["RatingPage"]]
    })], RatingPageModule);
    /***/
  },

  /***/
  "./src/app/rating/rating.page.scss":
  /*!*****************************************!*\
    !*** ./src/app/rating/rating.page.scss ***!
    \*****************************************/

  /*! exports provided: default */

  /***/
  function srcAppRatingRatingPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JhdGluZy9yYXRpbmcucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/rating/rating.page.ts":
  /*!***************************************!*\
    !*** ./src/app/rating/rating.page.ts ***!
    \***************************************/

  /*! exports provided: RatingPage */

  /***/
  function srcAppRatingRatingPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RatingPage", function () {
      return RatingPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_trip_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../services/trip.service */
    "./src/app/services/trip.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var RatingPage =
    /*#__PURE__*/
    function () {
      function RatingPage(tripService, router, route) {
        var _this = this;

        _classCallCheck(this, RatingPage);

        this.tripService = tripService;
        this.router = router;
        this.route = route;
        this.trip = {};
        this.driver = {};
        this.rating = 5;
        this.route.queryParams.subscribe(function (data) {
          _this.driver = JSON.parse(data.driver);
          _this.trip = JSON.parse(data.trip);
        });
      }

      _createClass(RatingPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "onRateChange",
        value: function onRateChange(event) {
          this.rating = event.value;
        }
      }, {
        key: "rateTrip",
        value: function rateTrip() {
          var _this2 = this;

          this.tripService.rateTrip(this.trip.key, this.rating).then(function () {
            _this2.router.navigateByUrl('home');
          });
        }
      }]);

      return RatingPage;
    }();

    RatingPage.ctorParameters = function () {
      return [{
        type: _services_trip_service__WEBPACK_IMPORTED_MODULE_2__["TripService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
      }];
    };

    RatingPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-rating',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./rating.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/rating/rating.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./rating.page.scss */
      "./src/app/rating/rating.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_trip_service__WEBPACK_IMPORTED_MODULE_2__["TripService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])], RatingPage);
    /***/
  }
}]);
//# sourceMappingURL=rating-rating-module-es5.js.map