(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <div class=\"login-header\">\n    <img src=\"./assets/img/logo-2.png\" />\n    <h2 ion-text color=\"light\">{{'RIDER' | translate}}</h2>\n  </div>\n  <div padding>\n    <ion-list lines=\"none\">\n      <ion-item>\n        <ion-label position=\"stacked\">{{'EMAIL_ADDRESS' | translate }}</ion-label>\n        <ion-input type=\"text\" [(ngModel)]=\"email\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"stacked\">{{'PASSWORD' | translate }}</ion-label>\n        <ion-input type=\"password\" [(ngModel)]=\"password\"></ion-input>\n\n      </ion-item>\n    </ion-list>\n    <div padding>\n      <ion-button color=\"dark\" expand=\"block\" (click)=\"login()\">{{'LOGIN' | translate }}</ion-button>\n      <ion-button color=\"primary\" expand=\"block\" (click)=\"login_fb()\">LOGIN WITH FACEBOOK</ion-button>\n      <ion-button color=\"medium\" expand=\"block\" fill=\"clear\" slot=\"end\" (click)=\"reset()\">\n        {{'FORGOT' | translate }}\n      </ion-button>\n      <ion-button color=\"medium\" expand=\"block\" fill=\"clear\" slot=\"end\" routerLink=\"/driver/login\">\n        {{'ARE_YOU_DRIVER' | translate }}\n      </ion-button>\n    </div>\n  </div>\n\n</ion-content>\n<ion-footer *ngIf=\"isRegisterEnabled\">\n  <ion-toolbar>\n    <ion-button color=\"dark\" fill=\"clear\" expand=\"block\" routerLink=\"/driver/register\">{{'REGISTER' | translate }}</ion-button>\n  </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");








const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
let LoginPageModule = class LoginPageModule {
};
LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/login/login.page.scss":
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header-md:after {\n  background-image: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vQzpcXFVzZXJzXFxKZXJpY28gUGF1bG9cXFJ1YnltaW5lUHJvamVjdHNcXHRudnNcXG1vYmlsZS9zcmNcXGFwcFxcbG9naW5cXGxvZ2luLnBhZ2Uuc2NzcyIsInNyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksc0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXItbWQ6YWZ0ZXJ7XG4gICAgYmFja2dyb3VuZC1pbWFnZTogbm9uZTtcbiAgfSIsIi5oZWFkZXItbWQ6YWZ0ZXIge1xuICBiYWNrZ3JvdW5kLWltYWdlOiBub25lO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/login/login.page.ts":
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_trip_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/trip.service */ "./src/app/services/trip.service.ts");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/environments/environment.prod */ "./src/environments/environment.prod.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/facebook/ngx */ "./node_modules/@ionic-native/facebook/ngx/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/es2015/index.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");













// import { GooglePlus } from '@ionic-native/google-plus/ngx';
let LoginPage = class LoginPage {
    constructor(authService, translate, commonService, router, menuCtrl, fireAuth, tripService, fb
    // private google: GooglePlus,
    ) {
        this.authService = authService;
        this.translate = translate;
        this.commonService = commonService;
        this.router = router;
        this.menuCtrl = menuCtrl;
        this.fireAuth = fireAuth;
        this.tripService = tripService;
        this.fb = fb;
        this.email = "";
        this.password = "";
        this.isRegisterEnabled = true;
        this.isUser = true;
        this.isDriver = false;
        this.isRegisterEnabled = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_7__["ENABLE_SIGNUP"];
        this.menuCtrl.enable(false);
    }
    ngOnInit() {
        this.isUser = true;
        this.isDriver = false;
        console.log(this.isUser);
    }
    ionViewDidLoad() {
        this.isUser = true;
        this.isDriver = false;
        console.log(this.isUser);
    }
    reset() {
        if (this.email) {
            this.authService.resetPassword(this.email)
                .then(data => this.commonService.showToast('Please Check inbox'))
                .catch(err => this.commonService.showToast(err.message));
        }
        else {
            this.commonService.showToast('Please enter your email.');
        }
    }
    login() {
        if (this.email.length == 0 || this.password.length == 0) {
            this.commonService.showAlert("Invalid Credentials");
        }
        else {
            this.commonService.showLoader('Authenticating...');
            console.log(this.isUser);
            this.authService.login(this.email, this.password).then(authData => {
                if (authData) {
                    this.authService.getUser(authData.user.uid).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_12__["take"])(1)).subscribe((snapshot) => {
                        if (snapshot.key) {
                            this.user = Object.assign({ uid: snapshot.key }, snapshot.payload.val());
                            this.tripService.getTrips().valueChanges().subscribe((trips) => {
                                trips.forEach(trip => {
                                    if (trip.status === 'waiting' || trip.status === 'accepted' || trip.status === 'going') {
                                        this.tripService.setId(trip.key);
                                        this.router.navigateByUrl('/rider/tracking');
                                    }
                                    else if (trip.status === 'finished') {
                                        this.router.navigateByUrl('/rider/home');
                                    }
                                });
                            });
                        }
                        else {
                            this.authService.logout().then(() => {
                                this.commonService.hideLoader();
                                this.commonService.showToast("User doesn't exist. Please try again.");
                            });
                        }
                    });
                }
            }, error => {
                this.commonService.hideLoader();
                this.commonService.showToast(error.message);
            });
        }
    }
    login_fb() {
        this.fb.login(['email'])
            .then((response) => {
            this.onLoginSuccess(response);
            console.log(response.authResponse.accessToken);
        }).catch((error) => {
            console.log(error);
            alert('error:' + error);
        });
    }
    login_google() {
        // let params;
        // if (this.platform.is('android')) {
        //     params = {
        //         'webClientId': '124018728460-sv8cqhnnmnf0jeqbnd0apqbnu6egkhug.apps.googleusercontent.com',
        //         'offline': true
        //     }
        // }
        // else {
        //     params = {}
        // }
        // this.google.login(params)
        //     .then((response) => {
        //         const { idToken, accessToken } = response
        //         this.onLoginSuccess(idToken, accessToken);
        //     }).catch((error) => {
        //     console.log(error)
        //     alert('error:' + JSON.stringify(error))
        // });
    }
    onLoginSuccess(res) {
        // const { token, secret } = res;
        const credential = firebase__WEBPACK_IMPORTED_MODULE_11__["auth"].FacebookAuthProvider.credential(res.authResponse.accessToken);
        this.authService.login_credentials(credential).then(authData => {
            this.commonService.showToast(JSON.stringify(authData.user));
            this.authService.register(authData.user.email, "", authData.user.displayName, "").subscribe(authData2 => {
                this.commonService.hideLoader();
                this.router.navigateByUrl('rider/home');
            }, error => {
                this.commonService.hideLoader();
                this.commonService.showToast(error.message);
            });
        }, error => {
            this.commonService.hideLoader();
            this.commonService.showToast(error.message);
        });
    }
};
LoginPage.ctorParameters = () => [
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] },
    { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"] },
    { type: _services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["MenuController"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_10__["AngularFireAuth"] },
    { type: _services_trip_service__WEBPACK_IMPORTED_MODULE_4__["TripService"] },
    { type: _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_9__["Facebook"] }
];
LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.page.scss */ "./src/app/login/login.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
        _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_5__["CommonService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["MenuController"],
        _angular_fire_auth__WEBPACK_IMPORTED_MODULE_10__["AngularFireAuth"],
        _services_trip_service__WEBPACK_IMPORTED_MODULE_4__["TripService"],
        _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_9__["Facebook"]
        // private google: GooglePlus,
    ])
], LoginPage);



/***/ })

}]);
//# sourceMappingURL=login-login-module-es2015.js.map