(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rider-chat-chat-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/chat/chat.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rider/chat/chat.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\t<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n\t\t<ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Chat</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\t<div id=\"chats\">\n\t\t<div class=\"message\" *ngFor=\"let chat of chats\" [class]=\"chat.specialMessage ? 'message special' : 'message '\">\n\t\t\t<div [class]=\"chat.email == email ? 'chats messageRight' : 'chats messageLeft'\">\n\t\t\t\t<div class=\"username\">{{ chat.name }} </div>\n\t\t\t\t<div class=\"chatMessage\">{{ chat.message }} </div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar>\n\t<ion-row class=\"footer\">\n\t    <ion-col size=\"9\">\n\t   \t\t<ion-input type=\"text\" [(ngModel)]=\"message\" name=\"message\"></ion-input>\n\t   \t</ion-col>\n\t   \t<ion-col size=\"3\">\n\t   \t\t<ion-button (click)=\"sendMessage()\">\n\t\t\t  <ion-icon slot=\"icon-only\" name=\"md-send\"></ion-icon>\n\t\t\t</ion-button>\n\t\t</ion-col>\n\t</ion-row>\n  </ion-toolbar>\n</ion-footer>\n");

/***/ }),

/***/ "./src/app/rider/chat/chat.module.ts":
/*!*******************************************!*\
  !*** ./src/app/rider/chat/chat.module.ts ***!
  \*******************************************/
/*! exports provided: ChatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageModule", function() { return ChatPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _chat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./chat.page */ "./src/app/rider/chat/chat.page.ts");







const routes = [
    {
        path: '',
        component: _chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"]
    }
];
let ChatPageModule = class ChatPageModule {
};
ChatPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"]]
    })
], ChatPageModule);



/***/ }),

/***/ "./src/app/rider/chat/chat.page.scss":
/*!*******************************************!*\
  !*** ./src/app/rider/chat/chat.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".chats {\n  display: inline-block;\n  padding: 5px 10px;\n  background: #3880ff;\n  margin: 5px;\n  color: #ffffff;\n  border-radius: 11px;\n  font-size: 15px;\n}\n\n.username {\n  text-align: right;\n  font-size: 11px;\n  padding-bottom: 7px;\n  color: black;\n}\n\n.messageLeft {\n  float: left;\n  background: #eee;\n  margin-right: 30%;\n}\n\n.messageRight {\n  float: right;\n  margin-left: 30%;\n}\n\n.chats.messageLeft .chatMessage {\n  color: #000;\n}\n\n.footer {\n  width: 100%;\n  margin: 0;\n  padding: 0;\n  background: #fff;\n  color: #000;\n}\n\n.message:after {\n  content: \"\";\n  display: block;\n  clear: both;\n}\n\n.message.special .chats .chatMessage {\n  color: #000;\n  display: inline-block;\n  padding: 0 10px;\n}\n\n.message.special .chats:before {\n  content: \"\";\n  height: 2px;\n  background: #333;\n  display: block;\n  top: 50px;\n  position: absolute;\n  left: 0;\n  width: 100%;\n  z-index: -1;\n}\n\n.message.special .chats {\n  background: transparent;\n  color: #000;\n  font-size: 80%;\n  width: 100%;\n  float: none;\n  text-align: center;\n  position: relative;\n  padding: 3px 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmlkZXIvY2hhdC9DOlxcVXNlcnNcXEplcmljbyBQYXVsb1xcUnVieW1pbmVQcm9qZWN0c1xcdG52c1xcbW9iaWxlL3NyY1xcYXBwXFxyaWRlclxcY2hhdFxcY2hhdC5wYWdlLnNjc3MiLCJzcmMvYXBwL3JpZGVyL2NoYXQvY2hhdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ0NEOztBREVBO0VBQ0MsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FDQ0Q7O0FER0E7RUFDQyxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQ0FEOztBREdBO0VBQ0MsWUFBQTtFQUNBLGdCQUFBO0FDQUQ7O0FER0E7RUFDQyxXQUFBO0FDQUQ7O0FESUE7RUFDQyxXQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUNERDs7QURJQTtFQUNDLFdBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtBQ0REOztBRElBO0VBQ0MsV0FBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtBQ0REOztBRElBO0VBQ0MsV0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxTQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7QUNERDs7QURJQTtFQUNDLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ0REIiwiZmlsZSI6InNyYy9hcHAvcmlkZXIvY2hhdC9jaGF0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jaGF0cyB7XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdHBhZGRpbmc6IDVweCAxMHB4O1xyXG5cdGJhY2tncm91bmQ6ICMzODgwZmY7O1xyXG5cdG1hcmdpbjogNXB4O1xyXG5cdGNvbG9yOiAjZmZmZmZmO1xyXG5cdGJvcmRlci1yYWRpdXM6IDExcHg7XHJcblx0Zm9udC1zaXplOiAxNXB4O1xyXG59XHJcblxyXG4udXNlcm5hbWUge1xyXG5cdHRleHQtYWxpZ246IHJpZ2h0O1xyXG5cdGZvbnQtc2l6ZTogMTFweDtcclxuXHRwYWRkaW5nLWJvdHRvbTogN3B4O1xyXG5cdGNvbG9yOiBibGFjaztcclxufVxyXG5cclxuXHJcbi5tZXNzYWdlTGVmdCB7XHJcblx0ZmxvYXQ6IGxlZnQ7XHJcblx0YmFja2dyb3VuZDogI2VlZTtcclxuXHRtYXJnaW4tcmlnaHQ6IDMwJTtcclxufVxyXG5cclxuLm1lc3NhZ2VSaWdodCB7XHJcblx0ZmxvYXQ6IHJpZ2h0O1xyXG5cdG1hcmdpbi1sZWZ0OiAzMCU7XHJcbn1cclxuXHJcbi5jaGF0cy5tZXNzYWdlTGVmdCAuY2hhdE1lc3NhZ2Uge1xyXG5cdGNvbG9yOiAjMDAwO1xyXG59XHJcblxyXG5cclxuLmZvb3RlciB7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0bWFyZ2luOiAwO1xyXG5cdHBhZGRpbmc6IDA7XHJcblx0YmFja2dyb3VuZDogI2ZmZjtcclxuXHRjb2xvcjogIzAwMDtcclxufVxyXG5cclxuLm1lc3NhZ2U6YWZ0ZXIge1xyXG5cdGNvbnRlbnQ6ICcnO1xyXG5cdGRpc3BsYXk6IGJsb2NrO1xyXG5cdGNsZWFyOiBib3RoO1xyXG59XHJcblxyXG4ubWVzc2FnZS5zcGVjaWFsIC5jaGF0cyAuY2hhdE1lc3NhZ2Uge1xyXG5cdGNvbG9yOiAjMDAwO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRwYWRkaW5nOiAwIDEwcHg7XHJcbn1cclxuXHJcbi5tZXNzYWdlLnNwZWNpYWwgLmNoYXRzOmJlZm9yZSB7XHJcblx0Y29udGVudDogJyc7XHJcblx0aGVpZ2h0OiAycHg7XHJcblx0YmFja2dyb3VuZDogIzMzMztcclxuXHRkaXNwbGF5OiBibG9jaztcclxuXHR0b3A6IDUwcHg7XHJcblx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdGxlZnQ6IDA7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0ei1pbmRleDogLTE7XHJcbn1cclxuXHJcbi5tZXNzYWdlLnNwZWNpYWwgLmNoYXRzIHtcclxuXHRiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuXHRjb2xvcjogIzAwMDtcclxuXHRmb250LXNpemU6IDgwJTtcclxuXHR3aWR0aDogMTAwJTtcclxuXHRmbG9hdDogbm9uZTtcclxuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdHBhZGRpbmc6IDNweCAxMHB4O1xyXG5cdFxyXG59XHJcbiIsIi5jaGF0cyB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcGFkZGluZzogNXB4IDEwcHg7XG4gIGJhY2tncm91bmQ6ICMzODgwZmY7XG4gIG1hcmdpbjogNXB4O1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgYm9yZGVyLXJhZGl1czogMTFweDtcbiAgZm9udC1zaXplOiAxNXB4O1xufVxuXG4udXNlcm5hbWUge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBwYWRkaW5nLWJvdHRvbTogN3B4O1xuICBjb2xvcjogYmxhY2s7XG59XG5cbi5tZXNzYWdlTGVmdCB7XG4gIGZsb2F0OiBsZWZ0O1xuICBiYWNrZ3JvdW5kOiAjZWVlO1xuICBtYXJnaW4tcmlnaHQ6IDMwJTtcbn1cblxuLm1lc3NhZ2VSaWdodCB7XG4gIGZsb2F0OiByaWdodDtcbiAgbWFyZ2luLWxlZnQ6IDMwJTtcbn1cblxuLmNoYXRzLm1lc3NhZ2VMZWZ0IC5jaGF0TWVzc2FnZSB7XG4gIGNvbG9yOiAjMDAwO1xufVxuXG4uZm9vdGVyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMDtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgY29sb3I6ICMwMDA7XG59XG5cbi5tZXNzYWdlOmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGNsZWFyOiBib3RoO1xufVxuXG4ubWVzc2FnZS5zcGVjaWFsIC5jaGF0cyAuY2hhdE1lc3NhZ2Uge1xuICBjb2xvcjogIzAwMDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBwYWRkaW5nOiAwIDEwcHg7XG59XG5cbi5tZXNzYWdlLnNwZWNpYWwgLmNoYXRzOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGhlaWdodDogMnB4O1xuICBiYWNrZ3JvdW5kOiAjMzMzO1xuICBkaXNwbGF5OiBibG9jaztcbiAgdG9wOiA1MHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAtMTtcbn1cblxuLm1lc3NhZ2Uuc3BlY2lhbCAuY2hhdHMge1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6ICMwMDA7XG4gIGZvbnQtc2l6ZTogODAlO1xuICB3aWR0aDogMTAwJTtcbiAgZmxvYXQ6IG5vbmU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBwYWRkaW5nOiAzcHggMTBweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/rider/chat/chat.page.ts":
/*!*****************************************!*\
  !*** ./src/app/rider/chat/chat.page.ts ***!
  \*****************************************/
/*! exports provided: ChatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPage", function() { return ChatPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/es2015/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/es2015/index.js");
/* harmony import */ var _services_trip_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/trip.service */ "./src/app/services/trip.service.ts");





// import { UserService } from 'src/app/services/user.service';
let ChatPage = class ChatPage {
    constructor(afdb, af, tripService) {
        this.afdb = afdb;
        this.af = af;
        this.tripService = tripService;
        this.email = '';
        this.message = '';
        this.allChat = [];
        this.email = this.af.auth.currentUser.email;
        this.name = this.af.auth.currentUser.displayName;
        this.username = this.email.split("@")[0];
        this.getMessage();
    }
    sendMessage() {
        if (this.message) {
            this.allChat.push({
                email: this.email,
                message: this.message,
                name: this.name
            })
                .then(data => {
                this.message = '';
                this.getMessage();
            })
                .catch(error => console.log(error));
        }
    }
    getMessage() {
        this.tripService.getTrips().valueChanges().subscribe((trips) => {
            trips.forEach(trip => {
                if (trip.status === 'waiting' || trip.status === 'accepted' || trip.status === 'going') {
                    this.allChat = this.afdb.list('/chats/' + trip.key + '/');
                    this.allChat.valueChanges().subscribe(data => {
                        this.chats = data;
                    });
                }
            });
        });
    }
    ngOnInit() { }
};
ChatPage.ctorParameters = () => [
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"] },
    { type: _services_trip_service__WEBPACK_IMPORTED_MODULE_4__["TripService"] }
];
ChatPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-chat',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./chat.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/chat/chat.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./chat.page.scss */ "./src/app/rider/chat/chat.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"], _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"], _services_trip_service__WEBPACK_IMPORTED_MODULE_4__["TripService"]])
], ChatPage);



/***/ })

}]);
//# sourceMappingURL=rider-chat-chat-module-es2015.js.map