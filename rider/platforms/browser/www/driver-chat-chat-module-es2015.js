(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["driver-chat-chat-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/driver/chat/chat.page.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/driver/chat/chat.page.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n\t\t<ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Chat</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\t<div id=\"chats\">\n\t\t<div class=\"message\" *ngFor=\"let chat of chats\" [class]=\"chat.specialMessage ? 'message special' : 'message '\">\n\t\t\t<div [class]=\"chat.email == email ? 'chats messageRight' : 'chats messageLeft'\">\n\t\t\t\t<div class=\"username\">{{ chat.name }} </div>\n\t\t\t\t<div class=\"chatMessage\">{{ chat.message }} </div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar>\n\t<ion-row class=\"footer\">\n\t    <ion-col size=\"9\">\n\t   \t\t<ion-input type=\"text\" [(ngModel)]=\"message\" name=\"message\"></ion-input>\n\t   \t</ion-col>\n\t   \t<ion-col size=\"3\">\n\t   \t\t<ion-button (click)=\"sendMessage()\">\n\t\t\t  <ion-icon slot=\"icon-only\" name=\"md-send\"></ion-icon>\n\t\t\t</ion-button>\n\t\t</ion-col>\n\t</ion-row>\n  </ion-toolbar>\n</ion-footer>\n");

/***/ }),

/***/ "./src/app/driver/chat/chat.module.ts":
/*!********************************************!*\
  !*** ./src/app/driver/chat/chat.module.ts ***!
  \********************************************/
/*! exports provided: ChatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageModule", function() { return ChatPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _chat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./chat.page */ "./src/app/driver/chat/chat.page.ts");







const routes = [
    {
        path: '',
        component: _chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"]
    }
];
let ChatPageModule = class ChatPageModule {
};
ChatPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"]]
    })
], ChatPageModule);



/***/ }),

/***/ "./src/app/driver/chat/chat.page.scss":
/*!********************************************!*\
  !*** ./src/app/driver/chat/chat.page.scss ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".chats {\n  display: inline-block;\n  padding: 5px 10px;\n  background: #3880ff;\n  margin: 5px;\n  color: #ffffff;\n  border-radius: 11px;\n  font-size: 15px;\n}\n\n.username {\n  text-align: right;\n  font-size: 11px;\n  padding-bottom: 7px;\n  color: black;\n}\n\n.messageLeft {\n  float: left;\n  background: #eee;\n  margin-right: 30%;\n}\n\n.messageRight {\n  float: right;\n  margin-left: 30%;\n}\n\n.chats.messageLeft .chatMessage {\n  color: #000;\n}\n\n.footer {\n  width: 100%;\n  margin: 0;\n  padding: 0;\n  background: #fff;\n  color: #000;\n}\n\n.message:after {\n  content: \"\";\n  display: block;\n  clear: both;\n}\n\n.message.special .chats .chatMessage {\n  color: #000;\n  display: inline-block;\n  padding: 0 10px;\n}\n\n.message.special .chats:before {\n  content: \"\";\n  height: 2px;\n  background: #333;\n  display: block;\n  top: 50px;\n  position: absolute;\n  left: 0;\n  width: 100%;\n  z-index: -1;\n}\n\n.message.special .chats {\n  background: transparent;\n  color: #000;\n  font-size: 80%;\n  width: 100%;\n  float: none;\n  text-align: center;\n  position: relative;\n  padding: 3px 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZHJpdmVyL2NoYXQvQzpcXFVzZXJzXFxKZXJpY28gUGF1bG9cXFJ1YnltaW5lUHJvamVjdHNcXHRudnNcXG1vYmlsZS9zcmNcXGFwcFxcZHJpdmVyXFxjaGF0XFxjaGF0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvZHJpdmVyL2NoYXQvY2hhdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ0NEOztBREVBO0VBQ0MsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FDQ0Q7O0FER0E7RUFDQyxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQ0FEOztBREdBO0VBQ0MsWUFBQTtFQUNBLGdCQUFBO0FDQUQ7O0FER0E7RUFDQyxXQUFBO0FDQUQ7O0FESUE7RUFDQyxXQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUNERDs7QURJQTtFQUNDLFdBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtBQ0REOztBRElBO0VBQ0MsV0FBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtBQ0REOztBRElBO0VBQ0MsV0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxTQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7QUNERDs7QURJQTtFQUNDLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQ0REIiwiZmlsZSI6InNyYy9hcHAvZHJpdmVyL2NoYXQvY2hhdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2hhdHMge1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHRwYWRkaW5nOiA1cHggMTBweDtcclxuXHRiYWNrZ3JvdW5kOiAjMzg4MGZmOztcclxuXHRtYXJnaW46IDVweDtcclxuXHRjb2xvcjogI2ZmZmZmZjtcclxuXHRib3JkZXItcmFkaXVzOiAxMXB4O1xyXG5cdGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG5cclxuLnVzZXJuYW1lIHtcclxuXHR0ZXh0LWFsaWduOiByaWdodDtcclxuXHRmb250LXNpemU6IDExcHg7XHJcblx0cGFkZGluZy1ib3R0b206IDdweDtcclxuXHRjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcblxyXG4ubWVzc2FnZUxlZnQge1xyXG5cdGZsb2F0OiBsZWZ0O1xyXG5cdGJhY2tncm91bmQ6ICNlZWU7XHJcblx0bWFyZ2luLXJpZ2h0OiAzMCU7XHJcbn1cclxuXHJcbi5tZXNzYWdlUmlnaHQge1xyXG5cdGZsb2F0OiByaWdodDtcclxuXHRtYXJnaW4tbGVmdDogMzAlO1xyXG59XHJcblxyXG4uY2hhdHMubWVzc2FnZUxlZnQgLmNoYXRNZXNzYWdlIHtcclxuXHRjb2xvcjogIzAwMDtcclxufVxyXG5cclxuXHJcbi5mb290ZXIge1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdG1hcmdpbjogMDtcclxuXHRwYWRkaW5nOiAwO1xyXG5cdGJhY2tncm91bmQ6ICNmZmY7XHJcblx0Y29sb3I6ICMwMDA7XHJcbn1cclxuXHJcbi5tZXNzYWdlOmFmdGVyIHtcclxuXHRjb250ZW50OiAnJztcclxuXHRkaXNwbGF5OiBibG9jaztcclxuXHRjbGVhcjogYm90aDtcclxufVxyXG5cclxuLm1lc3NhZ2Uuc3BlY2lhbCAuY2hhdHMgLmNoYXRNZXNzYWdlIHtcclxuXHRjb2xvcjogIzAwMDtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0cGFkZGluZzogMCAxMHB4O1xyXG59XHJcblxyXG4ubWVzc2FnZS5zcGVjaWFsIC5jaGF0czpiZWZvcmUge1xyXG5cdGNvbnRlbnQ6ICcnO1xyXG5cdGhlaWdodDogMnB4O1xyXG5cdGJhY2tncm91bmQ6ICMzMzM7XHJcblx0ZGlzcGxheTogYmxvY2s7XHJcblx0dG9wOiA1MHB4O1xyXG5cdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRsZWZ0OiAwO1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdHotaW5kZXg6IC0xO1xyXG59XHJcblxyXG4ubWVzc2FnZS5zcGVjaWFsIC5jaGF0cyB7XHJcblx0YmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcblx0Y29sb3I6ICMwMDA7XHJcblx0Zm9udC1zaXplOiA4MCU7XHJcblx0d2lkdGg6IDEwMCU7XHJcblx0ZmxvYXQ6IG5vbmU7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHRwYWRkaW5nOiAzcHggMTBweDtcclxuXHRcclxufVxyXG4iLCIuY2hhdHMge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBhZGRpbmc6IDVweCAxMHB4O1xuICBiYWNrZ3JvdW5kOiAjMzg4MGZmO1xuICBtYXJnaW46IDVweDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDExcHg7XG4gIGZvbnQtc2l6ZTogMTVweDtcbn1cblxuLnVzZXJuYW1lIHtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgcGFkZGluZy1ib3R0b206IDdweDtcbiAgY29sb3I6IGJsYWNrO1xufVxuXG4ubWVzc2FnZUxlZnQge1xuICBmbG9hdDogbGVmdDtcbiAgYmFja2dyb3VuZDogI2VlZTtcbiAgbWFyZ2luLXJpZ2h0OiAzMCU7XG59XG5cbi5tZXNzYWdlUmlnaHQge1xuICBmbG9hdDogcmlnaHQ7XG4gIG1hcmdpbi1sZWZ0OiAzMCU7XG59XG5cbi5jaGF0cy5tZXNzYWdlTGVmdCAuY2hhdE1lc3NhZ2Uge1xuICBjb2xvcjogIzAwMDtcbn1cblxuLmZvb3RlciB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDA7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGNvbG9yOiAjMDAwO1xufVxuXG4ubWVzc2FnZTphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBjbGVhcjogYm90aDtcbn1cblxuLm1lc3NhZ2Uuc3BlY2lhbCAuY2hhdHMgLmNoYXRNZXNzYWdlIHtcbiAgY29sb3I6ICMwMDA7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcGFkZGluZzogMCAxMHB4O1xufVxuXG4ubWVzc2FnZS5zcGVjaWFsIC5jaGF0czpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICBoZWlnaHQ6IDJweDtcbiAgYmFja2dyb3VuZDogIzMzMztcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRvcDogNTBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwO1xuICB3aWR0aDogMTAwJTtcbiAgei1pbmRleDogLTE7XG59XG5cbi5tZXNzYWdlLnNwZWNpYWwgLmNoYXRzIHtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIGNvbG9yOiAjMDAwO1xuICBmb250LXNpemU6IDgwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIGZsb2F0OiBub25lO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgcGFkZGluZzogM3B4IDEwcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/driver/chat/chat.page.ts":
/*!******************************************!*\
  !*** ./src/app/driver/chat/chat.page.ts ***!
  \******************************************/
/*! exports provided: ChatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPage", function() { return ChatPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/es2015/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/es2015/index.js");
/* harmony import */ var _services_trip_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/trip.service */ "./src/app/driver/services/trip.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../environments/environment.prod */ "./src/environments/environment.prod.ts");







// import { UserService } from 'src/app/services/user.service';
let ChatPage = class ChatPage {
    constructor(afdb, af, tripDriverService) {
        this.afdb = afdb;
        this.af = af;
        this.tripDriverService = tripDriverService;
        this.email = '';
        this.message = '';
        this.allChat = [];
        this.email = this.af.auth.currentUser.email;
        this.name = this.af.auth.currentUser.displayName;
        this.username = this.email.split("@")[0];
        this.getMessage();
    }
    sendMessage() {
        if (this.message) {
            this.allChat.push({
                email: this.email,
                message: this.message,
                name: this.name
            })
                .then(data => {
                this.message = '';
                this.getMessage();
            })
                .catch(error => console.log(error));
        }
    }
    getMessage() {
        this.tripDriverService.getTrips().valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1)).subscribe((trips) => {
            trips.forEach(trip => {
                if (trip.status == _environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__["TRIP_STATUS_WAITING"] || trip.status == _environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__["TRIP_STATUS_GOING"]) {
                    this.allChat = this.afdb.list('/chats/' + trip.key + '/');
                    this.allChat.valueChanges().subscribe(data => {
                        this.chats = data;
                    });
                }
            });
        });
        this.tripDriverService.getTrips().valueChanges().subscribe((trips) => {
            trips.forEach(trip => {
                if (trip.status === 'waiting' || trip.status === 'accepted' || trip.status === 'going') {
                    this.allChat = this.afdb.list('/chats/' + trip.key + '/');
                    this.allChat.valueChanges().subscribe(data => {
                        this.chats = data;
                    });
                }
            });
        });
    }
    ngOnInit() { }
};
ChatPage.ctorParameters = () => [
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"] },
    { type: _services_trip_service__WEBPACK_IMPORTED_MODULE_4__["TripDriverService"] }
];
ChatPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-chat',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./chat.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/driver/chat/chat.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./chat.page.scss */ "./src/app/driver/chat/chat.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"], _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"], _services_trip_service__WEBPACK_IMPORTED_MODULE_4__["TripDriverService"]])
], ChatPage);



/***/ })

}]);
//# sourceMappingURL=driver-chat-chat-module-es2015.js.map