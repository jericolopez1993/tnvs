(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["driver-wallet-wallet-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/driver/wallet/wallet.page.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/driver/wallet/wallet.page.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>{{'WALLET' | translate}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content class=\"wallet\">\n  <div class=\"widhdraw-header\">\n    <h2 style=\"font-size: 4rem;\">{{currency}} {{ (driver)?.balance }}</h2>\n    <ion-button color=\"light\" (click)=\"withdraw()\">{{'WITHDRAW' | translate}}</ion-button>\n  </div>\n  <ion-list>\n    <ion-item-divider>\n      {{'HISTORY' | translate}}\n    </ion-item-divider>\n    <ion-item *ngFor=\"let record of records\">\n      <ion-label>\n        <ion-text color=\"medium\">\n          <p>{{ record.createdAt | amDateFormat: 'YYYY-MM-DD HH:mm'}}</p>\n        </ion-text>\n        <ion-text>\n          <p>You've request of <strong>{{currency}} {{ record.amount }}</strong>\n            is <code>{{ record.status}}</code>\n          </p>\n          <p>Ref: {{record.createdAt }} / key: {{record.$key}} / Type: {{ record.type }} </p>\n        </ion-text>\n      </ion-label>\n    </ion-item>\n  </ion-list>\n</ion-content>");

/***/ }),

/***/ "./src/app/driver/services/transaction.service.ts":
/*!********************************************************!*\
  !*** ./src/app/driver/services/transaction.service.ts ***!
  \********************************************************/
/*! exports provided: TransactionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionService", function() { return TransactionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/es2015/index.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth.service */ "./src/app/driver/services/auth.service.ts");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment.prod */ "./src/environments/environment.prod.ts");





let TransactionService = class TransactionService {
    constructor(db, authService) {
        this.db = db;
        this.authService = authService;
    }
    getTransactions() {
        let user = this.authService.getUserData();
        console.log(user);
        return this.db.list('transactions', ref => ref.orderByChild('userId').equalTo(user.uid));
    }
    widthDraw(amount, balance) {
        let user = this.authService.getUserData();
        return this.db.list('transactions/').push({
            userId: user.uid,
            name: user.displayName,
            amount: amount,
            createdAt: Date.now(),
            type: src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_4__["TRANSACTION_TYPE_WITHDRAW"],
            status: 'PENDING'
        });
    }
};
TransactionService.ctorParameters = () => [
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }
];
TransactionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"], _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])
], TransactionService);



/***/ }),

/***/ "./src/app/driver/wallet/wallet.module.ts":
/*!************************************************!*\
  !*** ./src/app/driver/wallet/wallet.module.ts ***!
  \************************************************/
/*! exports provided: WalletPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletPageModule", function() { return WalletPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _wallet_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./wallet.page */ "./src/app/driver/wallet/wallet.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var angular2_moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular2-moment */ "./node_modules/angular2-moment/index.js");
/* harmony import */ var angular2_moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(angular2_moment__WEBPACK_IMPORTED_MODULE_8__);









const routes = [
    {
        path: '',
        component: _wallet_page__WEBPACK_IMPORTED_MODULE_6__["WalletPage"]
    }
];
let WalletPageModule = class WalletPageModule {
};
WalletPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            angular2_moment__WEBPACK_IMPORTED_MODULE_8__["MomentModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_wallet_page__WEBPACK_IMPORTED_MODULE_6__["WalletPage"]]
    })
], WalletPageModule);



/***/ }),

/***/ "./src/app/driver/wallet/wallet.page.scss":
/*!************************************************!*\
  !*** ./src/app/driver/wallet/wallet.page.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RyaXZlci93YWxsZXQvd2FsbGV0LnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/driver/wallet/wallet.page.ts":
/*!**********************************************!*\
  !*** ./src/app/driver/wallet/wallet.page.ts ***!
  \**********************************************/
/*! exports provided: WalletPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletPage", function() { return WalletPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_driver_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/driver.service */ "./src/app/driver/services/driver.service.ts");
/* harmony import */ var _services_transaction_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/transaction.service */ "./src/app/driver/services/transaction.service.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment.prod */ "./src/environments/environment.prod.ts");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/common.service */ "./src/app/driver/services/common.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");








let WalletPage = class WalletPage {
    constructor(transactionService, translate, driverService, common, alertCtrl) {
        this.transactionService = transactionService;
        this.translate = translate;
        this.driverService = driverService;
        this.common = common;
        this.alertCtrl = alertCtrl;
        this.currency = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["CURRENCY_SYMBOL"];
        // get transactions from service
        transactionService.getTransactions().valueChanges().subscribe(snapshot => {
            if (snapshot != null)
                this.records = snapshot.reverse();
        });
        this.driverService.getDriver().valueChanges().subscribe(snapshot => {
            this.driver = snapshot;
        });
    }
    withdraw() {
        this.alertCtrl.create({
            header: "Make a Withdraw",
            inputs: [
                { name: 'amount', placeholder: 'Amount' },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Submit',
                    handler: data => {
                        console.log(data);
                        if (parseFloat(data.amount) > parseFloat(this.driver.balance)) {
                            this.common.showAlert("Insufficient Balance");
                        }
                        else {
                            this.transactionService.widthDraw(data.amount, this.driver.balance).then(() => {
                                this.common.showToast('withdraw Requested');
                            });
                        }
                    }
                }
            ]
        }).then(x => x.present());
    }
    ngOnInit() {
    }
};
WalletPage.ctorParameters = () => [
    { type: _services_transaction_service__WEBPACK_IMPORTED_MODULE_3__["TransactionService"] },
    { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"] },
    { type: _services_driver_service__WEBPACK_IMPORTED_MODULE_2__["DriverService"] },
    { type: _services_common_service__WEBPACK_IMPORTED_MODULE_6__["CommonService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] }
];
WalletPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-wallet',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./wallet.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/driver/wallet/wallet.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./wallet.page.scss */ "./src/app/driver/wallet/wallet.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_transaction_service__WEBPACK_IMPORTED_MODULE_3__["TransactionService"],
        _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"],
        _services_driver_service__WEBPACK_IMPORTED_MODULE_2__["DriverService"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_6__["CommonService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]])
], WalletPage);



/***/ })

}]);
//# sourceMappingURL=driver-wallet-wallet-module-es2015.js.map