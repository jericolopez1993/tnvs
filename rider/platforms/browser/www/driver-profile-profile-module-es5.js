function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["driver-profile-profile-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/driver/profile/profile.page.html":
  /*!****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/driver/profile/profile.page.html ***!
    \****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppDriverProfileProfilePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>{{ user.name }}</ion-title>\n    <ion-buttons slot=\"end\">\n\n      <ion-button (click)=\"logout()\">\n        <ion-icon name=\"log-out\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-segment [(ngModel)]=\"tabs\">\n    <ion-segment-button value=\"profile\">\n      {{'BASIC_PROFILE' | translate}}\n    </ion-segment-button>\n    <ion-segment-button value=\"carinfo\">\n      {{'CAR_INFO' | translate }}\n    </ion-segment-button>\n  </ion-segment>\n  <div [ngSwitch]=\"tabs\" class=\"ion-padding\">\n    <div *ngSwitchCase=\"'profile'\">\n      <div style=\"text-align: center;\">\n        <img src=\"{{ user.photoURL }}\" onError=\"this.src='assets/img/default-dp.png'\" style=\"width:50px;height:50px;border-radius:100px\" (click)=\"chooseFile()\">\n        <form ngNoForm>\n          <input id=\"avatar\" name=\"file\" type=\"file\" (change)=\"upload()\">\n        </form>\n      </div>\n      <ion-list lines=\"none\">\n        <ion-item>\n          <ion-label position=\"stacked\" color=\"primary\">{{'FIRST_NAME' | translate}}</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"user.first_name\"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\" color=\"primary\">{{'MIDDLE_NAME' | translate}}</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"user.middle_name\"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\" color=\"primary\">{{'LAST_NAME' | translate}}</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"user.last_name\"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\" color=\"primary\">{{'BIRTH_DATE' | translate}}</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"user.birth_date\"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\" color=\"primary\">{{'GENDER' | translate}}</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"user.gender\"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\" color=\"primary\">{{'ADDRESS' | translate}}</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"user.address\"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\">{{'EMAIL_ADDRESS' | translate}}</ion-label>\n          <ion-input type=\"email\" [(ngModel)]=\"user.email\" disabled placeholder=\"{{'EMAIL_ADDRESS' | translate}}\">\n          </ion-input>\n          <ion-button ion-button item-right clear *ngIf=\"!user.isEmailVerified\" (click)=\"verifyEmail()\">\n            {{'VERIFY' | translate}}</ion-button>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\">{{'PHONE_NUMBER' | translate}}</ion-label>\n          <ion-input type=\"tel\" [(ngModel)]=\"user.phoneNumber\" [disabled]=\"user.isPhoneVerified\"\n            placeholder=\"{{'PHONE_NUMBER' | translate}}\"></ion-input>\n        </ion-item>\n      </ion-list>\n    </div>\n    <div *ngSwitchCase=\"'carinfo'\">\n      <ion-list lines=\"none\">\n        <ion-item>\n          <ion-label position=\"stacked\">{{'CAR_BRAND' | translate}}</ion-label>\n          <span>{{user.brand}}</span>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\">{{'CAR_MODEL' | translate}}</ion-label>\n          <span>{{user.brand}}</span>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\">{{'PLATE_NUMBER' | translate}}</ion-label>\n          <span>{{user.plate}}</span>\n        </ion-item>\n        <ion-item *ngIf=\"types\">\n          <ion-label position=\"stacked\">{{'CAR_TYPE' | translate}}</ion-label>\n          <span>{{user.type}}</span>\n        </ion-item>\n      </ion-list>\n    </div>\n  </div>\n</ion-content>\n<ion-footer class=\"ion-padding\">\n  <ion-button expand=\"block\" (click)=\"save()\">{{'SAVE' | translate}}</ion-button>\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/driver/profile/profile.module.ts":
  /*!**************************************************!*\
    !*** ./src/app/driver/profile/profile.module.ts ***!
    \**************************************************/

  /*! exports provided: ProfilePageModule */

  /***/
  function srcAppDriverProfileProfileModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function () {
      return ProfilePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./profile.page */
    "./src/app/driver/profile/profile.page.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");

    var routes = [{
      path: '',
      component: _profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]
    }];

    var ProfilePageModule = function ProfilePageModule() {
      _classCallCheck(this, ProfilePageModule);
    };

    ProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]]
    })], ProfilePageModule);
    /***/
  },

  /***/
  "./src/app/driver/profile/profile.page.scss":
  /*!**************************************************!*\
    !*** ./src/app/driver/profile/profile.page.scss ***!
    \**************************************************/

  /*! exports provided: default */

  /***/
  function srcAppDriverProfileProfilePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2RyaXZlci9wcm9maWxlL3Byb2ZpbGUucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/driver/profile/profile.page.ts":
  /*!************************************************!*\
    !*** ./src/app/driver/profile/profile.page.ts ***!
    \************************************************/

  /*! exports provided: ProfilePage */

  /***/
  function srcAppDriverProfileProfilePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ProfilePage", function () {
      return ProfilePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/environments/environment.prod */
    "./src/environments/environment.prod.ts");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../services/auth.service */
    "./src/app/driver/services/auth.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _services_setting_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../services/setting.service */
    "./src/app/driver/services/setting.service.ts");
    /* harmony import */


    var _services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../services/common.service */
    "./src/app/driver/services/common.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/fire/storage */
    "./node_modules/@angular/fire/storage/es2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var ProfilePage =
    /*#__PURE__*/
    function () {
      function ProfilePage(authService, settingService, common, platform, translate, router, afStorage) {
        _classCallCheck(this, ProfilePage);

        this.authService = authService;
        this.settingService = settingService;
        this.common = common;
        this.platform = platform;
        this.translate = translate;
        this.router = router;
        this.afStorage = afStorage;
        this.user = {};
        this.support = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["CUSTOMER_CARE"];
        this.tripCount = 0;
        this.totalEarning = 0;
        this.rating = 5;
        this.types = [];
        this.tabs = 'profile';
      }

      _createClass(ProfilePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          // this.user = this.authService.getUser(this.authService.getUserData().uid);
          this.authService.getUser(this.authService.getUserData().uid).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["take"])(1)).subscribe(function (snapshot) {
            console.log(snapshot);
            _this.user = snapshot;
          });
          this.currency = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["CURRENCY_SYMBOL"];
          this.settingService.getVehicleType().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["take"])(1)).subscribe(function (snapshot) {
            if (snapshot === null) {
              _this.settingService.getDefaultVehicleType().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["take"])(1)).subscribe(function (snapshot) {
                console.log(snapshot);
                _this.types = Object.keys(snapshot).map(function (key) {
                  return snapshot[key];
                });
              });
            } else {
              _this.types = Object.keys(snapshot).map(function (key) {
                return snapshot[key];
              });
            }
          });
          console.log(this.user);
        }
      }, {
        key: "save",
        value: function save() {
          var _this2 = this;

          console.log(this.user);
          this.authService.getUser(this.user.uid).update(this.user).then(function (data) {
            _this2.common.showToast("Updated successfully");

            _this2.router.navigateByUrl('/driver/home');
          });
        }
      }, {
        key: "chooseFile",
        value: function chooseFile() {
          document.getElementById('avatar').click();
        }
      }, {
        key: "upload",
        value: function upload() {
          var _this3 = this;

          // Create a root reference
          this.common.showLoader('Uploading..');

          var _loop = function _loop() {
            var selectedFile = _arr[_i];
            var path = '/users/' + Date.now() + "_".concat(selectedFile.name);

            var ref = _this3.afStorage.ref(path);

            ref.put(selectedFile).then(function () {
              ref.getDownloadURL().subscribe(function (data) {
                _this3.user.photoURL = data;
              });

              _this3.common.hideLoader();
            }).catch(function (err) {
              _this3.common.hideLoader();

              console.log(err);
            });
          };

          for (var _i = 0, _arr = [document.getElementById('avatar').files[0]]; _i < _arr.length; _i++) {
            _loop();
          }
        } // code for uploading licence image

      }, {
        key: "chooseDocs",
        value: function chooseDocs() {
          document.getElementById('docsPDF').click();
        }
      }, {
        key: "uploadDocs",
        value: function uploadDocs() {
          var _this4 = this;

          this.common.showLoader('Uploading..');

          var _loop2 = function _loop2() {
            var selectedFile = _arr2[_i2];
            console.log(selectedFile.name);
            var path = '/users/' + Date.now() + "".concat(selectedFile.name);

            var ref = _this4.afStorage.ref(path);

            ref.put(selectedFile).then(function () {
              ref.getDownloadURL().subscribe(function (url) {
                return _this4.user.docsURL = url;
              });

              _this4.common.hideLoader();
            }).catch(function (err) {
              _this4.common.hideLoader();

              console.log(err);
            });
          };

          for (var _i2 = 0, _arr2 = [document.getElementById('docsPDF').files[0]]; _i2 < _arr2.length; _i2++) {
            _loop2();
          }
        }
      }, {
        key: "logout",
        value: function logout() {
          var _this5 = this;

          this.authService.logout().then(function () {
            return _this5.router.navigateByUrl('/driver/login', {
              skipLocationChange: true
            });
          });
        }
      }, {
        key: "verifyEmail",
        value: function verifyEmail() {
          this.authService.sendVerification();
        }
      }]);

      return ProfilePage;
    }();

    ProfilePage.ctorParameters = function () {
      return [{
        type: _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
      }, {
        type: _services_setting_service__WEBPACK_IMPORTED_MODULE_6__["SettingService"]
      }, {
        type: _services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"]
      }, {
        type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_9__["AngularFireStorage"]
      }];
    };

    ProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-profile',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./profile.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/driver/profile/profile.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./profile.page.scss */
      "./src/app/driver/profile/profile.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"], _services_setting_service__WEBPACK_IMPORTED_MODULE_6__["SettingService"], _services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"], _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"], _angular_fire_storage__WEBPACK_IMPORTED_MODULE_9__["AngularFireStorage"]])], ProfilePage);
    /***/
  },

  /***/
  "./src/app/driver/services/setting.service.ts":
  /*!****************************************************!*\
    !*** ./src/app/driver/services/setting.service.ts ***!
    \****************************************************/

  /*! exports provided: SettingService */

  /***/
  function srcAppDriverServicesSettingServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SettingService", function () {
      return SettingService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/fire/database */
    "./node_modules/@angular/fire/database/es2015/index.js");
    /* harmony import */


    var _place_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./place.service */
    "./src/app/driver/services/place.service.ts");

    var SettingService =
    /*#__PURE__*/
    function () {
      function SettingService(db, placeService) {
        _classCallCheck(this, SettingService);

        this.db = db;
        this.placeService = placeService;
      }

      _createClass(SettingService, [{
        key: "getVehicleType",
        value: function getVehicleType() {
          return this.db.object('master_settings/prices/' + this.placeService.getLocality() + '/vehicles').valueChanges();
        }
      }, {
        key: "getDefaultVehicleType",
        value: function getDefaultVehicleType() {
          return this.db.object('master_settings/prices/default/vehicles').valueChanges();
        }
      }]);

      return SettingService;
    }();

    SettingService.ctorParameters = function () {
      return [{
        type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]
      }, {
        type: _place_service__WEBPACK_IMPORTED_MODULE_3__["PlaceService"]
      }];
    };

    SettingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"], _place_service__WEBPACK_IMPORTED_MODULE_3__["PlaceService"]])], SettingService);
    /***/
  }
}]);
//# sourceMappingURL=driver-profile-profile-module-es5.js.map