function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["driver-login-login-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/driver/login/login.page.html":
  /*!************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/driver/login/login.page.html ***!
    \************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppDriverLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-content>\n  <div class=\"login-header\">\n    <img src=\"./assets/img/logo-2.png\" />\n    <h2 ion-text color=\"light\">{{'DRIVER' | translate}}</h2>\n  </div>\n  <div class=\"ion-padding\">\n    <ion-list lines=\"none\">\n      <ion-item>\n        <ion-label position=\"stacked\">{{'EMAIL_ADDRESS'| translate}}</ion-label>\n        <ion-input type=\"text\" [(ngModel)]=\"userInfo.email\"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label position=\"stacked\">{{'PASSWORD'| translate}}</ion-label>\n        <ion-input type=\"password\" [(ngModel)]=\"userInfo.password\"></ion-input>\n      </ion-item>\n    </ion-list>\n    <div class=\"ion-padding\">\n      <ion-button color=\"dark\" expand=\"block\" (click)=\"login()\">{{'LOGIN' | translate}}</ion-button>\n      <ion-button expand=\"block\" color=\"medium\" fill=\"clear\" (click)=\"reset()\">{{'FORGOT' | translate}}</ion-button>\n\n      <ion-button color=\"medium\" expand=\"block\" fill=\"clear\" slot=\"end\" routerLink=\"/login\">\n        {{'ARE_YOU_RIDER' | translate }}\n      </ion-button>\n    </div>\n  </div>\n</ion-content>\n<ion-footer *ngIf=\"isRegisterEnabled\">\n  <ion-toolbar>\n    <ion-button color=\"dark\" fill=\"clear\" expand=\"block\" routerLink=\"/driver/register\">{{'REGISTER' | translate}}</ion-button>\n  </ion-toolbar>\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/driver/login/login.module.ts":
  /*!**********************************************!*\
    !*** ./src/app/driver/login/login.module.ts ***!
    \**********************************************/

  /*! exports provided: LoginPageModule */

  /***/
  function srcAppDriverLoginLoginModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
      return LoginPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./login.page */
    "./src/app/driver/login/login.page.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");

    var routes = [{
      path: '',
      component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }];

    var LoginPageModule = function LoginPageModule() {
      _classCallCheck(this, LoginPageModule);
    };

    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })], LoginPageModule);
    /***/
  },

  /***/
  "./src/app/driver/login/login.page.scss":
  /*!**********************************************!*\
    !*** ./src/app/driver/login/login.page.scss ***!
    \**********************************************/

  /*! exports provided: default */

  /***/
  function srcAppDriverLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".header-md:after {\n  background-image: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZHJpdmVyL2xvZ2luL0M6XFxVc2Vyc1xcSmVyaWNvIFBhdWxvXFxSdWJ5bWluZVByb2plY3RzXFx0bnZzXFxtb2JpbGUvc3JjXFxhcHBcXGRyaXZlclxcbG9naW5cXGxvZ2luLnBhZ2Uuc2NzcyIsInNyYy9hcHAvZHJpdmVyL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHNCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9kcml2ZXIvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmhlYWRlci1tZDphZnRlcntcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBub25lO1xuICB9IiwiLmhlYWRlci1tZDphZnRlciB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IG5vbmU7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/driver/login/login.page.ts":
  /*!********************************************!*\
    !*** ./src/app/driver/login/login.page.ts ***!
    \********************************************/

  /*! exports provided: LoginPage */

  /***/
  function srcAppDriverLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
      return LoginPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/environments/environment.prod */
    "./src/environments/environment.prod.ts");
    /* harmony import */


    var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../services/common.service */
    "./src/app/driver/services/common.service.ts");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../services/auth.service */
    "./src/app/driver/services/auth.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _services_trip_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../services/trip.service */
    "./src/app/driver/services/trip.service.ts");
    /* harmony import */


    var _services_driver_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../services/driver.service */
    "./src/app/driver/services/driver.service.ts");

    var LoginPage =
    /*#__PURE__*/
    function () {
      function LoginPage(translate, common, auth, router, menuCtrl, tripDriverService, driverService) {
        _classCallCheck(this, LoginPage);

        this.translate = translate;
        this.common = common;
        this.auth = auth;
        this.router = router;
        this.menuCtrl = menuCtrl;
        this.tripDriverService = tripDriverService;
        this.driverService = driverService;
        this.userInfo = {};
        this.isRegisterEnabled = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["ENABLE_SIGNUP"];
        this.isUser = false;
        this.isDriver = true;
        this.menuCtrl.enable(false);
      }

      _createClass(LoginPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "login",
        value: function login() {
          var _this = this;

          if (this.userInfo.email.length == 0 || this.userInfo.password.length == 0) {
            this.common.showAlert("Invalid Credentials");
          } else {
            this.common.showLoader('Authenticating...');
            this.auth.login(this.userInfo.email, this.userInfo.password).then(function (authData) {
              if (authData) {
                _this.auth.getUser(authData.user.uid).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["take"])(1)).subscribe(function (snapshot) {
                  console.log(snapshot);

                  if (snapshot.key) {
                    _this.driver = Object.assign({
                      uid: snapshot.key
                    }, snapshot.payload.val());
                    var root = 'driver/home'; // check for uncompleted trip

                    _this.tripDriverService.getTrips().valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["take"])(1)).subscribe(function (trips) {
                      console.log(trips);
                      trips.forEach(function (trip) {
                        if (trip.status == src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["TRIP_STATUS_WAITING"] || trip.status == src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["TRIP_STATUS_GOING"]) {
                          _this.tripDriverService.setCurrentTrip(trip.key);

                          root = 'driver/pickup';
                        }
                      });
                      _this.driver = _this.auth.getUserData();

                      _this.driverService.setUser(_this.driver);

                      _this.driverService.getDriver().valueChanges().subscribe(function (snapshot) {
                        _this.driver = snapshot;
                      });

                      _this.router.navigateByUrl(root);
                    });
                  } else {
                    _this.auth.logout().then(function () {
                      _this.common.hideLoader();

                      _this.common.showToast("Driver doesn't exist. Please try again.");
                    });
                  }
                });
              }
            }, function (error) {
              _this.common.hideLoader();

              _this.common.showToast(error.message);
            });
          }
        }
      }, {
        key: "reset",
        value: function reset() {
          var _this2 = this;

          if (this.userInfo.email) {
            this.auth.reset(this.userInfo.email).then(function (data) {
              return _this2.common.showToast('Please Check your inbox');
            }).catch(function (err) {
              return _this2.common.showToast(err.message);
            });
          } else this.common.showToast("Please Enter Email Address");
        }
      }]);

      return LoginPage;
    }();

    LoginPage.ctorParameters = function () {
      return [{
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"]
      }, {
        type: _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"]
      }, {
        type: _services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"]
      }, {
        type: _services_trip_service__WEBPACK_IMPORTED_MODULE_9__["TripDriverService"]
      }, {
        type: _services_driver_service__WEBPACK_IMPORTED_MODULE_10__["DriverService"]
      }];
    };

    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-login',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./login.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/driver/login/login.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./login.page.scss */
      "./src/app/driver/login/login.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"], _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"], _services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"], _services_trip_service__WEBPACK_IMPORTED_MODULE_9__["TripDriverService"], _services_driver_service__WEBPACK_IMPORTED_MODULE_10__["DriverService"]])], LoginPage);
    /***/
  }
}]);
//# sourceMappingURL=driver-login-login-module-es5.js.map