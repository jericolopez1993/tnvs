(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rider-tracking-tracking-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/tracking/tracking.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rider/tracking/tracking.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>{{'ON_THE_WAY' | translate}}</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button *ngIf=\"tripStatus == 'waiting'\" (click)=\"cancelTrip()\">{{'CANCEL_TRIP' | translate}}\n      </ion-button>\n      <ion-button fill=\"outline\" color=\"danger\" href=\"tel:{{sos}}\" *ngIf=\"tripStatus == 'going'\">{{'SOS' | translate}}\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div id=\"map\" style=\"height:100%\"></div>\n</ion-content>\n<ion-footer>\n\n\n  <div style=\"text-align:right; color: #222\">\n    <span style=\"background:#ffff00\">{{ 'OTP' | translate}}: {{ (trip)?.otp }}</span>\n  </div>\n  <ion-item>\n    <ion-avatar slot=\"start\">\n      <img src=\"{{ (driver)?.photoURL }}\" onError=\"this.src='assets/img/default-dp.png'\" />\n    </ion-avatar>\n    <ion-label>\n      <ion-text>\n        <h2>{{ (driver)?.name }} &nbsp; {{(driver)?.rating}} <ion-icon name=\"md-star\" color=\"yellow\"></ion-icon>\n        </h2>\n      </ion-text>\n      <ion-text>\n        <p>{{ (driver)?.plate }} &middot; {{ (driver)?.brand }}</p>\n      </ion-text>\n    </ion-label>\n    <ion-button slot=\"end\" href=\"tel: {{ (driver)?.phoneNumber }} \">\n      <ion-icon name=\"call\"></ion-icon>&nbsp;{{'CALL' | translate}}\n    </ion-button>\n    <ion-button color=\"dark\" slot=\"end\"  (click)=\"showChat()\">\n      <ion-icon name=\"chatbubbles\"></ion-icon>&nbsp;Message\n    </ion-button>\n\n  </ion-item>\n  <ion-button color=\"dark\" expand=\"block\" (click)=\"showRateCard()\">{{'SHOW_RATE_CARD' | translate}}\n  </ion-button>\n</ion-footer>");

/***/ }),

/***/ "./src/app/rider/tracking/tracking.module.ts":
/*!***************************************************!*\
  !*** ./src/app/rider/tracking/tracking.module.ts ***!
  \***************************************************/
/*! exports provided: TrackingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrackingPageModule", function() { return TrackingPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _tracking_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tracking.page */ "./src/app/rider/tracking/tracking.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");








const routes = [
    {
        path: '',
        component: _tracking_page__WEBPACK_IMPORTED_MODULE_6__["TrackingPage"]
    }
];
let TrackingPageModule = class TrackingPageModule {
};
TrackingPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_tracking_page__WEBPACK_IMPORTED_MODULE_6__["TrackingPage"]]
    })
], TrackingPageModule);



/***/ }),

/***/ "./src/app/rider/tracking/tracking.page.scss":
/*!***************************************************!*\
  !*** ./src/app/rider/tracking/tracking.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3JpZGVyL3RyYWNraW5nL3RyYWNraW5nLnBhZ2Uuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/rider/tracking/tracking.page.ts":
/*!*************************************************!*\
  !*** ./src/app/rider/tracking/tracking.page.ts ***!
  \*************************************************/
/*! exports provided: TrackingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrackingPage", function() { return TrackingPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment.prod */ "./src/environments/environment.prod.ts");
/* harmony import */ var _services_driver_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/driver.service */ "./src/app/services/driver.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_trip_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/trip.service */ "./src/app/services/trip.service.ts");
/* harmony import */ var _services_place_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/place.service */ "./src/app/services/place.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");









let TrackingPage = class TrackingPage {
    constructor(driverService, tripService, placeService, router, menuCtrl) {
        this.driverService = driverService;
        this.tripService = tripService;
        this.placeService = placeService;
        this.router = router;
        this.menuCtrl = menuCtrl;
        this.trip = {};
        this.alertCnt = 0;
        this.rate = 5;
        this.sos = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["SOS"];
    }
    ngOnInit() {
    }
    ionViewDidEnter() {
        this.menuCtrl.enable(true);
        let tripId = this.tripService.getId();
        this.tripService.getTrip(tripId).valueChanges().subscribe((snapshot) => {
            if (snapshot != null) {
                console.log(this.trip);
                this.trip = snapshot;
                console.log(this.trip);
                this.driverService.getDriver(this.trip.driverId).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["take"])(1)).subscribe(snap => {
                    console.log(snap);
                    this.driver = snap;
                    this.watchTrip(tripId);
                    // init map
                    this.loadMap();
                });
            }
        });
    }
    ionViewWillLeave() {
        clearInterval(this.driverTracking);
    }
    watchTrip(tripId) {
        this.tripService.getTrip(tripId).valueChanges().subscribe((snapshot) => {
            this.tripStatus = snapshot.status;
        });
    }
    showRateCard() {
        let final = this.trip.fee - (this.trip.fee * (parseInt(this.trip.discount) / 100));
        this.trip.final = final;
        this.router.navigate(['rating'], {
            queryParams: {
                trip: JSON.stringify(this.trip),
                driver: JSON.stringify(this.driver)
            }
        });
    }
    loadMap() {
        console.log("load Map calling");
        let latLng = new google.maps.LatLng(this.trip.origin.location.lat, this.trip.origin.location.lng);
        let mapOptions = {
            center: latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            streetViewControl: false
        };
        this.map = new google.maps.Map(document.getElementById("map"), mapOptions);
        this.marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: latLng,
            icon: {
                url: 'assets/img/map-suv.png'
            }
        });
        this.trackDriver();
    }
    // make array with range is n
    range(n) {
        return new Array(Math.round(n));
    }
    trackDriver() {
        // this.showDriverOnMap();
        this.driverTracking = setInterval(() => {
            this.marker.setMap(null);
            this.showDriverOnMap();
        }, src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["POSITION_INTERVAL"]);
        console.log(src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["POSITION_INTERVAL"]);
    }
    cancelTrip() {
        this.tripService.cancelTrip(this.trip.key).then(data => {
            console.log(data);
            this.router.navigateByUrl('/rider/home');
        });
    }
    // show user on map
    showDriverOnMap() {
        // get user's position
        this.driverService.getDriverPosition(this.placeService.getLocality(), this.driver.type, this.driver.uid).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["take"])(1)).subscribe((snapshot) => {
            // create or update
            console.log(snapshot);
            let latLng = new google.maps.LatLng(snapshot.lat, snapshot.lng);
            if (this.tripStatus == src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["TRIP_STATUS_GOING"]) {
                console.log(this.tripStatus);
                this.map.setCenter(latLng);
            }
            // show vehicle to map
            this.marker = new google.maps.Marker({
                map: this.map,
                position: latLng,
                icon: {
                    url: 'assets/img/map-suv.png',
                    size: new google.maps.Size(32, 32),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(16, 16),
                    scaledSize: new google.maps.Size(32, 32)
                },
            });
        });
    }
    showChat() {
        this.router.navigateByUrl('/rider/chat');
    }
};
TrackingPage.ctorParameters = () => [
    { type: _services_driver_service__WEBPACK_IMPORTED_MODULE_3__["DriverService"] },
    { type: _services_trip_service__WEBPACK_IMPORTED_MODULE_5__["TripService"] },
    { type: _services_place_service__WEBPACK_IMPORTED_MODULE_6__["PlaceService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] }
];
TrackingPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-tracking',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./tracking.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/tracking/tracking.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./tracking.page.scss */ "./src/app/rider/tracking/tracking.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_driver_service__WEBPACK_IMPORTED_MODULE_3__["DriverService"],
        _services_trip_service__WEBPACK_IMPORTED_MODULE_5__["TripService"],
        _services_place_service__WEBPACK_IMPORTED_MODULE_6__["PlaceService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"]])
], TrackingPage);



/***/ })

}]);
//# sourceMappingURL=rider-tracking-tracking-module-es2015.js.map