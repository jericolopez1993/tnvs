import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../services/auth.service';
import { TripService } from '../services/trip.service';
import { CommonService } from '../services/common.service';
import { Router } from '@angular/router';
import { ENABLE_SIGNUP } from 'src/environments/environment.prod';
import { MenuController } from '@ionic/angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { AngularFireAuth } from '@angular/fire/auth';

import * as firebase from 'firebase';
import {take} from "rxjs/operators";
// import { GooglePlus } from '@ionic-native/google-plus/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    user: any;
    email: string = "";
    password: string = "";
    isRegisterEnabled: any = true;
    isUser: any = true;
    isDriver: any = false;

  ngOnInit() {
      this.isUser = true;
      this.isDriver = false;
  }
  constructor(
    private authService: AuthService,
    private translate: TranslateService,
    private commonService: CommonService,
    private router: Router,
    private menuCtrl: MenuController,
    private fireAuth: AngularFireAuth,
    private tripService: TripService,
    private fb: Facebook
    // private google: GooglePlus,

  ) {
    this.isRegisterEnabled = ENABLE_SIGNUP;
    this.menuCtrl.enable(false);
  }



    ionViewDidLoad() {
        this.isUser = true;
        this.isDriver = false;
    }



    reset() {
    if (this.email) {
      this.authService.resetPassword(this.email)
        .then(data => this.commonService.showToast('Please Check inbox'))
        .catch(err => this.commonService.showToast(err.message));
    }else{
        this.commonService.showToast('Please enter your email.');
    }
  }

  login() {
    if (this.email.length == 0 || this.password.length == 0) {
      this.commonService.showAlert("Invalid Credentials");
    }
    else {
      this.commonService.showLoader('Authenticating...');
      this.authService.login(this.email, this.password).then(authData => {
          if (authData) {
              this.authService.getUser(authData.user.uid).snapshotChanges().pipe(take(1)).subscribe((snapshot: any) => {
                  if (snapshot.key) {
                      this.user = {uid: snapshot.key, ...snapshot.payload.val()};
                      this.tripService.getTrips().valueChanges().subscribe((trips: any) => {
                          if (trips && trips != undefined && trips.length != 0) {
                              trips.forEach(trip => {
                                  if (trip.status === 'waiting' || trip.status === 'accepted' || trip.status === 'going') {
                                      this.tripService.setId(trip.key)
                                      this.router.navigateByUrl('tracking');
                                  } else if (trip.status === 'finished') {
                                      this.router.navigateByUrl('home');
                                  }
                              })
                          }else{
                              this.router.navigateByUrl('home');
                          }
                      })
                  }else{
                      this.authService.logout().then(() => {
                          this.commonService.hideLoader();
                          this.commonService.showToast("User doesn't exist. Please try again.");
                      });
                  }
              });
          }
      }, error => {
        this.commonService.hideLoader();
        this.commonService.showToast(error.message);
      });
    }

  }

  login_fb() {
      this.fb.login(['email'])
          .then((response: FacebookLoginResponse) => {
              this.onLoginSuccess(response);
          }).catch((error) => {
          console.log(error)
          alert('error:' + error)
      });
  }

  login_google(){
      // let params;
      // if (this.platform.is('android')) {
      //     params = {
      //         'webClientId': '124018728460-sv8cqhnnmnf0jeqbnd0apqbnu6egkhug.apps.googleusercontent.com',
      //         'offline': true
      //     }
      // }
      // else {
      //     params = {}
      // }
      // this.google.login(params)
      //     .then((response) => {
      //         const { idToken, accessToken } = response
      //         this.onLoginSuccess(idToken, accessToken);
      //     }).catch((error) => {
      //     console.log(error)
      //     alert('error:' + JSON.stringify(error))
      // });
  }
    onLoginSuccess(res: FacebookLoginResponse) {
        // const { token, secret } = res;
        const credential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
        this.authService.login_credentials(credential).then(authData => {
            this.commonService.showToast(JSON.stringify(authData.user));
            // this.authService.register(authData.user.email, "", authData.user.displayName, "").subscribe(authData2 => {
            //     this.commonService.hideLoader();
            //     this.router.navigateByUrl('home');
            // }, error => {
            //     this.commonService.hideLoader();
            //     this.commonService.showToast(error.message);
            // });
        }, error => {
            this.commonService.hideLoader();
            this.commonService.showToast(error.message);
        });

    }
}
