import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { TripService } from '../services/trip.service';
// import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  email:string = '';
    username: string;
    name: string;
  message:string = '';
  allChat: any = [];
  chats: any;


  constructor(private afdb: AngularFireDatabase, private af: AngularFireAuth, private tripService: TripService,) {
  	 this.email = this.af.auth.currentUser.email;
      this.name = this.af.auth.currentUser.displayName;
  	 this.username = this.email.split("@")[0];

  	 this.getMessage();
  }

  sendMessage() {
    if(this.message){
      this.allChat.push({
        email: this.email,
        message: this.message,
        name: this.name
      })
      .then( data => {
        this.message = '';
        this.getMessage();
      })
      .catch(error => console.log(error))
    }
  	
  }

  getMessage() {
      this.tripService.getTrips().valueChanges().subscribe((trips: any) => {
          trips.forEach(trip => {
              if (trip.status === 'waiting' || trip.status === 'accepted' || trip.status === 'going') {
                  this.allChat = this.afdb.list('/chats/' + trip.key + '/');
                  this.allChat.valueChanges().subscribe( data => {
                      this.chats = data;
                  });
              }
          })
      });
  }

  ngOnInit(){}

  /* ngOnInit() {
  	this.allChat.push({
  		specialMessage: true,
  		message: `${this.username} has joined the conversation`
  	});
  	

  }

  ionViewWillLeave() {
  	this.allChat.push({
  		specialMessage: true,
  		message: `${this.username} has left the conversation`
  	});
  }
 */
}
