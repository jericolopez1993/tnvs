import { Component,ViewChildren, QueryList  } from '@angular/core';

import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { take } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { TripService } from './services/trip.service';
import { IonRouterOutlet } from '@ionic/angular';


@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {
    user: any = {};
    driver: any = {};
    isUser: any = false;
    isDriver: any = false;
    public driverPages = [
        {
            title: 'Home',
            url: '/driver/home',
            icon: 'home'
        },
        {
            title: 'Profile',
            url: '/driver/profile',
            icon: 'contact'
        },
        {
            title: 'Ride History',
            url: '/driver/history',
            icon: 'time'
        },
        {
            title: 'Wallet',
            url: '/driver/wallet',
            icon: 'wallet'
        },
        {
            title: 'Settings',
            url: '/driver/settings',
            icon: 'settings'
        }

    ];
    public riderPages = [
        {
            title: 'Home',
            url: 'home',
            icon: 'home'
        },
        {
            title: 'Ride History',
            url: 'history',
            icon: 'time'
        },
        {
            title: 'Profile',
            url: 'profile',
            icon: 'contact'
        }

    ];
    @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private translate: TranslateService,
        private afAuth: AngularFireAuth,
        private router: Router,
        private authService: AuthService,
        private alertCtrl: AlertController,
        private tripService: TripService
    ) {
        this.initializeApp();
        this.backButtonEvent();
    }
    getProfileUrl(){
        if (this.user.driver){
            return '/driver/profile'
        }else{
            return 'profile'
        }
    }
    backButtonEvent() {
        this.platform.backButton.subscribe(async () => {

            this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
                if (this.router.url === '/home' || this.router.url === '/tracking') {
                    this.alertCtrl.create({
                        header: 'Exit eShip',
                        message: "Are you sure?",
                        buttons: [
                            {
                                text: 'No',
                                handler: () => {
                                }
                            },
                            {
                                text: 'Yes',
                                handler: () => {
                                    navigator['app'].exitApp();
                                }
                            }
                        ]
                    }).then(r => r.present());
                } else if (outlet && outlet.canGoBack()) {
                    outlet.pop();
                }
            });
        });
    }
    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.translate.setDefaultLang('en');
            this.translate.use('en');
            this.afAuth.authState.subscribe(authData => this.user = authData);
            console.log('this.router.url', this.router.url);
            this.afAuth.authState.pipe(take(1)).subscribe(authData => {
                if (authData) {
                    this.authService.getUser(authData.uid).snapshotChanges().pipe(take(1)).subscribe((snapshot: any) => {
                        this.isUser = (snapshot != null && snapshot.key != undefined);
                        this.user = { uid: snapshot.key, ...snapshot.payload.val() };
                        if (this.isUser) {
                            this.tripService.getTrips().valueChanges().subscribe((trips: any) => {
                                trips.forEach(trip => {
                                    if (trip.status === 'waiting' || trip.status === 'accepted' || trip.status === 'going') {
                                        this.tripService.setId(trip.key)
                                        this.router.navigateByUrl('tracking', { replaceUrl: true });
                                    }
                                    else if (trip.status === 'finished') {
                                        this.router.navigateByUrl('home', { replaceUrl: true });
                                    }
                                })
                            })
                            this.router.navigateByUrl('home', { replaceUrl: true });
                        }
                    });
                } else {
                    this.router.navigateByUrl('/login', { replaceUrl: true });
                }
            });
        });
    }
}
