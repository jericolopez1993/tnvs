import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { DEFAULT_AVATAR, EMAIL_VERIFICATION_ENABLED } from 'src/environments/environment.prod';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: any;

  constructor(private afAuth: AngularFireAuth, private db: AngularFireDatabase) { }

  // get current user data from firebase
  getUserData() {
    return this.afAuth.auth.currentUser;
  }

  // get passenger by id
  getUser(id) {
    return this.db.object('passengers/' + id);
  }

  getDriverUser(id) {
    return this.db.object('drivers/' + id);
  }

  // login by email and password
  login(email, password) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  login_credentials(credential) {
      return this.afAuth.auth.signInWithCredential(credential);
  }


  resetPassword(email) {
    return this.afAuth.auth.sendPasswordResetEmail(email);
  }

  sendVerificationEmail() {
    return this.afAuth.auth.currentUser.sendEmailVerification();
  }

  logout() {
    return this.afAuth.auth.signOut();
  }

  // register new account
  register(email, password, first_name, middle_name, last_name, gender, address, birth_date, phoneNumber) {
    return Observable.create(observer => {
      this.afAuth.auth.createUserWithEmailAndPassword(email, password).then((authData: any) => {

        let userInfo: any = {
          uid: authData.user.uid,
            first_name: first_name,
            middle_name: middle_name,
            last_name: last_name,
            gender: gender,
            address: address,
            birth_date: birth_date,
          phoneNumber: phoneNumber,
          isPhoneVerified: false,
          email: email
        };

        if (EMAIL_VERIFICATION_ENABLED === true)
          this.getUserData().sendEmailVerification();
        // update passenger object
        this.updateUserProfile(userInfo);
        observer.next();
      }).catch((error: any) => {
        if (error) {
          observer.error(error);
        }
      });
    });
  }

  // update user display name and photo
  updateUserProfile(user) {
    let name = user.first_name + " " + user.last_name;
    let photoUrl = user.photoURL ? user.photoURL : DEFAULT_AVATAR;

    this.getUserData().updateProfile({
      displayName: name,
      photoURL: photoUrl
    });

    // create or update passenger
    this.db.object('passengers/' + user.uid).update({
        first_name: user.first_name,
        middle_name: user.middle_name,
        last_name: user.last_name,
        gender: user.gender,
        address: user.address,
        birth_date: user.birth_date,
      photoURL: photoUrl,
      email: user.email,
      phoneNumber: user.phoneNumber ? user.phoneNumber : '',
      isPhoneVerified: user.isPhoneVerified
    })
  }

  // create new user if not exist
  createUserIfNotExist(user) {
    this.getUser(user.uid).valueChanges().pipe(take(1)).subscribe((snapshot: any) => {
      if (snapshot === null) {
        this.updateUserProfile(user);
      }
    });
  }

  // update card setting
  updateCardSetting(number, exp, cvv, token) {
    const user = this.getUserData();
    this.db.object('passengers/' + user.uid + '/card').update({
      number: number,
      exp: exp,
      cvv: cvv,
      token: token
    })
  }

  // get card setting
  getCardSetting() {
    const user = this.getUserData();
    return this.db.object('passengers/' + user.uid + '/card');
  }

}
