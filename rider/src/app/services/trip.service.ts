import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AuthService } from './auth.service';
import { Place } from "./place";
import {take} from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class TripService {
    currentTrip: any;

    private id: any;
    private trips: any;
    private currency: string;
    private origin: any;
    private destination: any;
    private distance: number;
    private fee: number;
    private note: string;
    private paymentMethod: any = 'card';
    private vehicle: any;
    private promocode: any;
    private discount: any;
    private locationDetails: string;
    private recipientName: string;
    private mobileNumber: string;
    private itemCategory: string;
    // vehicle's icon
    private icon: any;
    private availableDrivers: Array<any> = [];

    constructor(private db: AngularFireDatabase, private authService: AuthService) {

    }

    getAll() {
        return this.trips;
    }

    setId(id) {
        return this.id = id;
    }

    getId() {
        return this.id;
    }

    setCurrency(currency) {
        return this.currency = currency;
    }

    getCurrency() {
        return this.currency;
    }

    setOrigin(vicinity, lat, lng) {
        let place = new Place(vicinity, lat, lng);
        return this.origin = place.getFormatted();
    }

    getOrigin() {
        return this.origin;
    }

    setDestination(vicinity, lat, lng) {
        let place = new Place(vicinity, lat, lng);
        return this.destination = place.getFormatted();
    }

    getDestination() {
        return this.destination
    }

    setDistance(distance) {
        return this.distance = distance;
    }

    getDistance() {
        return this.distance;
    }

    setFee(fee) {
        return this.fee = fee;
    }

    getFee() {
        return this.fee;
    }

    setNote(note) {
        return this.note = note;
    }

    getNote() {
        return this.note;
    }
    setLocationDetails(locationDetails) {
        return this.locationDetails = locationDetails;
    }

    getLocationDetails() {
        return this.locationDetails;
    }
    setRecipientName(recipientName) {
        return this.recipientName = recipientName;
    }
    getRecipientName() {
        return this.recipientName;
    }
    setMobileNumber(mobileNumber) {
        return this.mobileNumber = mobileNumber;
    }

    getMobileNumber() {
        return this.mobileNumber;
    }
    setItemCategory(itemCategory) {
        return this.itemCategory = itemCategory;
    }
    getItemCategory() {
        return this.itemCategory;
    }
    setPromo(promocode) {
        return this.promocode = promocode;
    }
    getPromo() {
        return this.promocode;
    }

    setDiscount(discount) {
        return this.discount = discount;
    }
    getDiscount() {
        return this.discount;
    }

    setPaymentMethod(method) {
        return this.paymentMethod = method;
    }

    getPaymentMethod() {
        return this.paymentMethod;
    }

    setVehicle(vehicle) {
        return this.vehicle = vehicle;
    }

    getVehicle() {
        return this.vehicle;
    }

    setIcon(icon) {
        return this.icon = icon;
    }

    getIcon() {
        return this.icon;
    }

    setAvailableDrivers(vehicles) {
        this.availableDrivers = vehicles;
    }

    getAvailableDrivers() {
        return this.availableDrivers;
    }

    getTrip(id) {
        return this.db.object('trips/' + id);
    }

    getTrips() {
        let user = this.authService.getUserData();
        return this.db.list('trips', res => res.orderByChild('passengerId').equalTo(user.uid));
    }

    getDriverTrips() {
        let user = this.authService.getUserData();
        return this.db.list('trips', ref => ref.orderByChild('driverId').equalTo(user.uid));
    }

    cancelTrip(id) {
        return this.db.object('trips/' + id).update({ status: 'canceled' })
    }

    rateTrip(tripId, stars) {
        return this.db.object('trips/' + tripId).update({
            rating: parseInt(stars)
        });
    }
    setCurrentTrip(tripId) {
        return this.db.object('trips/' + tripId).snapshotChanges().pipe(take(1)).subscribe((snapshot: any) => {
            this.db.object('trips/' + tripId).update({ key: tripId });
            if (snapshot != null) {
                this.currentTrip = { key: snapshot.key, ...snapshot.payload.val() };
            }
        });
    }
}
