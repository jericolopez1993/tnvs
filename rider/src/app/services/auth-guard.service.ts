import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot } from "@angular/router";

@Injectable({
    providedIn: "root"
})
export class AuthGuardService implements CanActivate {
    constructor(private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot): boolean {
        let navigationExtras = {
            queryParams: {
                triggerLogout: true
            }
        };
        if (route.url[0].path == "home" ) {
            this.router.navigate(["home"], navigationExtras);
        }else if (route.url[0].path == "tracking") {
            this.router.navigate(["tracking"], navigationExtras);
        }
        return true;
    }
}