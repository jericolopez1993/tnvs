import { Component, OnInit, ChangeDetectorRef, NgZone  } from '@angular/core';
import { PlaceService } from '../services/place.service';
import { TripService } from '../services/trip.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router, ActivatedRoute } from '@angular/router';

declare var google: any;

@Component({
    selector: 'app-map',
    templateUrl: './map.page.html',
    styleUrls: ['./map.page.scss'],
})


export class MapPage implements OnInit {
    ngOnInit() {
    }

    map: any;
    // pin address
    address: any;
    marker: any;
    lat: any;
    lng: any;
    googlePlace: any;
    googleAutocomplete: any;
    autocomplete: { input: string; };
    autocompleteItems: any[];
    location: any;
    placeid: any;

    currentLocation: string;

    data: any;

    constructor(
        public zone: NgZone,
        private router: Router,
        private geolocation: Geolocation,
        private chRef: ChangeDetectorRef,
        private route: ActivatedRoute,
        private placeService: PlaceService,
        private tripService: TripService
        )
    {
        this.googleAutocomplete = new google.maps.places.AutocompleteService();

        this.currentLocation = tripService.getOrigin().vicinity;
        this.autocomplete = { input: '' };
        this.autocompleteItems = [];

    }

    // Load map only after view is initialized
    ionViewDidEnter() {
        this.route.queryParams.subscribe(data => {
            if (data && data.address) {
                this.getCurrentAddressMap(data.address);
            }else{
                this.loadMap();
            }
        });
    }

    updateSearchResults(){
        this.googleAutocomplete.getPlacePredictions({
                input: this.autocomplete.input,
                componentRestrictions: {country: "ph"} },
            (predictions, status) => {
                this.autocompleteItems = [];
                this.zone.run(() => {
                    predictions.forEach((prediction) => {
                        this.autocompleteItems.push(prediction);
                    });
                });
            });
    }

    selectSearchResult(item) {
        this.location = item;
        this.placeid = this.location.place_id;
        this.address = this.location.description;
        this.findPlaceAndSelect(this.placeid)
    }


    getMap(latLng) {
        this.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            disableDefaultUI: true
        });

        this.marker = new google.maps.Marker({
            map: this.map,
            position: latLng
        });
        this.marker.setMap(this.map);

        // get center's address
        this.findPlace(latLng);

        this.map.addListener('drag', (event) => {
            this.marker.setPosition(this.map.getCenter());
        });

        this.map.addListener('dragend', (event) => {
            console.log('dragend');
            let latLng = this.map.getCenter();
            new google.maps.Geocoder().geocode({ 'latLng': latLng }, (results, status) => {
                if (status == google.maps.GeocoderStatus.OK) {
                    this.address = results[0];
                    this.autocomplete = {input: this.address.formatted_address};
                    this.chRef.detectChanges();
                    this.autocompleteItems = [];
                }
            });
        });
    }

    getCurrentAddressMap(address) {
        this.autocomplete = {input: address};
        new google.maps.Geocoder().geocode( { 'address': address}, (results) => {
            let latLng = results[0].geometry.location
            this.map = new google.maps.Map(document.getElementById('map'), {
                zoom: 16,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                disableDefaultUI: true
            });

            this.marker = new google.maps.Marker({
                map: this.map,
                position: latLng
            });
            this.marker.setMap(this.map);

            // get center's address
            let geocoder = new google.maps.Geocoder();

            this.marker.setMap(null);
            this.marker = new google.maps.Marker({ map: this.map, position: latLng });
            this.marker.setMap(this.map);

            geocoder.geocode({ 'latLng': latLng }, (results, status) => {
                if (status == google.maps.GeocoderStatus.OK) {
                    this.address = results[0];
                    this.chRef.detectChanges();
                    this.autocompleteItems = [];
                }
            });

            this.map.addListener('drag', (event) => {
                this.marker.setPosition(this.map.getCenter());
            });

            this.map.addListener('dragend', (event) => {
                console.log('dragend');
                let latLng = this.map.getCenter();
                new google.maps.Geocoder().geocode({ 'latLng': latLng }, (results, status) => {
                    if (status == google.maps.GeocoderStatus.OK) {
                        this.address = results[0];
                        this.autocomplete = {input: this.address.formatted_address};
                        this.chRef.detectChanges();
                        this.autocompleteItems = [];
                    }
                });
            });
        });
    }
    loadMap() {
        // set current location as map center
        this.geolocation.getCurrentPosition().then((resp) => {
            this.getMap(new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude));
        }).catch((error) => {
            console.log('Error getting location', error);
        });
    }

    findPlaceAndSelect (placeID) {
        let geocoder = new google.maps.Geocoder();

        this.marker.setMap(null);
        this.marker = new google.maps.Marker({ map: this.map, placeId: placeID });
        this.marker.setMap(this.map);

        geocoder.geocode({ 'placeId': placeID }, (results, status) => {
            if (status == google.maps.GeocoderStatus.OK) {
                this.lat = results[0].geometry.location.lat();
                this.lng = results[0].geometry.location.lng();
                this.map.panTo(new google.maps.LatLng(this.lat, this.lng));
                this.address = results[0];
                this.chRef.detectChanges();
            }
        });
    }

    // find address by LatLng
    findPlace(latLng) {
        let geocoder = new google.maps.Geocoder();

        this.marker.setMap(null);
        this.marker = new google.maps.Marker({ map: this.map, position: latLng});
        this.marker.setMap(this.map);

        geocoder.geocode({ 'latLng': latLng }, (results, status) => {
            if (status == google.maps.GeocoderStatus.OK) {
                this.address = results[0];
                this.autocomplete = {input: this.address.formatted_address};
                this.chRef.detectChanges();
                this.autocompleteItems = [];
            }
        });
    }

    findPlaceByID(placeID) {
        let geocoder = new google.maps.Geocoder();

        geocoder.geocode({ 'placeId': placeID }, (results, status) => {
            if (status == google.maps.GeocoderStatus.OK) {
                this.lat = results[0].geometry.location.lat();
                this.lng = results[0].geometry.location.lng();
                this.map.panTo(new google.maps.LatLng(this.lat, this.lng));
                this.findPlace(new google.maps.LatLng(this.lat, this.lng));
            }
        });
    }

    // choose address and go back to home page
    selectPlace() {

        let address = this.placeService.formatAddress(this.address);

        this.route.queryParams.subscribe(data => {
            let type = data.type
            if (type == 'origin') {
                this.tripService.setOrigin(address.vicinity, address.location.lat, address.location.lng);
            } else if (type == 'destination') {
                this.tripService.setDestination(address.vicinity, address.location.lat, address.location.lng);
            }
            this.router.navigateByUrl('home');

        });
    }

    selectCurrentLocation (){
        this.loadMap();
    }

    setLocation() {
        this.selectPlace()
    }

}
