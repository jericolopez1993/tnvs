import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from "./services/auth-guard.service";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
    {
        path: 'home', loadChildren: './home/home.module#HomePageModule'
    },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
    { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
    { path: 'map', loadChildren: './map/map.module#MapPageModule' },
    { path: 'tracking', loadChildren: './tracking/tracking.module#TrackingPageModule' },
    { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
    { path: 'history', loadChildren: './history/history.module#HistoryPageModule' },
    { path: 'payments', loadChildren: './payments/payments.module#PaymentsPageModule' },
    { path: 'rating', loadChildren: './rating/rating.module#RatingPageModule' },
    { path: 'chat', loadChildren: './chat/chat.module#ChatPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
