# Change these
server 'deploy@167.71.214.114', roles: [:web, :app, :db, :worker], primary: true

set :repo_url,        'git@bitbucket.org:jericolopez1993/tnvs.git'
set :repo_tree,       'registration'
set :application,     'registration'
set :user,            'deploy'
set :password,        'D9P1dasdwI^sZOZ#cM'
set :puma_threads,    [4, 16]
set :puma_workers,    0
set :branch, ENV.fetch('REVISION', 'master')
set :ssh_options, paranoid: false


# Don't change these unless you know what you're doing
set :pty,             true
set :use_sudo,        false
set :stage,           :production
set :deploy_via,      :remote_cache
set :deploy_to,       "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :ssh_options,     { forward_agent: true, user: fetch(:user), password: fetch(:password), keepalive: true }
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true  # Change to false when not using ActiveRecord
set :keep_releases, 1
set :linked_dirs, %w{public/system storage}
