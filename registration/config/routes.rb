Rails.application.routes.draw do
  resources :registration do
    collection do
      get 'landing'
    end
  end
  root to:  'registration#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
