class RegistrationController < ApplicationController
  def index
  end

  def create
    data = {
        nbiUrl: params[:nbi_url],
        licenseFrontUrl: params[:license_front_url],
        licenseBackUrl: params[:license_back_url],
        previousIdUrl: params[:previous_id_url],
        orcrUrl: params[:orcr_url],
        balance: 0,
        canRide: false,
        email: params[:email],
        isPhoneVerified: false,
        name: params[:name],
        first_name:  params[:first_name],
        middle_name:  params[:middle_name],
        last_name:  params[:last_name],
        present_address:  params[:address],
        gender:  params[:gender],
        birth_date:  params[:birth_date],
        ecp_first_name:  params[:ecp_first_name],
        ecp_middle_name:  params[:ecp_middle_name],
        ecp_last_name:  params[:ecp_last_name],
        ecp_phone_number:  params[:ecp_phone_number],
        ecp_relationship:  params[:ecp_relationship],
        brand: params[:brand],
        model: params[:model],
        plate: params[:plate],
        type: params[:type],
        password: params[:password],
        phoneNumber: params[:phone_number],
        photoURL: "http://placehold.it/150x150",
        rating: 5,
        uid: params[:uid],
        is_promo_quota: false,
        promo_quota_amount: 0,
        driver_type: params[:driver_type],
        userStatus: 'Pending'
    }
    FirebaseApi.new.update("drivers/#{params[:uid]}", data)
    send_mail
    redirect_to landing_registration_index_path
  end

  def landing
  end

  private

    def send_mail
      response = FirebaseApi.new.get("drivers/#{params[:uid]}")
      if response.success?
        @driver = response.body
        name = (@driver["first_name"] ? @driver["first_name"] : "") + " " + (@driver["last_name"] ? @driver["last_name"] : "")
        subject = "Account Pending for Verification"
        DriversMailer.send_mail(@driver["email"], name, params["uid"], subject ).deliver
      else
        @driver = {}
      end
    end
end
