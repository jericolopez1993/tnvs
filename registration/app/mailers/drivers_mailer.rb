class DriversMailer < ApplicationMailer
 def send_mail(driver_email, driver_name, uid, subject)
  from = "no-reply@eship.ph"
  subject = subject + " - " + uid
  @name = driver_name
  @uid = uid
  mail(
      :from => from,
      :to => driver_email,
      :subject => subject
  )
 end
end
