Rails.application.routes.draw do
  resources :payments
  resources :bookings
  resources :settings
  resources :vehicles
  resources :drivers do
   collection do
      get     'approved'
      get     'declined'
   end
  end
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_scope :user do
    get '/sign-in' => "devise/sessions#new", :as => :login
    authenticated :user do
      root 'static_pages#index', as: :authenticated_root
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

  resources :static_pages do
    collection do
      get   'maps'
      get     'approved'
      get     'declined'
    end
  end

  namespace :api do
    #For Mobile
    namespace :v1 do
      resources :localities do
        collection do
        end
      end
      resources :trips do
        collection do
        end
      end
      resources :deals do
        collection do
        end
      end
      resources :master_settings do
        collection do
          get  'prices'
        end
      end
      resources :users do
        collection do
          post  'reset_password'
          post  'resent_verification'
        end
      end
      devise_scope :user do
        post "/sign_in", :to => 'sessions#create'
        post "/sign_up", :to => 'registrations#create'
        delete "/sign_out", :to => 'sessions#destroy'
      end
      put 'users', to: 'users#update'
      patch 'users', to: 'users#update'
      put 'localities', to: 'localities#update'
      patch 'localities', to: 'localities#update'

    end
  end

  match '/customers',   to: 'users#index',   via: 'get'
  match '/customers/new',   to: 'users#new',   via: 'get'
  match '/customers/:id/edit',   to: 'users#edit',   via: 'get',   as: 'edit_customers'
  match '/customers/create',   to: 'users#create',   via: 'post'
  match '/customers/:id',   to: 'users#show',   via: 'get',   as: 'customer'
  match '/customers/:id',   to: 'users#update',   via: 'patch'
  match '/customers/:id',   to: 'users#update',   via: 'put'
  match '/customers/:id',   to: 'users#destroy',   via: 'delete'
  # match '/drivers',   to: 'users#drivers',   via: 'get'
  match '/dashboard',   to: 'static_pages#index',   via: 'get'
  match '/maps',   to: 'static_pages#maps',   via: 'get'

end
