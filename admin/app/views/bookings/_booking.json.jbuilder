json.extract! booking, :id, :source, :destination, :distance, :price, :user_id, :source_name, :destination_name, :source_place_id, :destination_place_id, :created_at, :updated_at
json.url booking_url(booking, format: :json)
