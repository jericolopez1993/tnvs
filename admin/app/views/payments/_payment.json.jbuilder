json.extract! payment, :id, :holder_name, :card_number, :card_cvv, :card_expiry, :user_id, :created_at, :updated_at
json.url payment_url(payment, format: :json)
