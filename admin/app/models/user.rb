class User < ApplicationRecord
  audited
  rolify
  has_one_attached :avatar
  extend Devise::Models
  attr_accessor :skip_password_validation
  before_save -> { skip_confirmation! }
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  acts_as_token_authenticatable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable, :trackable

  def full_name
    "#{self.first_name && self.first_name} #{self.last_name && self.last_name}"
  end

  def self.valid_login?(email, password)
    user = where(email: email).first
    if user && user.valid_password?(password)
      user.reset_authentication_token!
    end
    [user&.valid_password?(password), user]
  end

  def reset_authentication_token!
   update_column(:authentication_token, Devise.friendly_token)
  end

  def user_type
    if self.has_role? :driver
      "driver"
    else
      "user"
    end
  end


  protected
   def password_required?
     return false if skip_password_validation
     super
   end

end
``