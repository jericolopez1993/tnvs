class Trip < ApplicationRecord
  validates_presence_of :passenger_id
  validates_presence_of :driver_id
  validates_presence_of :origin
  validates_presence_of :destination
  validates_presence_of :distance
end
