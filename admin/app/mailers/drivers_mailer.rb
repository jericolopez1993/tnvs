class DriversMailer < ApplicationMailer
 def send_mail(driver_email, driver_name, driver_password, uid, subject, driver_status)
  from = "no-reply@eship.ph"
  puts "#{driver_email}"
  subject = subject + " - " + uid
  @name = driver_name
  @password = driver_password
  @status = driver_status
  mail(
      :from => from,
      :to => driver_email,
      :subject => subject
  )
 end
end
