// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require vendor/app.min
//= require vendor/default.min
//= require vendor/dashboard-v3.js
//= require vendor/table-manage-buttons.demo
//= require vendor/render.highlight
//= require vendor/form-slider-switcher.demo
//= require vendor/map-google.demo
//= require vendor/countries
//= require vendor/depends_on
//= require vendor/email-inbox.demo
//= require vendor/jquery-jvectormap.min
//= require vendor/jquery-jvectormap-world-mill
//= require inputmask
//= require jquery.inputmask
//= require inputmask.extensions
//= require inputmask.date.extensions
//= require inputmask.phone.extensions
//= require inputmask.numeric.extensions
//= require inputmask.regex.extensions
//= require tinymce
//= require select2
//= require drivers
//= require users
//= require_tree .


function isValidated() {
  var fields = $('input[required="required"], select[required="required"]');
  var isValidate = true;
  for(var i=0;i<fields.length;i++){
    if(!$(fields[i]).val() || 0 === $(fields[i]).val().length){
      isValidate = false;
      break;
    }
  }
  return isValidate;
}