class Api::V1::SessionsController < Devise::SessionsController
  acts_as_token_authentication_handler_for User, fallback_to_devise: false
  skip_before_action :verify_signed_out_user
  # prepend_before_action :require_no_authentication, :only => [:create]
  # before_action :ensure_params_exist, only: :create
  respond_to :json

  def create
    success, user = User.valid_login?(params[:email], params[:password])
    if success
      if user.confirmed?
          render json: {
              :user => user.as_json(
                  methods: [:user_type]
              )
          }, status: :created
      else
        render json: {"error":"Please verify your account."}, :status => :unauthorized
      end

    else
      render json: {"error":"Invalid email and password combination"}, :status => :unauthorized
    end
  end

  def destroy
    begin
      current_user.reset_authentication_token!
      head :ok
    rescue
      render json: {"error":"user token doesn't exist."}, :status => :unauthorized
    end
  end

  private
  def current_user
    authenticate_with_http_token do |token, options|
      User.find_by(authentication_token: token)
    end
  end

  protected
  def ensure_params_exist
    return unless params[:email].blank?
    render json: {:success => false, :message => "missing user_login parameter"
    }, :status => 422
  end

  def invalid_login_attempt
    warden.custom_failure!
    render json: {:success => false, :message => "Error with your login or password"
    }, :status => 401
  end
end
