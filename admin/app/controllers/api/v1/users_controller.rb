class Api::V1::UsersController < Api::V1::ApplicationController
  before_action :authenticate, only: [:update, :index]
  before_action :set_user, only: [:update]
  respond_to :json

  def reset_password
    @user = User.find_by_email(params[:email])
    if @user.present?
      @user.send_reset_password_instructions
      render :text => "updated"
    else
      render json: {"error":"Email doesn't exist."}, status: :unprocessable_entity
    end
  end

  def resent_verification
    @user = User.find_by_email(params[:email])
    if @user.present?
      @user.send_confirmation_instructions
      render json: {"success":"Account verification send to email."}, status: :ok
    else
      render json: {"error":"Email doesn't exist."}, status: :unprocessable_entity
    end
  end

  def index
    render json: current_user, methods: [:user_type], status: :ok
  end

  def update
    if !user_params.fetch(:password, '').empty?  # so it won't throw the ParameterMissing error
      if @user.update(user_params)
        render json: current_user, status: :ok
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    else
      if @user.update_without_password(user_params)
        render json: current_user, status: :ok
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end
  end

  def show
    if params[:id]
      @user = User.find(params[:id])
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = current_user
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:first_name, :last_name, :city, :phone_number, :email, :password, :password_confirmation)
  end
end
