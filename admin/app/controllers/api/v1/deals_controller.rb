class Api::V1::DealsController < Api::V1::ApplicationController
  before_action :authenticate

  def index

  end

  def create
    @deal = Deal.new(deal_params)
    if @deal.save
      render json: @deal, status: :created
      return
    else
      render json: @deal.errors, :status=>422
    end
  end

  def show
    @deal = Deal.where("status = ? AND driver_id = ?", "pending", params[:id]).order("updated_at DESC").first
    render json: @deal, status: :created
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = current_user
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def deal_params
    params.require(:deal).permit(:passenger_id, :driver_id, :currency, :origin, :destination, :distance, :fee, :note, :payment_method, :status, :promo_code, :discount, :created_at_number)
  end
end
