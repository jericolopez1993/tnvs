class Api::V1::MasterSettingsController < Api::V1::ApplicationController
  before_action :authenticate

  def prices
    price = Price.first
    render :json => price, methods: [:vehicles], :status => :ok
  end
end
