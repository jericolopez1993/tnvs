class Api::V1::ApplicationController < ActionController::API
  include ActionController::HttpAuthentication::Token::ControllerMethods
  include ActionController::ImplicitRender

  private
  def current_user
    authenticate_with_http_token do |token, options|
      User.find_by(authentication_token: token)
    end
  end

  def authenticate
    authenticate_with_http_token do |token, options|
      if token
        user = User.find_by(authentication_token: token)
        if !user
          render json: {"error":"user token doesn't exist."}, :status => :unauthorized
          return
        end
      else
        render json: {"error":"Please sign in or sign up."}, :status => :unauthorized
        return
      end
    end
  end
end
