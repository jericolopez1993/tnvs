class Api::V1::RegistrationsController < Devise::RegistrationsController
  respond_to :json

  def create
    if params[:user][:email] && params[:user][:password]
      user = User.new(user_params)
        if user.save
          if params[:user][:role] == "driver" || params[:user][:role] == "Driver"
            user.add_role :driver
          else
            user.add_role :user
          end
          user.add_role :admin
          # url = request.base_url + "/users/confirmation?confirmation_token=" + user.confirmation_token
          # render json: {"url": url, "message":"Confirmation has been sent to your email."}, status: :created
         render json: {"message":"Account Successfully Created. Thank you!"}, status: :created
          return
        else
          warden.custom_failure!
          render json: user.errors, :status=>422
        end
    else
      render json: {"error":"Please enter a required fields."}, status: :unprocessable_entity
    end
  end

  private

  def user_params
    params.require(:user).permit(:email,:password,:password_confirmation,:first_name, :last_name, :city, :phone_number, :gender)
  end
end
