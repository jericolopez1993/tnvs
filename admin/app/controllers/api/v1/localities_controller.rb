class Api::V1::LocalitiesController < Api::V1::ApplicationController
  before_action :authenticate
  before_action :set_user, only: [:update]
  before_action :set_driver, only: [:show]

  def index
    @drivers = []
    if params[:locality] && params[:vehicle_type]
      @drivers = User.with_role(:driver).where("locality = ? AND vehicle_type = ? AND last_active >= NOW() - INTERVAL '10 minutes'", params[:locality], params[:vehicle_type])
    end
    render json: @drivers, status: :ok
  end

  def show
    render json: @driver, status: :ok
  end

  def update
    if @user.update_without_password(user_params)
      render json: @user, status: :ok
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = current_user
  end

  def set_driver
    @driver = User.with_role(:driver).find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params[:user][:last_active] = params[:user][:last_active].to_datetime
    params.require(:user).permit(:locality, :lat, :lng, :oldLat, :oldLng, :last_active, :last_active_number)

  end
end
