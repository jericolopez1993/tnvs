class Api::V1::TripsController < Api::V1::ApplicationController
  before_action :authenticate

  def index
    if params[:driver_id]
      @trips = Trip.where(:driver_id => params[:driver_id])
    elsif params[:passenger_id]
      @trips = Trip.where(:passenger_id => params[:passenger_id])
    else
      @trips = Trip.all
    end
    render json: @trips, status: :ok
  end

  def create
    @trip = Trip.new(trip_params)
    if @trip.save
      if params[:deal_id] && params[:status]
        deal = Deal.find(params[:deal_id])
        deal.update_attributes(trip_id: @trip.id, status: params[:status])
      end
      render json: @trip, status: :created
      return
    else
      render json: @trip.errors, :status=>422
    end
  end

  def update
    @trip = Trip.find(params[:id])
    if @trip.update(trip_params)
      render json: @trip, status: :ok
    else
      render json: @trip.errors, :status=>422
    end
  end

  def show
    @trip = Trip.where("status = ? AND driver_id = ?", "pending", params[:id]).order("updated_at DESC").first
    render json: @trip, status: :created
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = current_user
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def trip_params
    params.require(:trip).permit(:passenger_id, :driver_id, :currency, :origin, :destination, :distance, :fee, :note, :payment_method, :status, :promo_code, :discount, :created_at_number, :pickup_at, :drop_off_at, :cancelled_at, :pickup_at_number, :drop_off_at_number, :cancelled_at_number, :otp)
  end
end
