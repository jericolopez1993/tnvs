class StaticPagesController < ApplicationController

  def index
    response = FirebaseApi.new.get("drivers")
    if response.success?
      @drivers = response.body ? response.body.values : []
      @drivers = @drivers.select { |driver| driver["canRide"] == false && driver["userStatus"] == "Pending"}
    else
      @drivers = []
    end
  end

  def maps
  end

  def approved
    FirebaseApi.new.update("drivers/#{params[:uid]}", {canRide: true, userStatus: 'Approved'})
    send_mail("Approved")
    redirect_to static_pages_path
  end

  def declined
    FirebaseApi.new.update("drivers/#{params[:uid]}", {canRide: false, userStatus: 'Declined'})
    send_mail("Declined")
    redirect_to static_pages_path
  end

  # DELETE /drivers/1
  # DELETE /drivers/1.json
  def destroy
    FirebaseApi.new.delete("drivers/#{params[:id]}")
    redirect_to drivers_path
  end

  private

  def send_mail(stats)
    response = FirebaseApi.new.get("drivers/#{params[:id]}")
    if response.success?
      @driver = response.body[params[:uid]]
      name = (@driver["first_name"] ? @driver["first_name"] : "") + " " + (@driver["last_name"] ? @driver["last_name"] : "")
      subject = "Account #{stats}"
      DriversMailer.send_mail(@driver["email"], name, @driver["password"], params["uid"], subject, stats).deliver
    else
      @driver = {}
    end
  end

end
