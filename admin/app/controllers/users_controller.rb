class UsersController < ApplicationController

  # GET /users
  # GET /users.json
  def index
    if !user_signed_in?
      redirect_to(unauthenticated_root_path)
    else
      response = FirebaseApi.new.get("passengers")
      if response.success? && response.body
        @customers = response.body.values
      else
        @customers = []
      end
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    response = FirebaseApi.new.get("passengers/#{params[:id]}")
    if response.success?
      @customer = response.body
    else
      @customer = {}
    end
  end

  # GET /drivers/new
  def new
  end

  # GET /drivers/1/edit
  def edit
    response = FirebaseApi.new.get("passengers/#{params[:id]}")
    if response.success?
      @customer = response.body
    else
      @customer = {}
    end
  end

  # POST /drivers
  # POST /drivers.json
  def create

    data = {
        first_name:  params[:first_name],
        middle_name:  params[:middle_name],
        last_name:  params[:last_name],
        present_address:  params[:address],
        gender:  params[:gender],
        birth_date:  params[:birth_date],
        email: params[:email],
        isPhoneVerified: false,
        name: params[:name],
        phoneNumber: params[:phone_number],
        uid: params[:uid],
        photoURL: "http://placehold.it/150x150"
    }
    FirebaseApi.new.update("passengers/#{params[:uid]}", data)
    redirect_to customers_path
  end

  # PATCH/PUT /drivers/1
  # PATCH/PUT /drivers/1.json
  def update
    response = FirebaseApi.new.get("passengers/#{params[:uid]}")
    if response.success?
      @customer = response.body
    else
      @customer = {}
    end
    data = {
        first_name:  params[:first_name],
        middle_name:  params[:middle_name],
        last_name:  params[:last_name],
        present_address:  params[:address],
        gender:  params[:gender],
        birth_date:  params[:birth_date],
        email: params[:email],
        isPhoneVerified: @customer['isPhoneVerified'],
        name: params[:name],
        password: @customer['password'],
        phoneNumber: params[:phone_number],
        uid: params[:uid],
        photoURL: @customer['photoURL']
    }
    FirebaseApi.new.set("passengers/#{params[:uid]}", data)
    redirect_to customers_path
  end

  # DELETE /drivers/1
  # DELETE /drivers/1.json
  def destroy
    FirebaseApi.new.delete("passengers/#{params[:id]}")
    redirect_to customers_path
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @customer = User.find(params[:id])
  end

  def set_roles
    if params[:is_user].present?
      @customer.add_role :user unless @customer.has_role? :user
    else
      @customer.remove_role :user if @customer.has_role? :user
    end

    if params[:is_driver].present?
      @customer.add_role :driver unless @customer.has_role? :driver
    else
      @customer.remove_role :driver if @customer.has_role? :driver
    end

    if params[:is_admin].present?
      @customer.add_role :admin unless @customer.has_role? :admin
    else
      @customer.remove_role :admin if @customer.has_role? :admin
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :city, :phone_number)
  end
end
