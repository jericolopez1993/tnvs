class SettingsController < ApplicationController
  def index
    response = FirebaseApi.new.get("master_settings/prices/default")
    if response.success?
      @price = response.body
      @vehicles = response.body["vehicles"].values
    else
      @price = {}
      @vehicles = []
    end
  end

  def show
    @price = Price.first
    @price ||= Price.new
    @vehicles = Vehicle.where(:price_id => @price.id)
    render 'index'
  end

  def edit
    @price = Price.first
    @vehicles = Vehicle.where(:price_id => @price.id)
  end

  def create
    if params[:currency]
      FirebaseApi.new.set("master_settings/prices/default/currency", params[:currency] )
    end
    redirect_to settings_path
  end

  def update
    @price = Price.first
    respond_to do |format|
      if @price.update(price_params)
        format.html { redirect_to settings_path, notice: 'Price was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @price.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  def price_params
    params.require(:price).permit(:currency)
  end
end
