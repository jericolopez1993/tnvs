class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery with: :null_session
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?
  layout :layout_by_resource
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized



  private

  def layout_by_resource
    if devise_controller?
      "devise"
    else
      "application"
    end
  end


  protected

  def after_sign_in_path_for(resource)
    if current_user.has_role?(:admin)
      request.env['omniauth.origin'] || stored_location_for(resource) || authenticated_root_path
    else
      reset_session
      flash['alert'] = "Invalid login. Please use a adminstrator account."
      unauthenticated_root_path
    end
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :city, :phone_number, :locality, :lat, :lng, :oldLat, :oldLng, :last_active])
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name, :city, :phone_number, :locality, :lat, :lng, :oldLat, :oldLng, :last_active])
  end



end
