class DriversController < ApplicationController

  # GET /drivers
  # GET /drivers.json
  def index
      response = FirebaseApi.new.get("drivers")
      if response.success?
        @drivers = response.body ? response.body.values : []
        # array_of_objects.select { |favor| favor.completed == false }
      else
        @drivers = []
      end
  end


  # GET /drivers/1
  # GET /drivers/1.json
  def show
    response = FirebaseApi.new.get("drivers/#{params[:id]}")
    if response.success?
      @driver = response.body
    else
      @driver = {}
    end
  end

  # GET /drivers/new
  def new
  end

  # GET /drivers/1/edit
  def edit
    response = FirebaseApi.new.get("drivers/#{params[:id]}")
    if response.success?
      @driver = response.body
      puts "#{@driver}"
    else
      @driver = {}
    end
  end

  # POST /drivers
  # POST /drivers.json
  def create
    data = {
        balance: params[:balance],
        canRide: params[:can_ride].present?,
        email: params[:email],
        isPhoneVerified: false,
        name: params[:name],
        first_name:  params[:first_name],
        middle_name:  params[:middle_name],
        last_name:  params[:last_name],
        present_address:  params[:address],
        gender:  params[:gender],
        birth_date:  params[:birth_date],
        ecp_first_name:  params[:ecp_first_name],
        ecp_middle_name:  params[:ecp_middle_name],
        ecp_last_name:  params[:ecp_last_name],
        ecp_phone_number:  params[:ecp_phone_number],
        ecp_relationship:  params[:ecp_relationship],
        brand: params[:brand],
        model: params[:model],
        plate: params[:plate],
        type: params[:type],
        password: params[:password],
        phoneNumber: params[:phone_number],
        photoURL: "http://placehold.it/150x150",
        rating: 5,
        uid: params[:uid],
        is_promo_quota: params[:is_promo_quota].present?,
        promo_quota_amount: params[:promo_quota_amount],
        driver_type: params[:driver_type],
        userStatus: (params[:can_ride].present? ? 'Approved' : 'Declined')
    }
    FirebaseApi.new.update("drivers/#{params[:uid]}", data)
    userStatus = params[:can_ride].present? ? 'Approved' : 'Declined'
    send_mail(userStatus)
    redirect_to drivers_path
  end

  # PATCH/PUT /drivers/1
  # PATCH/PUT /drivers/1.json
  def update
    response = FirebaseApi.new.get("drivers/#{params[:uid]}")
    if response.success?
      @driver = response.body
    else
      @driver = {}
    end
    data = {
        balance: params[:balance],
        canRide: params[:can_ride].present?,
        email: params[:email],
        isPhoneVerified: @driver['isPhoneVerified'],
        name: params[:name],
        first_name:  params[:first_name],
        middle_name:  params[:middle_name],
        last_name:  params[:last_name],
        present_address:  params[:address],
        gender:  params[:gender],
        birth_date:  params[:birth_date],
        ecp_first_name:  params[:ecp_first_name],
        ecp_middle_name:  params[:ecp_middle_name],
        ecp_last_name:  params[:ecp_last_name],
        ecp_phone_number:  params[:ecp_phone_number],
        ecp_relationship:  params[:ecp_relationship],
        brand: params[:brand],
        model: params[:model],
        plate: params[:plate],
        type: params[:type],
        password: @driver['password'],
        phoneNumber: params[:phone_number],
        photoURL: @driver['photoURL'],
        rating: @driver['rating'],
        uid: params[:uid],
        is_promo_quota: params[:is_promo_quota].present?,
        promo_quota_amount: params[:promo_quota_amount],
        driver_type: params[:driver_type],
        userStatus: (params[:can_ride].present? ? 'Approved' : 'Declined')
    }
    FirebaseApi.new.set("drivers/#{params[:uid]}", data)
    userStatus = params[:can_ride].present? ? 'Approved' : 'Declined'
    send_mail(userStatus)
    redirect_to drivers_path
  end

  def approved
    FirebaseApi.new.update("drivers/#{params[:uid]}", {canRide: true, userStatus: 'Approved'})
    send_mail("Approved")
    redirect_to driver_path(:id => params[:uid])
  end

  def declined
    FirebaseApi.new.update("drivers/#{params[:uid]}", {canRide: false, userStatus: 'Declined'})
    send_mail("Declined")
    redirect_to driver_path(:id => params[:uid])
  end

  # DELETE /drivers/1
  # DELETE /drivers/1.json
  def destroy
    FirebaseApi.new.delete("drivers/#{params[:id]}")
    redirect_to drivers_path
  end

  private

    def send_mail(stats)
      response = FirebaseApi.new.get("drivers/#{params[:id]}")
      if response.success?
        @driver = response.body[params[:uid]]
        name = (@driver["first_name"] ? @driver["first_name"] : "") + " " + (@driver["last_name"] ? @driver["last_name"] : "")
        subject = "Account #{stats}"
        DriversMailer.send_mail(@driver["email"], name, @driver["password"], params["uid"], subject, stats).deliver
      else
        @driver = {}
      end
    end

  # Never trust parameters from the scary internet, only allow the white list through.
  def driver_params
    params.require(:driver).permit(:first_name, :last_name, :email, :password, :city, :phone_number)
  end
end
