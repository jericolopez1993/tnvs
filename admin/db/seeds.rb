# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
names = Array[User, Price, Vehicle]

for name in names
  name.delete_all
end


Price.create!([{currency: "₱"}])

price = Price.first

Vehicle.create!([
                    {price_id: price.id, active: true, enable: true, fee: 40.0, icon: "assets/img/001-scooter.png", map_icon: "assets/img/001-scooter-top.png", name: "Motorcycle", price: 5.5, vehicle_type: "motorcycle"}
                ])
vehicle = Vehicle.first

User.create!([
                 {id: 1, email: "jerico.lopez1993@gmail.com", password: "password", password_confirmation: "password", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, created_at: "2019-09-11 06:09:58", updated_at: "2019-09-11 02:49:48", first_name: "Jerico", last_name: "Lopez", confirmation_token: nil, confirmed_at: "2019-09-11 19:04:36", confirmation_sent_at: nil},
                 {id: 2, email: "jerryCPA2014@yahoo.com", password: "password", password_confirmation: "password", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, created_at: "2019-09-11 06:09:58", updated_at: "2019-09-11 02:49:48", first_name: "Jeremiah", last_name: "Lopez", confirmation_token: nil, confirmed_at: "2019-09-11 19:04:36", confirmation_sent_at: nil},
                 {id: 3, email: "king.munoz@gmail.com", password: "password", password_confirmation: "password", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, created_at: "2019-09-11 06:09:58", updated_at: "2019-09-11 02:49:48", first_name: "Ricky", last_name: "Ignacio", confirmation_token: nil, confirmed_at: "2019-09-11 19:04:36", confirmation_sent_at: nil},
                 {id: 4, email: "admin@test.com", password: "password", password_confirmation: "password", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, created_at: "2019-09-11 06:09:58", updated_at: "2019-09-11 02:49:48", first_name: "Admin", last_name: "Test", confirmation_token: nil, confirmed_at: "2019-09-11 19:04:36", confirmation_sent_at: nil},
                 {id: 5, email: "driver@test.com", password: "password", password_confirmation: "password", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, created_at: "2019-09-11 06:09:58", updated_at: "2019-09-11 02:49:48", first_name: "Driver", last_name: "Test", confirmation_token: nil, confirmed_at: "2019-09-11 19:04:36", confirmation_sent_at: nil, vehicle_type: vehicle.id, brand: "suzuki", model: "barako", plate: "ABC 123456"},
                 {id: 6, email: "user@test.com", password: "password", password_confirmation: "password", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, created_at: "2019-09-11 06:09:58", updated_at: "2019-09-11 02:49:48", first_name: "User", last_name: "Test", confirmation_token: nil, confirmed_at: "2019-09-11 19:04:36", confirmation_sent_at: nil}
             ])
User.all.each do |user|
  if user.email == "driver@test.com"
    user.add_role :driver
  elsif user.email == "user@test.com"
    user.add_role :user
  else
    user.add_role :admin
  end
end


ActiveRecord::Base.connection.tables.each do |table_name|
  ActiveRecord::Base.connection.reset_pk_sequence!(table_name)
end

# ,
#     {price_id: price.id, active: true, enable: true, fee: 80.0, icon: "assets/img/003-van-black-side-view.png", map_icon: "assets/img/map-sedan.png", name: "Van", price: 5.5, vehicle_type: "van"},
#     {price_id: price.id, active: true, enable: true, fee: 120.0, icon: "assets/img/004-truck.png", map_icon: "assets/img/map-sedan.png", name: "Truck", price: 8.25, vehicle_type: "truck"}