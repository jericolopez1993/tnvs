class RenameTripsToDeals < ActiveRecord::Migration[5.2]
  def up
    rename_table :trips, :deals
  end

  def down
    rename_table :deals, :trips
  end
end
