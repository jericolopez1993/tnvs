class AddCreatedAtNumberToTrips < ActiveRecord::Migration[5.2]
  def change
    add_column :trips, :created_at_number, :bigint
  end
end
