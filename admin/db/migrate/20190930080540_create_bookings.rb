class CreateBookings < ActiveRecord::Migration[5.2]
  def change
    create_table :bookings do |t|
      t.string :source
      t.string :destination
      t.decimal :distance
      t.decimal :price
      t.integer :user_id
      t.string :source_name
      t.string :destination_name
      t.string :source_place_id
      t.string :destination_place_id

      t.timestamps
    end
  end
end
