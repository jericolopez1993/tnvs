class CreateDeals < ActiveRecord::Migration[5.2]
  def change
    create_table :trips do |t|
      t.integer "passenger_id"
      t.integer "driver_id"
      t.string "origin"
      t.string "destination"
      t.decimal "distance"
      t.decimal "fee"
      t.text "note"
      t.string "payment_method"
      t.string "status"
      t.string "promo_code"
      t.decimal "discount"
      t.bigint "created_at_number"
      t.datetime "pickup_at"
      t.datetime "drop_off_at"
      t.datetime "cancelled_at"
      t.timestamps
    end
  end
end
