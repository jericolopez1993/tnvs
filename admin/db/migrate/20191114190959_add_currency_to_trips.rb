class AddCurrencyToTrips < ActiveRecord::Migration[5.2]
  def change
    add_column :trips, :currency, :string
  end
end
