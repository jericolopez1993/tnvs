class AddDateNumbersToTrip < ActiveRecord::Migration[5.2]
  def change
    add_column :trips, :pickup_at_number, :bigint
    add_column :trips, :drop_off_at_number, :bigint
    add_column :trips, :cancelled_at_number, :bigint
    add_column :trips, :otp, :bigint
  end
end
