class AddTripIdToDeals < ActiveRecord::Migration[5.2]
  def change
    add_column :deals, :trip_id, :integer
  end
end
