class AddMoreFieldsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :locality, :string
    add_column :users, :lat, :decimal
    add_column :users, :lng, :decimal
    add_column :users, :oldLat, :decimal
    add_column :users, :oldLng, :decimal
    add_column :users, :last_active, :datetime
    add_column :users, :rating, :decimal
    add_column :users, :brand, :string
    add_column :users, :model, :string
    add_column :users, :plate, :string
    add_column :users, :vehicle_type, :integer
  end
end
