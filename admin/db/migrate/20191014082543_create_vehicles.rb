class CreateVehicles < ActiveRecord::Migration[5.2]
  def change
    create_table :vehicles do |t|
      t.boolean :enable
      t.boolean :active
      t.string :icon
      t.string :map_icon
      t.string :name
      t.string :vehicle_type
      t.integer :price_id

      t.timestamps
    end
  end
end
