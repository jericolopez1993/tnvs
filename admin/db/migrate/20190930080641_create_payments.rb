class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.string :holder_name
      t.string :card_number
      t.string :card_cvv
      t.string :card_expiry
      t.string :user_id

      t.timestamps
    end
  end
end
