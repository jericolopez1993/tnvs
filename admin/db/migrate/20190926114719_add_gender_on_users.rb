class AddGenderOnUsers < ActiveRecord::Migration[5.2]
  def up
    add_column :users, :gender, :string
  end

  def down
    remove_column :users, :gender
  end
end
