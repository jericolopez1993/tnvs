import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { EnvService } from './env.service';
import { User } from '../models/user';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = false;
  token:any;
  constructor(
    private http: HttpClient,
    private storage: NativeStorage,
    private env: EnvService,
  ) { }
  login(email: String, password: String) {
    return this.http.post(this.env.API_URL + 'sign_in',
      {email: email, password: password}
    ).pipe(
      tap(token => {
        console.log(token);
        this.storage.setItem('token', token)
        .then(
          () => {
            console.log('Token Stored');
          },
          error => console.error('Error storing item', error)
        );
        this.token = token;
        this.isLoggedIn = true;
        return token;
      }),
    );
  }
  send_instructions(email: String) {
    return this.http.post(this.env.API_URL + 'users/reset_password',
      {email: email}
    )
  }
  resend_confirmation(email: String) {
    return this.http.post(this.env.API_URL + 'users/resent_verification',
      {email: email}
    )
  }
  register(first_name: String, last_name: String, city: String, phone_number: String, email: String, password: String, password_confirmation: String, user_type: String) {
    return this.http.post(this.env.API_URL + 'sign_up',
      {user: {first_name: first_name, last_name: last_name, city: city, phone_number: phone_number, email: email, password: password, password_confirmation: password_confirmation, user_type: user_type}}
    )
  }
  logout() {
    const headers = new HttpHeaders({
      'Authorization': "Bearer "+this.token["user"]["authentication_token"]
    });
    return this.http.delete(this.env.API_URL + 'sign_out', { headers: headers })
    .pipe(
      tap(data => {
        this.storage.remove("token");
        this.isLoggedIn = false;
        delete this.token;
        return data;
      })
    )
  }
  updateProfile(first_name: String, last_name: String, city: String, phone_number: String) {
    const headers = new HttpHeaders({
      'Authorization': "Bearer "+this.token["user"]["authentication_token"]
    });
    return this.http.put<User>(this.env.API_URL + 'users',
      {user: {first_name: first_name, last_name: last_name, city: city, phone_number: phone_number}}, {headers: headers} ).pipe(
        tap(user => {
          return user;
        })
      )
  }
  user() {
    const headers = new HttpHeaders({
      'Authorization': "Bearer "+this.token["user"]["authentication_token"]
    });
    return this.http.get<User>(this.env.API_URL + 'users', { headers: headers })
    .pipe(
      tap(user => {
        return user;
      })
    )
  }
  getToken() {
    return this.storage.getItem('token').then(
      data => {
        this.token = data;
        if(this.token != null) {
          this.isLoggedIn=true;
        } else {
          this.isLoggedIn=false;
        }
      },
      error => {
        this.token = null;
        this.isLoggedIn=false;
      }
    );
  }
}