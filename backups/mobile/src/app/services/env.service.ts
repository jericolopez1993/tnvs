import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class EnvService {
  API_URL = 'http://tnvs.herokuapp.com/api/v1/';
  // API_URL = 'https://tnvs.herokuapp.com/api/v1/';

  constructor() { }
}