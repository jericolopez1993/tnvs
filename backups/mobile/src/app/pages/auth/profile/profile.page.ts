import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { LoginPage } from '../login/login.page';
import { AuthService } from 'src/app/services/auth.service';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { User } from 'src/app/models/user';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  user: User;
  constructor(private menu: MenuController,
    private modalController: ModalController,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService) {

    this.menu.enable(true);
  }
  ngOnInit() {
  }
  ionViewWillEnter() {
    this.authService.user().subscribe(
      user => {
        console.log(user);
        this.user = user;
        console.log(  this.user );
      }
    );
  }
  logout() {
    this.authService.logout().subscribe(
      data => {
        this.alertService.presentToast("Sign out Successful. Thank you.");
      },
      error => {
        console.log(error);
      },
      () => {
        this.navCtrl.navigateRoot('/login');
      }
    );
  }
}

