import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { NgForm } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { User } from 'src/app/models/user';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
  user: User;
  constructor(private menu: MenuController,
    private modalController: ModalController,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService) {

    this.menu.enable(true);
  }
  ngOnInit() {
  }
  ionViewWillEnter() {
    this.authService.user().subscribe(
      user => {
        this.user = user;
        console.log(this.user);
      }
    );
  }
  updateProfile(form: NgForm) {
    this.authService.updateProfile(form.value.first_name, form.value.last_name, form.value.city, form.value.phone_number).subscribe(
      data => {
        this.alertService.presentToast("Update Profile Successful.");
      },
      error => {
        console.log(error);
      },
      () => {
        this.navCtrl.navigateRoot('/profile');
      }
    );
  }
}

