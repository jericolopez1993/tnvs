import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { AlertService } from 'src/app/services/alert.service';


@Component({
  selector: 'app-resent-confirmation',
  templateUrl: './resent-confirmation.page.html',
  styleUrls: ['./resent-confirmation.page.scss'],
})
export class ResentConfirmationPage implements OnInit {

  constructor(
    private modalController: ModalController,
    private authService: AuthService,
    private navCtrl: NavController,
    private alertService: AlertService
  ) { }
  ngOnInit() {
  }
  resend_confirmation(form: NgForm) {
    this.authService.resend_confirmation(form.value.email).subscribe(
      data => {
        this.alertService.presentToast("Confirmation instruction send to your email address. Please check into your inbox or spam. Thanks you.");
      },
      error => {
        console.log(error);
      },
      () => {
        this.navCtrl.navigateRoot('/login');
      }
    );
  }
}