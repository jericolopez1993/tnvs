import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    loadChildren: './pages/auth/login/login.module#LoginPageModule'
  },
  {
    path: 'register',
    loadChildren: './pages/auth/register/register.module#RegisterPageModule'
  },
  {
    path: 'register/driver',
    loadChildren: './pages/auth/register/driver/driver.module#DriverPageModule'
  },
  {
    path: 'profile',
    loadChildren: './pages/auth/profile/profile.module#ProfilePageModule' ,
    canActivate: [AuthGuard]
  },
  {
    path: 'profile/edit',
    loadChildren: './pages/auth/profile/edit/edit.module#EditPageModule',
    canActivate: [AuthGuard]
  },
  {
    path: 'forgot-password',
    loadChildren: './pages/auth/forgot-password/forgot-password.module#ForgotPasswordPageModule'
  },
  {
    path: 'resent-confirmation',
    loadChildren: './pages/auth/resent-confirmation/resent-confirmation.module#ResentConfirmationPageModule' 
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
