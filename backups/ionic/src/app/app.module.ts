import { NgModule, ErrorHandler } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NativeStorage } from '@ionic-native/native-storage';

import { AuthService } from '../services/auth.service';
import { AlertService } from '../services/alert.service';
import { EnvService } from '../services/env.service';

import { LoginPage } from '../pages/login/login';
import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { RequestPage } from '../pages/request/request';
import { AcceptriderPage } from '../pages/acceptrider/acceptrider';
import { AcceptPage } from '../pages/accept/accept';
import { RoderPage } from '../pages/roder/roder';
import { CancelridePage } from '../pages/cancelride/cancelride';
import { NevigatePage } from '../pages/nevigate/nevigate';
import { LocationPage } from '../pages/location/location';
import { FarePage } from '../pages/fare/fare';
import { RatingsPage } from '../pages/ratings/ratings';
import { EarningsPage } from '../pages/earnings/earnings';
import { ProfilePage } from '../pages/profile/profile';
import { HelpPage } from '../pages/help/help';
import { DocumentsPage } from '../pages/documents/documents';
import { SettingsPage } from '../pages/settings/settings';
import { MytripsPage } from '../pages/mytrips/mytrips';
import { SharePage } from '../pages/share/share';
import { MyprofilePage } from '../pages/myprofile/myprofile';
import { SignupPage } from '../pages/signup/signup';
import { VerificationPage } from '../pages/verification/verification';
import { AddmoneyPage } from '../pages/addmoney/addmoney';
import { Trip_endPage } from '../pages/trip_end/trip_end';
import { WalletPage } from '../pages/wallet/wallet';
import { RiderinfoPage } from '../pages/riderinfo/riderinfo';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    HomePage,
    TabsPage,
    LoginPage,
    RequestPage,
    AcceptriderPage,
    RoderPage,
    CancelridePage,
    NevigatePage,
    LocationPage,
    FarePage,
    RatingsPage,
    EarningsPage,
    ProfilePage,
    HelpPage,
    DocumentsPage,
    SettingsPage,
    MytripsPage,
    SharePage,
    MyprofilePage,
    AcceptPage,
    SignupPage,
    VerificationPage,
    RiderinfoPage,
    Trip_endPage,
    AddmoneyPage,
    WalletPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    HomePage,
    TabsPage,
    LoginPage,
    RequestPage,
    AcceptriderPage,
    RoderPage,
    CancelridePage,
    NevigatePage,
    LocationPage,
    FarePage,
    RatingsPage,
    EarningsPage,
    ProfilePage,
    HelpPage,
    DocumentsPage,
    SettingsPage,
    MytripsPage,
    SharePage,
    MyprofilePage,
    AcceptPage,
    SignupPage,
    VerificationPage,
    RiderinfoPage,
    Trip_endPage,
    AddmoneyPage,
    WalletPage
  ],
  providers: [
      StatusBar,
      SplashScreen,
      { provide: ErrorHandler, useClass: IonicErrorHandler },
      NativeStorage,
      AuthService,
      AlertService,
      EnvService
  ]

})
export class AppModule { }
