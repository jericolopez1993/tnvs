import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AuthService } from '../../services/auth.service';
import { AlertService } from '../../services/alert.service';
import { NgForm } from '@angular/forms';
import { TabsPage } from '../tabs/tabs';
import { SignupPage } from '../signup/signup';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  constructor(
      public navCtrl: NavController,
      private authService: AuthService,
      private alertService: AlertService
  ) {
  }
    ngOnInit() {
    }
    login(form: NgForm) {
        console.log(form);
        this.authService.login(form.value.email, form.value.password).subscribe(
            data => {
                this.alertService.presentToast("Logged In");
            },
            error => {
                console.log(error);
            },
            () => {
                this.navCtrl.setRoot(TabsPage);
            }
        );
    }

    signupPage(){
        this.navCtrl.push(SignupPage);
    }

}  