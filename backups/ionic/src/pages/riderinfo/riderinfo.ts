import { Component } from '@angular/core';
import { NavController, ViewController, ModalController} from 'ionic-angular';

//import { CancelridePage } from '../cancelride/cancelride';
@Component({
  selector: 'page-riderinfo',
  templateUrl: 'riderinfo.html'
})
export class RiderinfoPage {

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public viewCtrl: ViewController) {

  }
  
  dismiss() {
    this.viewCtrl.dismiss();
  }
// cancelride() {
//		let modal = this.modalCtrl.create(CancelridePage);
//		modal.present();
//	}
}
