import { NavController, ModalController } from 'ionic-angular';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { AcceptPage } from '../accept/accept';

declare var google;

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})

export class HomePage {
	@ViewChild('map') mapElement: ElementRef;
	map: any;

	checked: boolean = false;

	constructor(public navCtrl: NavController, public modalCtrl: ModalController, ) {

	}

	ionViewDidLoad() {
		let latLng = new google.maps.LatLng(20.5937, 78.9629);
		this.loadMap(latLng);
	}

	loadMap(latLng) {
		let mapOptions = {
			center: latLng,
			zoom: 4,
			mapTypeControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
		// new google.maps.Marker({position: latLng, map: this.map});
	}

	accept() {
		let modal = this.modalCtrl.create(AcceptPage);
		modal.present();
	}

	onlineOfflineToggle() {
		console.log('checked ' + this.checked);
	}

	declinePage() {
		//this.navCtrl.push(AcceptPage);
	}

}
