import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import {WalletPage } from '../wallet/wallet';
@Component({
  selector: 'page-earnings',
  templateUrl: 'earnings.html'
})
export class EarningsPage {
 all_week: string = "week6";
  constructor(public navCtrl: NavController) {

  }
wallet(){
    this.navCtrl.push(WalletPage);
    }
}
