
import { ModalController, NavController, ViewController } from 'ionic-angular';
import { Component, ViewChild, ElementRef } from '@angular/core';

declare var google;
import { CancelridePage } from '../cancelride/cancelride';
import { LocationPage } from '../location/location';
import { RiderinfoPage } from '../riderinfo/riderinfo';

@Component({
	selector: 'page-accept',
	templateUrl: 'accept.html'
})

export class AcceptPage {
	@ViewChild('map') mapElement: ElementRef;
	map: any;

	constructor(public navCtrl: NavController, public modalCtrl: ModalController, public viewCtrl: ViewController) {

	}

	ionViewDidLoad() {
		let latLng = new google.maps.LatLng(20.5937, 78.9629);
		this.loadMap(latLng);
	}

	loadMap(latLng) {
		let mapOptions = {
			center: latLng,
			zoom: 12,
			mapTypeControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
	}

	//	acceptriderPage() {
	//		let modal = this.modalCtrl.create(AcceptriderPage);
	//		modal.present();
	//    }

	cancelride() {
		let modal = this.modalCtrl.create(CancelridePage);
		modal.present();
	}
	riderinfo() {
		let modal = this.modalCtrl.create(RiderinfoPage);
		modal.present();
	}

	dismiss() {
		this.viewCtrl.dismiss();
	}
	location() {
		this.navCtrl.push(LocationPage);
	}


}
