import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthService } from '../../services/auth.service';
import { AlertService } from '../../services/alert.service';
import { NgForm } from '@angular/forms';

import { VerificationPage } from '../verification/verification';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {

  constructor(public navCtrl: NavController,
              private authService: AuthService,
              private alertService: AlertService) {

  }
    register(form: NgForm) {
        this.authService.register(form.value.first_name, form.value.last_name, form.value.city, form.value.phone_number, form.value.email, form.value.password, form.value.password_confirmation, form.value.role, form.value.gender).subscribe(
            data => {
                this.alertService.presentToast(data['message']);
            },
            error => {
                console.log(error);
            },
            () => {
                this.navCtrl.setRoot(LoginPage);
            }
        );
    }
    loginPage(){
        this.navCtrl.setRoot(LoginPage);
    }

}
