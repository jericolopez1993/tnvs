export class User {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    city: string;
    phone_number: string;
    user_type: string;
}