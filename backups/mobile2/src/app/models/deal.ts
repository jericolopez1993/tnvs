import {DEAL_STATUS_PENDING} from "../../environments/environment.prod";

export class Deal {
    id: number;
    passenger_id: number;
    driver_id: number;
    origin: string;
    destination: string;
    distance: number;
    fee: number;
    note: string;
    payment_method: string;
    status: string;
    promo_code: string;
    discount: number;
    currency: string;
    trip_id: number;
}
