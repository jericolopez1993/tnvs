import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../services/auth.service';
import { CommonService } from '../services/common.service';
import { Router } from '@angular/router';
import { ENABLE_SIGNUP } from 'src/environments/environment.prod';
import { MenuController } from '@ionic/angular';
// import { Facebook } from '@ionic-native/facebook/ngx';
// import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    user: any = { photoURL: 'http://placehold.it/50x50' }; //setting default image, if user dont have images


  ngOnInit() {
  }

  email: string = "";
  password: string = "";
  isRegisterEnabled: any = true;
  constructor(
    // private fb: Facebook,
    // private nativeStorage: NativeStorage,
    private authService: AuthService,
    private translate: TranslateService,
    private commonService: CommonService,
    private router: Router,
    private menuCtrl: MenuController

  ) {
    this.isRegisterEnabled = ENABLE_SIGNUP;
    this.menuCtrl.enable(false);
  }


  reset() {
    if (this.email) {
        this.authService.resetPassword(this.email).subscribe(
            data => {
                this.commonService.showToast('Reset Password instruction send to your email address. Please check into your inbox or spam. Thanks you.');
            },
            error => {
                this.commonService.showToast(error.message);
            },
            () => {
                this.router.navigateByUrl('/login');
            }
        );
    }
  }

  login() {
    if (this.email.length == 0 || this.password.length == 0) {
      this.commonService.showAlert("Invalid Credentials");
    }
    else {

      this.commonService.showLoader('Authenticating...');
      this.authService.login(this.email, this.password).subscribe(
          data => {
              this.user = data;
              console.log("login:::::" + this.user.user_type);
              if (this.user["user"]["user_type"] == 'driver') {
                  this.router.navigateByUrl('/driver/home');
              }else{
                  this.router.navigateByUrl('/rider/home');
              }
              this.commonService.hideLoader();
          },
          error => {
              this.commonService.hideLoader();
              this.commonService.showToast(error.message);
          }
      );
    }

  }
    // fbLogin() {
    //     this.commonService.showLoader('Authenticating...');
    //     //the permissions your facebook app needs from the user
    //     const permissions = ["public_profile", "email"];
    //
    //     this.fb.login(permissions)
    //         .then(response => {
    //             let userId = response.authResponse.userID;
    //             //Getting name and email properties
    //             //Learn more about permissions in https://developers.facebook.com/docs/facebook-login/permissions
    //
    //             this.fb.api("/me?fields=name,email", permissions)
    //                 .then(user => {
    //                     user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
    //                     //now we have the users info, let's save it in the NativeStorage
    //                     this.nativeStorage.setItem('facebook_user',
    //                         {
    //                             name: user.name,
    //                             email: user.email,
    //                             picture: user.picture
    //                         })
    //                         .then(() => {
    //                             this.router.navigateByUrl('/rider/home');
    //                             this.commonService.hideLoader();
    //                         }, error => {
    //                             console.log(error);
    //                             this.commonService.hideLoader();
    //                         })
    //                 })
    //         }, error =>{
    //             console.log(error);
    //             this.commonService.showToast(error.message);
    //             this.commonService.hideLoader();
    //         });
    //
    // }



}
