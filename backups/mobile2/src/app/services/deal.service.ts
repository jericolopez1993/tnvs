import { Injectable } from '@angular/core';
import { DEAL_STATUS_PENDING } from 'src/environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { tap } from 'rxjs/operators';
import { EnvService } from './env.service';
import { AuthService } from './auth.service';
import {Deal} from "../models/deal";

@Injectable({
  providedIn: 'root'
})
export class DealService {

    user: any;
    token:any;
    user_id: any;
    trip:any;
  constructor(
      private http: HttpClient,
      private storage: NativeStorage,
      private env: EnvService,
      private authService: AuthService) {
      this.user_id = this.authService.getUserId();
      this.token = this.authService.getToken();
  }

  // sort driver by rating & distance
  sortDriversList(drivers: Array<any>) {
    return drivers.sort((a, b) => {
      return (a.rating - a.distance / 5) - (b.rating - b.distance / 5);
    })
  }

  // make deal to driver
  makeDeal(driverId, origin, destination, distance, fee, currency, note, paymentMethod, promocode, discount) {
    return this.http.post(this.env.API_URL + 'deals',
          {deal: { passenger_id: this.user_id,
                  driver_id: driverId,
                  currency: currency,
                  origin: JSON.stringify(origin),
                  destination: JSON.stringify(destination),
                  distance: distance,
                  fee: fee,
                  note: note,
                  payment_method: paymentMethod,
                  status: DEAL_STATUS_PENDING,
                  promo_code: promocode,
                  discount: discount,
                  created_at_number: Date.now()}});
  }

  // get deal by driverId
  getDriverDeal(driverId) {
      return this.http.get<Deal>(this.env.API_URL + 'deals/' + driverId)
          .pipe(
              tap(deal => {
                  return deal;
              })
          );
  }

  // remove deal
  // removeDeal(driverId) {
  //     return this.http.get<User>(this.env.API_URL + 'deals/' + driverId)
  //         .pipe(
  //             tap(trip => {
  //                 return trip;
  //             })
  //         );
  //     return this.http.post(this.env.API_URL + 'deals',
  //         {trip: { passengerId: user.id,
  //                 currency: currency,
  //                 origin: origin,
  //                 destination: destination,
  //                 distance: distance,
  //                 fee: fee,
  //                 note: note,
  //                 paymentMethod: paymentMethod,
  //                 status: DEAL_STATUS_PENDING,
  //                 createdAt: Date.now(),
  //                 promocode: promocode,
  //                 discount: discount}}
  // }
}
