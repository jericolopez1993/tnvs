import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { tap } from 'rxjs/operators';
import { EnvService } from '../services/env.service';
import {User} from "../models/user";

@Injectable({
  providedIn: 'root'
})
export class DriverService {

    user: any;
    token:any;
    constructor(private http: HttpClient,
                private storage: NativeStorage,
                private env: EnvService,
                private authService: AuthService) {
        this.user = this.authService.getUserData();
        this.token = this.authService.getToken();
    }

  // get driver by id
  getDriver(id) {
      return this.http.get(this.env.API_URL + 'localities/' + id)
          .pipe(
              tap(driver => {
                  return driver;
              })
          )
  }

  // get driver position
  getDriverPosition(locality, vehicleType, id) {
      return this.http.get(this.env.API_URL + 'localities/' + id)
          .pipe(
              tap(driver => {
                  return driver;
              })
          )
  }

  getActiveDriver(locality, vehicleType) {
      return this.http.get(this.env.API_URL + 'localities?locality=' + locality + '&vehicle_type=' + vehicleType)
          .pipe(
              tap(drivers => {
                  return drivers;
              })
          )
  }

  // calculate vehicle angle
  calcAngle(oldLat, oldLng, lat, lng) {
    let brng = Math.atan2(lat - oldLat, lng - oldLng);
    brng = brng * (180 / Math.PI);

    return brng;
  }

  // return icon suffix by angle
  getIconWithAngle(vehicle) {
    let angle = this.calcAngle(vehicle.oldLat, vehicle.oldLng, vehicle.lat, vehicle.lng);

    if (angle >= -180 && angle <= -160) {
      return '_left';
    }

    if (angle > -160 && angle <= -110) {
      return '_bottom_left';
    }

    if (angle > -110 && angle <= -70) {
      return '_bottom';
    }

    if (angle > -70 && angle <= -20) {
      return '_bottom_right';
    }

    if (angle >= -20 && angle <= 20) {
      return '_right';
    }

    if (angle > 20 && angle <= 70) {
      return '_top_right';
    }

    if (angle > 70 && angle <= 110) {
      return '_top';
    }

    if (angle > 110 && angle <= 160) {
      return '_top_left';
    }

    if (angle > 160 && angle <= 180) {
      return '_left';
    }
  }
}
