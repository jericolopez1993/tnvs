import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Storage } from '@ionic/storage';
import { tap } from 'rxjs/operators';
import { EnvService } from './env.service';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
    isLoggedIn = false;
    token:any;
    user: any;

  constructor(private http: HttpClient,
              private env: EnvService,
              public storage: Storage) { }

  // get current user data from firebase
  getUserData() {
      return this.storage.get('token').then(
          user => {
              this.token = user;
              return user;
          }
      );

  }
  getToken() {
      if (!this.token) return null;
      return this.token["user"]["authentication_token"];
  }
  getName() {
      if (!this.token) return null;
      return this.token["user"]["first_name"] + " " + this.token["user"]["last_name"];
  }

  getEmail() {
      if (!this.token) return null;
      return this.token["user"]["email"];
  }
  getUserId(){
      if (!this.token) return null;
      return this.token["user"]["id"];
  }
  getUserType() {
      if (!this.token) return null;
      return this.token["user"]["user_type"];
   }

  // get passenger by id
  getUser() {
      const headers = new HttpHeaders({
          'Authorization': "Bearer "+this.token["user"]["authentication_token"]
      });
      return this.http.get<User>(this.env.API_URL + 'users', { headers: headers })
          .pipe(
              tap(user => {
                  return user;
              })
          )
  }

  // login by email and password
  login(email, password) {
      return this.http.post(this.env.API_URL + 'sign_in',
          {email: email, password: password}
      ).pipe(
          tap(token => {
              this.storage.set('token', token)
                  .then(
                      () => {
                          console.log('Token Stored');
                      },
                      error => console.error('Error storing item', error)
                  );
              this.token = token;
              this.isLoggedIn = true;
              return token;
          }),
      );
  }

  resetPassword(email) {
      return this.http.post(this.env.API_URL + 'users/reset_password',
          {email: email}
      )
  }

  sendVerificationEmail(email: String) {
        return this.http.post(this.env.API_URL + 'users/resent_verification',
          {email: email}
         )
  }

  logout() {
      const headers = new HttpHeaders({
          'Authorization': "Bearer "+this.token["user"]["authentication_token"]
      });
      return this.http.delete(this.env.API_URL + 'sign_out', { headers: headers })
          .pipe(
              tap(data => {
                  this.storage.remove("token");
                  this.isLoggedIn = false;
                  delete this.token;
                  return data;
              })
          )
  }

  // register new account
    register(first_name: String, last_name: String, city: String, phone_number: String, email: String, password: String, password_confirmation: String, user_type: String) {
        return this.http.post(this.env.API_URL + 'sign_up',
            {user: {first_name: first_name, last_name: last_name, city: city, phone_number: phone_number, email: email, password: password, password_confirmation: password_confirmation, user_type: user_type}}
        )
    }

  // update user display name and photo
  updateUserProfile(first_name: String, last_name: String, city: String, phone_number: String) {
      const headers = new HttpHeaders({
          'Authorization': "Bearer "+this.token["user"]["authentication_token"]
      });
      return this.http.put<User>(this.env.API_URL + 'users',
          {user: {first_name: first_name, last_name: last_name, city: city, phone_number: phone_number}}, {headers: headers} ).pipe(
          tap(user => {
              return user;
          })
      )
  }


    checkToken() {
        this.storage.get('token').then(token => {
            if (token['user']['authentication_token'] !== null && token['user']['authentication_token'] !== ""  && token['user']['authentication_token'] !== undefined ) {
                this.isLoggedIn=true;
            }else{
                this.isLoggedIn=false;
            }
        });
    }

  // // create new user if not exist
  // createUserIfNotExist(user) {
  //   this.getUser(user.uid).valueChanges().pipe(take(1)).subscribe((snapshot: any) => {
  //     if (snapshot === null) {
  //       this.updateUserProfile(user);
  //     }
  //   });
  // }

  // update card setting
  // updateCardSetting(number, exp, cvv, token) {
  //   const user = this.getUserData();
  //   this.db.object('passengers/' + user.uid + '/card').update({
  //     number: number,
  //     exp: exp,
  //     cvv: cvv,
  //     token: token
  //   })
  // }
  //
  // // get card setting
  // getCardSetting() {
  //   const user = this.getUserData();
  //   return this.db.object('passengers/' + user.uid + '/card');
  // }

}
