import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { tap } from 'rxjs/operators';
import { EnvService } from './env.service';

@Injectable({
  providedIn: 'root'
})
export class SettingService {

  constructor(private http: HttpClient,
              private storage: NativeStorage,
              private env: EnvService) { }

  getPrices() {
    // return this.db.object('master_settings/prices');
      return this.http.get(this.env.API_URL + 'master_settings/prices')
          .pipe(
              tap(price => {
                  return price;
              })
          )
  }
}
