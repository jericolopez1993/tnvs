import { Injectable } from '@angular/core';
import {
    TRIP_STATUS_WAITING,
    TRIP_STATUS_GOING,
    TRIP_STATUS_FINISHED,
    TRIP_STATUS_CANCELED,
    DEAL_STATUS_PENDING, DEAL_STATUS_ACCEPTED
} from "../../../environments/environment.prod";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { tap } from 'rxjs/operators';
import { EnvService } from './../../services/env.service';
import { AuthService } from './../../services/auth.service';
import {Trip} from "../../models/trip";
import {User} from "../../models/user";
import * as moment from 'moment';


@Injectable({
  providedIn: 'root'
})
export class TripService {

  currentTrip: any;
  user: any;
  user_id: any;
  token:any;
    trips:[];
  constructor(private http: HttpClient,
              private storage: NativeStorage,
              private env: EnvService,
              private authService: AuthService) {
      this.user_id = this.authService.getUserId();
      this.token = this.authService.getToken();
  }

  // create trip from deal object
  createFromDeal(deal) {
    deal.status = TRIP_STATUS_WAITING;
    deal.otp = Math.floor(Math.random() * 9999);
    return this.http.post(this.env.API_URL + 'trips',
        {deal_id: deal.id, status: DEAL_STATUS_ACCEPTED, trip: { passenger_id: deal.passenger_id,
                driver_id: deal.driver_id,
                currency: deal.currency,
                origin: JSON.stringify(deal.origin),
                destination: JSON.stringify(deal.destination),
                distance: deal.distance,
                fee: deal.fee,
                note: deal.note,
                payment_method: deal.payment_method,
                status: DEAL_STATUS_ACCEPTED,
                promo_code: deal.promo_code,
                discount: deal.discount,
                created_at_number: Date.now()}}).pipe(
        tap(trip => {
            return trip;
        })
    )
  }

  // pickup passenger
  pickUp(tripId) {
      return this.http.put(this.env.API_URL + 'trips/' + tripId,
          {trip: {pickup_at_number: Date.now(), pickup_at: moment().format('DD/MM/YYYY HH:mm:ss'), status: TRIP_STATUS_GOING}}).pipe(
          tap(user => {
              return user;
          })
      )
  }

  // drop off
  dropOff(tripId) {
      return this.http.put(this.env.API_URL + 'trips/' + tripId,
          {trip: {drop_off_at_number: Date.now(), drop_off_at: moment().format('DD/MM/YYYY HH:mm:ss'), status: TRIP_STATUS_FINISHED}}).pipe(
          tap(user => {
              return user;
          })
      )
  }

  cancel(tripId) {
      return this.http.put(this.env.API_URL + 'trips/' + tripId,
          {trip: {cancelled_at_number: Date.now(), cancelled_at: moment().format('DD/MM/YYYY HH:mm:ss'), status: TRIP_STATUS_CANCELED}}).pipe(
          tap(user => {
              return user;
          })
      )
  }

  setCurrentTrip(tripId) {
      const headers = new HttpHeaders({
          'Authorization': "Bearer "+this.token
      });
      return this.http.put<User>(this.env.API_URL + 'users',
          {user: {trip_id: tripId}}, {headers: headers} ).pipe(
          tap(user => {
              return user;
          })
      )
  }

  getCurrentTrip() {
    return this.currentTrip;
  }

  getTripStatus(tripId) {
      return this.http.get<Trip>(this.env.API_URL + 'trips/' + tripId)
          .pipe(
              tap(trip => {
                  return trip;
              })
          );
  }

  getPassenger(passengerId) {
      return this.http.get<User>(this.env.API_URL + 'users/' + passengerId)
          .pipe(
              tap(user => {
                  return user;
              })
          );
  }

  // get driver's trip
  getTrips() {
      return this.authService.getUserData().then(
          user => {
              return this.http.get(this.env.API_URL + 'trips?driver_id=' +  user['user']['id'])
                  .pipe(
                      tap(trips => {
                          return trips;
                      })
                  );
          }
      );
  }
}
