import { Injectable } from '@angular/core';
import { AuthService } from './../../services/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { tap } from 'rxjs/operators';
import { EnvService } from './../../services/env.service';
import {User} from "../../models/user";
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DriverService {

  user: any;
  token:any;
  constructor(private http: HttpClient,
              private storage: NativeStorage,
              private env: EnvService,
              private authService: AuthService) {
    this.user = this.authService.getUserData();
    this.token = this.authService.getToken();
  }

  setUser(user) {
    this.user = user;
  }

  // get driver by id
  getDriver() {
    return this.authService.getUserData();
  }
  // update driver's position
  updatePosition(vehicleId, vehicleType, locality, lat, lng) {
      console.log("working.....");
      console.log(this.token);
      const headers = new HttpHeaders({
          'Authorization': "Bearer "+ this.token
      });
      console.log(this.token);

      return this.http.put<User>(this.env.API_URL + 'localities',
          {user: {locality: locality, lat: lat, lng: lng, oldLat: lat, oldLng: lng, last_active: moment().format('DD/MM/YYYY HH:mm:ss'), last_active_number: Date.now()}}, {headers: headers} ).pipe(
          tap(user => {
              return user;
          })
      );
  }

}
