import { Injectable } from '@angular/core';
import { TripService } from './trip.service';
import {DEAL_STATUS_ACCEPTED, TRIP_STATUS_WAITING} from '../../../environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { tap } from 'rxjs/operators';
import { EnvService } from './../../services/env.service';
import { AuthService } from './../../services/auth.service';
import {Deal} from "../../models/deal";
import {User} from "../../models/user";

@Injectable({
  providedIn: 'root'
})
export class DealService {

    user: any;
    user_id: any;
    token:any;
  constructor(private http: HttpClient,
      private storage: NativeStorage,
      private env: EnvService,
      private authService: AuthService,
      private tripService: TripService) {
      this.user_id = this.authService.getUserId();
      this.token = this.authService.getToken();
  }

  getDeal(driverId) {
      return this.http.get<Deal>(this.env.API_URL + 'deals/' + driverId)
          .pipe(
              tap(deal => {
                  return deal;
              })
          );
  }
  removeDeal(driverId) {
      return this.http.delete<Deal>(this.env.API_URL + 'deals/' + driverId)
          .pipe(
              tap(deal => {
                  return deal;
              })
          );
  }

  // accept a deal
  acceptDeal(driverId, deal) {
    deal.driverId = driverId;

    // create trip from deal
    return this.tripService.createFromDeal(deal).subscribe((trip) => {
      this.tripService.setCurrentTrip(trip['id']);
    });
  }
}
