import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
    {
      path: '',
      redirectTo: 'login',
      pathMatch: 'full'
    },
    {
      path: 'rider/home',
      loadChildren: () => import('./rider/home/home.module').then(m => m.HomePageModule),
      canActivate: [AuthGuard]
    },
    {
        path: 'driver/home',
        loadChildren: () => import('./driver/home/home.module').then(m => m.HomePageModule),
        canActivate: [AuthGuard]
    },
    { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
    { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
    { path: 'rider/map', loadChildren: './rider/map/map.module#MapPageModule', canActivate: [AuthGuard] },
    { path: 'rider/tracking', loadChildren: './rider/tracking/tracking.module#TrackingPageModule', canActivate: [AuthGuard] },
    { path: 'rider/profile', loadChildren: './rider/profile/profile.module#ProfilePageModule', canActivate: [AuthGuard] },
    { path: 'rider/history', loadChildren: './rider/history/history.module#HistoryPageModule', canActivate: [AuthGuard] },
    { path: 'rider/payments', loadChildren: './rider/payments/payments.module#PaymentsPageModule', canActivate: [AuthGuard] },
    { path: 'rider/rating', loadChildren: './rider/rating/rating.module#RatingPageModule', canActivate: [AuthGuard] },
    { path: 'driver/history', loadChildren: './driver/history/history.module#HistoryPageModule', canActivate: [AuthGuard] },
    { path: 'driver/profile', loadChildren: './driver/profile/profile.module#ProfilePageModule', canActivate: [AuthGuard] },
    { path: 'driver/wallet', loadChildren: './driver/wallet/wallet.module#WalletPageModule', canActivate: [AuthGuard] },
    { path: 'driver/settings', loadChildren: './driver/settings/settings.module#SettingsPageModule', canActivate: [AuthGuard] },
    { path: 'driver/pickup', loadChildren: './driver/pickup/pickup.module#PickupPageModule', canActivate: [AuthGuard] }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
