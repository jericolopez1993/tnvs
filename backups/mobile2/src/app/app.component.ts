import {Component} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {TranslateService} from '@ngx-translate/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';
import {TripService} from './services/trip.service';
import {AuthService} from './services/auth.service';


@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {
    public appPages = [
        {
            title: 'Register',
            url: '/register',
            icon: 'home'
        }

    ];
    public driverPages = [
        {
            title: 'Home',
            url: '/driver/home',
            icon: 'home'
        },
        {
            title: 'Ride History',
            url: '/driver/history',
            icon: 'time'
        },
        {
            title: 'Wallet',
            url: '/driver/wallet',
            icon: 'wallet'
        },
        {
            title: 'Settings',
            url: '/settings',
            icon: 'settings'
        }

    ];

    positionTracking: any;
    driver: any;
    user: any = {};

    public riderPages = [
        {
            title: 'Home',
            url: '/rider/home',
            icon: 'home'
        },
        {
            title: 'Ride History',
            url: '/rider/history',
            icon: 'time'
        },
        {
            title: 'Payments',
            url: '/rider/payments',
            icon: 'card'
        },
        {
            title: 'Profile',
            url: '/rider/profile',
            icon: 'contact'
        },

    ];

    constructor(
        private authService: AuthService,
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private translate: TranslateService,
        private afAuth: AngularFireAuth,
        private router: Router,
        // private tripService: TripService
    ) {
        this.initializeApp();
    }

    current_user_name() {
        return this.authService.getName();
    }

    current_user_email() {
        return this.authService.getEmail();
    }

    current_user_type() {
        return this.authService.getUserType();
    }

    isRider() {
        var user_type = this.current_user_type();
        return user_type == 'user';
        1
    }

    isDriver() {
        var user_type = this.current_user_type();
        return user_type == 'driver';
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.translate.setDefaultLang('en');
            this.translate.use('en');
            this.authService.checkToken();
            this.authService.getUserData().then(
                user => {
                    if (user) {
                        this.user = user;
                        if (this.user['user']['user_type'] == 'driver') {
                            this.router.navigateByUrl('/driver/home');
                        }else{
                            this.router.navigateByUrl('/rider/home');
                        }
                    } else {
                        this.router.navigateByUrl('/login');
                    }

                }
            );
            // this.afAuth.authState.subscribe(authData => this.user = authData);
            // this.afAuth.authState.pipe(take(1)).subscribe(authData => {
            //   if (authData) {
            //     this.tripService.getTrips().valueChanges().subscribe((trips: any) => {
            //       trips.forEach(trip => {
            //         if (trip.status === 'waiting' || trip.status === 'accepted' || trip.status === 'going') {
            //           this.tripService.setId(trip.key)
            //           this.router.navigateByUrl('tracking');
            //         }
            //         else if (trip.status === 'finished') {
            //           this.router.navigateByUrl('/home');
            //         }
            //       })
            //     })
            //     this.router.navigateByUrl('/home');
            //   } else {
            //     this.router.navigateByUrl('/login');
            //   }
            // });
        });
    }
}
