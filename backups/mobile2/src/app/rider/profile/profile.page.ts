import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { CommonService } from '../../services/common.service';
import { Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  user: any = { photoURL: 'http://placehold.it/50x50' }; //setting default image, if user dont have images
  constructor(
    private authService: AuthService,
    private common: CommonService,
    private router: Router,
    private afStorage: AngularFireStorage,
    private platform: Platform
  ) { }

  ngOnInit() {
      this.authService.getUserData().then(
          user => {
              this.user = user['user'];
          }
      );
  }

  // save user info
  save() {
      this.authService.updateUserProfile(this.user.first_name, this.user.last_name, this.user.city, this.user.phone_number).subscribe(
          data => {
              this.common.showToast("Updated");
          },
          error => {
              console.log(error);
          },
          () => {
          }
      );
  }

  // choose file for upload
  chooseFile() {
    document.getElementById('avatar').click();
  }

  upload() {
    // Create a root reference
    this.common.showLoader('Uploading..');

    for (let selectedFile of [(<HTMLInputElement>document.getElementById('avatar')).files[0]]) {
      let path = '/users/' + Date.now() + `_${selectedFile.name}`;
      let ref = this.afStorage.ref(path)
      ref.put(selectedFile).then(() => {
        ref.getDownloadURL().subscribe(data => { this.user.photoURL = data; });
        this.common.hideLoader()
      }).catch(err => {
        this.common.hideLoader();
        console.log(err)
      });

    }
  }
  verifyEmail() {
    this.authService.sendVerificationEmail(this.user.email).subscribe(
        data => {
            this.common.showToast("Verfied Successfully");
        },
        error => {
            console.log(error);
        });
  }

  logout() {
      this.authService.logout().subscribe(
          data => {
              this.common.showToast("Sign out Successful. Thank you.");
          },
          error => {
              console.log(error);
          },
          () => {
              this.router.navigateByUrl('/login');
          }
      );
  }


}
