export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyASfN_OVXgjuLbKKv6TKul-1S14joHFzww",
    authDomain: "ionfiretaxi.firebaseapp.com",
    databaseURL: "https://ionfiretaxi.firebaseio.com",
    projectId: "ionfiretaxi",
    storageBucket: "ionfiretaxi.appspot.com",
    messagingSenderId: "493104185856"
  }
};

export let FB_APP_ID = 2392489640796829;

export let SHOW_VEHICLES_WITHIN = 5; // within 5km
export let POSITION_INTERVAL = 10000; // 2000ms
export let VEHICLE_LAST_ACTIVE_LIMIT = 60000; // 60s

export let DEAL_STATUS_PENDING = 'pending';
export let DEAL_STATUS_ACCEPTED = 'accepted';
export let TRIP_STATUS_GOING = 'going';
export let TRIP_STATUS_FINISHED = 'finished';
export let DEAL_TIMEOUT = 20000; // 20s

export let EMAIL_VERIFICATION_ENABLED = true; // send verification email after user register
export let ENABLE_SIGNUP = true;
export let SOS = "+919500707757";
export let DEFAULT_AVATAR = "http://placehold.it/150x150";




export let DRIVER_INIT_BALANCE = 10; // balance when user signed up for first time
export let DRIVER_INIT_RATING = 5; // rating when user signedup for first time


export let TRIP_STATUS_WAITING = 'waiting';
export let TRIP_STATUS_CANCELED = 'canceled';
export let TRANSACTION_TYPE_WITHDRAW = 'withdraw';

// Global Settings
export let DEFAULT_COUNTRY_CODE = "IN";  // used in AccountKit Mobile verification
export let DEFAULT_COUNTRY_MOBILE_CODE = "+91";

export let APPROVAL_REQUIRED = false; // driver can ride without any approval
export let CURRENCY_SYMBOL = "$";
export let PLAY_AUDIO_ON_REQUEST = true;
export let AUDIO_PATH = "./assets/audio/sound.mp3" //must have mp3 file

export let CUSTOMER_CARE = "1234567890";