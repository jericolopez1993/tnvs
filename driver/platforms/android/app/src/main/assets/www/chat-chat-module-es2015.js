(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["chat-chat-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/chat/chat.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/chat/chat.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n\t\t<ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Chat</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\t<div id=\"chats\">\n\t\t<div class=\"message\" *ngFor=\"let chat of chats\" [class]=\"chat.specialMessage ? 'message special' : 'message '\">\n\t\t\t<div [class]=\"chat.email == email ? 'chats messageRight' : 'chats messageLeft'\">\n\t\t\t\t<div class=\"username\">{{ chat.name }} </div>\n\t\t\t\t<div class=\"chatMessage\">{{ chat.message }} </div>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar>\n\t<ion-row class=\"footer\">\n\t    <ion-col size=\"9\">\n\t   \t\t<ion-input type=\"text\" [(ngModel)]=\"message\" name=\"message\"></ion-input>\n\t   \t</ion-col>\n\t   \t<ion-col size=\"3\">\n\t   \t\t<ion-button (click)=\"sendMessage()\">\n\t\t\t  <ion-icon slot=\"icon-only\" name=\"md-send\"></ion-icon>\n\t\t\t</ion-button>\n\t\t</ion-col>\n\t</ion-row>\n  </ion-toolbar>\n</ion-footer>\n");

/***/ }),

/***/ "./src/app/chat/chat.module.ts":
/*!*************************************!*\
  !*** ./src/app/chat/chat.module.ts ***!
  \*************************************/
/*! exports provided: ChatPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPageModule", function() { return ChatPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _chat_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./chat.page */ "./src/app/chat/chat.page.ts");







const routes = [
    {
        path: '',
        component: _chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"]
    }
];
let ChatPageModule = class ChatPageModule {
};
ChatPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_chat_page__WEBPACK_IMPORTED_MODULE_6__["ChatPage"]]
    })
], ChatPageModule);



/***/ }),

/***/ "./src/app/chat/chat.page.scss":
/*!*************************************!*\
  !*** ./src/app/chat/chat.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".chats {\n  display: inline-block;\n  padding: 5px 10px;\n  background: #3880ff;\n  margin: 5px;\n  color: #ffffff;\n  border-radius: 11px;\n  font-size: 15px;\n}\n\n.username {\n  text-align: right;\n  font-size: 11px;\n  padding-bottom: 7px;\n  color: black;\n}\n\n.messageLeft {\n  float: left;\n  background: #eee;\n  margin-right: 30%;\n}\n\n.messageRight {\n  float: right;\n  margin-left: 30%;\n}\n\n.chats.messageLeft .chatMessage {\n  color: #000;\n}\n\n.footer {\n  width: 100%;\n  margin: 0;\n  padding: 0;\n  background: #fff;\n  color: #000;\n}\n\n.message:after {\n  content: \"\";\n  display: block;\n  clear: both;\n}\n\n.message.special .chats .chatMessage {\n  color: #000;\n  display: inline-block;\n  padding: 0 10px;\n}\n\n.message.special .chats:before {\n  content: \"\";\n  height: 2px;\n  background: #333;\n  display: block;\n  top: 50px;\n  position: absolute;\n  left: 0;\n  width: 100%;\n  z-index: -1;\n}\n\n.message.special .chats {\n  background: transparent;\n  color: #000;\n  font-size: 80%;\n  width: 100%;\n  float: none;\n  text-align: center;\n  position: relative;\n  padding: 3px 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2hhdC9DOlxcVXNlcnNcXEplcmljbyBQYXVsb1xcUnVieW1pbmVQcm9qZWN0c1xcdG52c1xcZHJpdmVyL3NyY1xcYXBwXFxjaGF0XFxjaGF0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvY2hhdC9jaGF0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FDQ0Q7O0FERUE7RUFDQyxpQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNDRDs7QURHQTtFQUNDLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FDQUQ7O0FER0E7RUFDQyxZQUFBO0VBQ0EsZ0JBQUE7QUNBRDs7QURHQTtFQUNDLFdBQUE7QUNBRDs7QURJQTtFQUNDLFdBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQ0REOztBRElBO0VBQ0MsV0FBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0FDREQ7O0FESUE7RUFDQyxXQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0FDREQ7O0FESUE7RUFDQyxXQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLFNBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtBQ0REOztBRElBO0VBQ0MsdUJBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FDREQiLCJmaWxlIjoic3JjL2FwcC9jaGF0L2NoYXQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNoYXRzIHtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcblx0cGFkZGluZzogNXB4IDEwcHg7XHJcblx0YmFja2dyb3VuZDogIzM4ODBmZjs7XHJcblx0bWFyZ2luOiA1cHg7XHJcblx0Y29sb3I6ICNmZmZmZmY7XHJcblx0Ym9yZGVyLXJhZGl1czogMTFweDtcclxuXHRmb250LXNpemU6IDE1cHg7XHJcbn1cclxuXHJcbi51c2VybmFtZSB7XHJcblx0dGV4dC1hbGlnbjogcmlnaHQ7XHJcblx0Zm9udC1zaXplOiAxMXB4O1xyXG5cdHBhZGRpbmctYm90dG9tOiA3cHg7XHJcblx0Y29sb3I6IGJsYWNrO1xyXG59XHJcblxyXG5cclxuLm1lc3NhZ2VMZWZ0IHtcclxuXHRmbG9hdDogbGVmdDtcclxuXHRiYWNrZ3JvdW5kOiAjZWVlO1xyXG5cdG1hcmdpbi1yaWdodDogMzAlO1xyXG59XHJcblxyXG4ubWVzc2FnZVJpZ2h0IHtcclxuXHRmbG9hdDogcmlnaHQ7XHJcblx0bWFyZ2luLWxlZnQ6IDMwJTtcclxufVxyXG5cclxuLmNoYXRzLm1lc3NhZ2VMZWZ0IC5jaGF0TWVzc2FnZSB7XHJcblx0Y29sb3I6ICMwMDA7XHJcbn1cclxuXHJcblxyXG4uZm9vdGVyIHtcclxuXHR3aWR0aDogMTAwJTtcclxuXHRtYXJnaW46IDA7XHJcblx0cGFkZGluZzogMDtcclxuXHRiYWNrZ3JvdW5kOiAjZmZmO1xyXG5cdGNvbG9yOiAjMDAwO1xyXG59XHJcblxyXG4ubWVzc2FnZTphZnRlciB7XHJcblx0Y29udGVudDogJyc7XHJcblx0ZGlzcGxheTogYmxvY2s7XHJcblx0Y2xlYXI6IGJvdGg7XHJcbn1cclxuXHJcbi5tZXNzYWdlLnNwZWNpYWwgLmNoYXRzIC5jaGF0TWVzc2FnZSB7XHJcblx0Y29sb3I6ICMwMDA7XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdHBhZGRpbmc6IDAgMTBweDtcclxufVxyXG5cclxuLm1lc3NhZ2Uuc3BlY2lhbCAuY2hhdHM6YmVmb3JlIHtcclxuXHRjb250ZW50OiAnJztcclxuXHRoZWlnaHQ6IDJweDtcclxuXHRiYWNrZ3JvdW5kOiAjMzMzO1xyXG5cdGRpc3BsYXk6IGJsb2NrO1xyXG5cdHRvcDogNTBweDtcclxuXHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0bGVmdDogMDtcclxuXHR3aWR0aDogMTAwJTtcclxuXHR6LWluZGV4OiAtMTtcclxufVxyXG5cclxuLm1lc3NhZ2Uuc3BlY2lhbCAuY2hhdHMge1xyXG5cdGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG5cdGNvbG9yOiAjMDAwO1xyXG5cdGZvbnQtc2l6ZTogODAlO1xyXG5cdHdpZHRoOiAxMDAlO1xyXG5cdGZsb2F0OiBub25lO1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0cGFkZGluZzogM3B4IDEwcHg7XHJcblx0XHJcbn1cclxuIiwiLmNoYXRzIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBwYWRkaW5nOiA1cHggMTBweDtcbiAgYmFja2dyb3VuZDogIzM4ODBmZjtcbiAgbWFyZ2luOiA1cHg7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBib3JkZXItcmFkaXVzOiAxMXB4O1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cbi51c2VybmFtZSB7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBmb250LXNpemU6IDExcHg7XG4gIHBhZGRpbmctYm90dG9tOiA3cHg7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuLm1lc3NhZ2VMZWZ0IHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG4gIG1hcmdpbi1yaWdodDogMzAlO1xufVxuXG4ubWVzc2FnZVJpZ2h0IHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBtYXJnaW4tbGVmdDogMzAlO1xufVxuXG4uY2hhdHMubWVzc2FnZUxlZnQgLmNoYXRNZXNzYWdlIHtcbiAgY29sb3I6ICMwMDA7XG59XG5cbi5mb290ZXIge1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nOiAwO1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBjb2xvcjogIzAwMDtcbn1cblxuLm1lc3NhZ2U6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICBkaXNwbGF5OiBibG9jaztcbiAgY2xlYXI6IGJvdGg7XG59XG5cbi5tZXNzYWdlLnNwZWNpYWwgLmNoYXRzIC5jaGF0TWVzc2FnZSB7XG4gIGNvbG9yOiAjMDAwO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBhZGRpbmc6IDAgMTBweDtcbn1cblxuLm1lc3NhZ2Uuc3BlY2lhbCAuY2hhdHM6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgaGVpZ2h0OiAycHg7XG4gIGJhY2tncm91bmQ6ICMzMzM7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0b3A6IDUwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIHotaW5kZXg6IC0xO1xufVxuXG4ubWVzc2FnZS5zcGVjaWFsIC5jaGF0cyB7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBjb2xvcjogIzAwMDtcbiAgZm9udC1zaXplOiA4MCU7XG4gIHdpZHRoOiAxMDAlO1xuICBmbG9hdDogbm9uZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHBhZGRpbmc6IDNweCAxMHB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/chat/chat.page.ts":
/*!***********************************!*\
  !*** ./src/app/chat/chat.page.ts ***!
  \***********************************/
/*! exports provided: ChatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPage", function() { return ChatPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/es2015/index.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/es2015/index.js");
/* harmony import */ var _services_trip_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/trip.service */ "./src/app/services/trip.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../environments/environment.prod */ "./src/environments/environment.prod.ts");







// import { UserService } from 'src/app/services/user.service';
let ChatPage = class ChatPage {
    constructor(afdb, af, tripDriverService) {
        this.afdb = afdb;
        this.af = af;
        this.tripDriverService = tripDriverService;
        this.email = '';
        this.message = '';
        this.allChat = [];
        this.email = this.af.auth.currentUser.email;
        this.name = this.af.auth.currentUser.displayName;
        this.username = this.email.split("@")[0];
        this.getMessage();
    }
    sendMessage() {
        if (this.message) {
            this.allChat.push({
                email: this.email,
                message: this.message,
                name: this.name
            })
                .then(data => {
                this.message = '';
                this.getMessage();
            })
                .catch(error => console.log(error));
        }
    }
    getMessage() {
        this.tripDriverService.getTrips().valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1)).subscribe((trips) => {
            trips.forEach(trip => {
                if (trip.status == _environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__["TRIP_STATUS_WAITING"] || trip.status == _environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__["TRIP_STATUS_GOING"]) {
                    this.allChat = this.afdb.list('/chats/' + trip.key + '/');
                    this.allChat.valueChanges().subscribe(data => {
                        this.chats = data;
                    });
                }
            });
        });
        this.tripDriverService.getTrips().valueChanges().subscribe((trips) => {
            trips.forEach(trip => {
                if (trip.status === 'waiting' || trip.status === 'accepted' || trip.status === 'going') {
                    this.allChat = this.afdb.list('/chats/' + trip.key + '/');
                    this.allChat.valueChanges().subscribe(data => {
                        this.chats = data;
                    });
                }
            });
        });
    }
    ngOnInit() { }
};
ChatPage.ctorParameters = () => [
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"] },
    { type: _services_trip_service__WEBPACK_IMPORTED_MODULE_4__["TripService"] }
];
ChatPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-chat',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./chat.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/chat/chat.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./chat.page.scss */ "./src/app/chat/chat.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"], _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"], _services_trip_service__WEBPACK_IMPORTED_MODULE_4__["TripService"]])
], ChatPage);



/***/ })

}]);
//# sourceMappingURL=chat-chat-module-es2015.js.map