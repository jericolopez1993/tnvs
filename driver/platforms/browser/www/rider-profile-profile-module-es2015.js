(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rider-profile-profile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/profile/profile.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rider/profile/profile.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>{{'BASIC_PROFILE' | translate}}</ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"logout()\">Logout</ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <div style=\"text-align:center\">\n    <img src=\"{{ user.photoURL }}\" onError=\"this.src='assets/img/default-dp.png'\" style=\"width:50px;height:50px;border-radius:100px\" (click)=\"chooseFile()\">\n    <form ngNoForm>\n      <input id=\"avatar\" name=\"file\" type=\"file\" (change)=\"upload()\">\n    </form>\n    <h3>{{user.first_name}} {{user.last_name}}</h3>\n  </div>\n  <ion-list lines=\"none\">\n    <ion-item>\n      <ion-label position=\"stacked\" color=\"primary\">{{'FIRST_NAME' | translate}}</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"user.first_name\"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"stacked\" color=\"primary\">{{'MIDDLE_NAME' | translate}}</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"user.middle_name\"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"stacked\" color=\"primary\">{{'LAST_NAME' | translate}}</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"user.last_name\"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"stacked\" color=\"primary\">{{'BIRTH_DATE' | translate}}</ion-label>\n      <ion-datetime displayFormat=\"MM/DD/YYYY\" placeholder=\"Select Date\" [(ngModel)]=\"user.birth_date\"></ion-datetime>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"stacked\" color=\"primary\">{{'GENDER' | translate}}</ion-label>\n      <ion-select placeholder=\" Select gender\" [(ngModel)]=\"user.gender\">\n        <ion-select-option value=\"female\">Female</ion-select-option>\n        <ion-select-option value=\"male\">Male</ion-select-option>\n      </ion-select>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"stacked\" color=\"primary\">{{'ADDRESS' | translate}}</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"user.address\"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"stacked\" color=\"primary\">{{'PHONE_NUMBER' | translate}}</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"user.phoneNumber\" [disabled]=\"user.isPhoneVerified\"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label position=\"stacked\" color=\"primary\">{{'EMAIL_ADDRESS' | translate}}</ion-label>\n      <ion-input type=\"text\" [(ngModel)]=\"user.email\" disabled></ion-input>\n      <ion-button *ngIf=\"!user.isEmailVerified\" (click)=\"verifyEmail()\">\n        {{'VERIFY' | translate}}</ion-button>\n    </ion-item>\n  </ion-list>\n  <div padding>\n    <ion-button color=\"dark\" expand=\"block\" (click)=\"save()\">{{'SAVE' | translate}}</ion-button>\n  </div>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/rider/profile/profile.module.ts":
/*!*************************************************!*\
  !*** ./src/app/rider/profile/profile.module.ts ***!
  \*************************************************/
/*! exports provided: ProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile.page */ "./src/app/rider/profile/profile.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");








const routes = [
    {
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]
    }
];
let ProfilePageModule = class ProfilePageModule {
};
ProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]]
    })
], ProfilePageModule);



/***/ }),

/***/ "./src/app/rider/profile/profile.page.scss":
/*!*************************************************!*\
  !*** ./src/app/rider/profile/profile.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#avatar {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmlkZXIvcHJvZmlsZS9DOlxcVXNlcnNcXEplcmljbyBQYXVsb1xcUnVieW1pbmVQcm9qZWN0c1xcdG52c1xcbW9iaWxlL3NyY1xcYXBwXFxyaWRlclxccHJvZmlsZVxccHJvZmlsZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3JpZGVyL3Byb2ZpbGUvcHJvZmlsZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9yaWRlci9wcm9maWxlL3Byb2ZpbGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2F2YXRhciB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfSIsIiNhdmF0YXIge1xuICBkaXNwbGF5OiBub25lO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/rider/profile/profile.page.ts":
/*!***********************************************!*\
  !*** ./src/app/rider/profile/profile.page.ts ***!
  \***********************************************/
/*! exports provided: ProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePage", function() { return ProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/es2015/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");








let ProfilePage = class ProfilePage {
    constructor(authService, common, router, afStorage, alertCtrl, platform) {
        this.authService = authService;
        this.common = common;
        this.router = router;
        this.afStorage = afStorage;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.user = {}; //setting default image, if user dont have images
    }
    ngOnInit() {
        this.authService.getUser(this.authService.getUserData().uid).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1)).subscribe((snapshot) => {
            this.user = Object.assign({ uid: snapshot.key }, snapshot.payload.val());
            this.user.isEmailVerified = this.authService.getUserData().emailVerified;
        });
    }
    // save user info
    save() {
        this.authService.updateUserProfile(this.user);
        this.common.showToast("Updated");
    }
    // choose file for upload
    chooseFile() {
        document.getElementById('avatar').click();
    }
    upload() {
        // Create a root reference
        this.common.showLoader('Uploading..');
        for (let selectedFile of [document.getElementById('avatar').files[0]]) {
            let path = '/users/' + Date.now() + `_${selectedFile.name}`;
            let ref = this.afStorage.ref(path);
            ref.put(selectedFile).then(() => {
                ref.getDownloadURL().subscribe(data => { this.user.photoURL = data; });
                this.common.hideLoader();
            }).catch(err => {
                this.common.hideLoader();
                console.log(err);
            });
        }
    }
    verifyEmail() {
        this.authService.sendVerificationEmail().then(data => {
            this.common.showToast("Verfied Successfully");
        }).catch(err => console.log(err));
    }
    logout() {
        this.alertCtrl.create({
            header: 'Sign out',
            message: "Are you sure?",
            buttons: [
                {
                    text: 'No',
                    handler: () => {
                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                        this.authService.logout().then(() => {
                            this.router.navigateByUrl('/login');
                        });
                    }
                }
            ]
        }).then(r => r.present());
    }
};
ProfilePage.ctorParameters = () => [
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] },
    { type: _services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_6__["AngularFireStorage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"] }
];
ProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/profile/profile.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./profile.page.scss */ "./src/app/rider/profile/profile.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
        _angular_fire_storage__WEBPACK_IMPORTED_MODULE_6__["AngularFireStorage"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"]])
], ProfilePage);



/***/ }),

/***/ "./src/app/services/common.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/common.service.ts ***!
  \********************************************/
/*! exports provided: CommonService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonService", function() { return CommonService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



let CommonService = class CommonService {
    constructor(toastCtrl, loadCtrl, alertCtrl) {
        this.toastCtrl = toastCtrl;
        this.loadCtrl = loadCtrl;
        this.alertCtrl = alertCtrl;
    }
    showToast(message) {
        this.toastCtrl.create({ message: message, duration: 3000 }).then(res => res.present());
    }
    showAlert(message) {
        this.alertCtrl.create({
            message: message,
            buttons: ['ok']
        }).then(res => res.present());
    }
    showLoader(message) {
        this.loadCtrl.create({ message: message }).then(res => {
            this.loader = res.present();
            setTimeout(() => this.loadCtrl.dismiss(), 10000);
        });
    }
    hideLoader() {
        this.loadCtrl.dismiss();
    }
};
CommonService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] }
];
CommonService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])
], CommonService);



/***/ })

}]);
//# sourceMappingURL=rider-profile-profile-module-es2015.js.map