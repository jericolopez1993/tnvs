function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rider-map-map-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/map/map.page.html":
  /*!*******************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rider/map/map.page.html ***!
    \*******************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppRiderMapMapPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <input class=\"locationinput\" type=\"text\" id=\"searchbar\" #searchbar />\n    <!-- <ion-title>{{ address ? address.formatted_address : '' }}</ion-title> -->\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"selectPlace()\">\n        Done\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n\n</ion-header>\n\n\n<ion-content>\n  <div #map id=\"map\"></div>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/rider/map/map.module.ts":
  /*!*****************************************!*\
    !*** ./src/app/rider/map/map.module.ts ***!
    \*****************************************/

  /*! exports provided: MapPageModule */

  /***/
  function srcAppRiderMapMapModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MapPageModule", function () {
      return MapPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _map_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./map.page */
    "./src/app/rider/map/map.page.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");

    var routes = [{
      path: '',
      component: _map_page__WEBPACK_IMPORTED_MODULE_6__["MapPage"]
    }];

    var MapPageModule = function MapPageModule() {
      _classCallCheck(this, MapPageModule);
    };

    MapPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_map_page__WEBPACK_IMPORTED_MODULE_6__["MapPage"]]
    })], MapPageModule);
    /***/
  },

  /***/
  "./src/app/rider/map/map.page.scss":
  /*!*****************************************!*\
    !*** ./src/app/rider/map/map.page.scss ***!
    \*****************************************/

  /*! exports provided: default */

  /***/
  function srcAppRiderMapMapPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "#map {\n  height: 100%;\n}\n\n.marker {\n  position: fixed;\n  z-index: 1000;\n  top: 45%;\n  left: 45%;\n}\n\n.locationinput {\n  background: #eee;\n  border: 0;\n  outline: 0;\n  width: 100%;\n  padding: 0.5rem;\n  color: #333;\n  margin: 4px;\n  font-size: 14px;\n  border-radius: 4px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmlkZXIvbWFwL0M6XFxVc2Vyc1xcSmVyaWNvIFBhdWxvXFxSdWJ5bWluZVByb2plY3RzXFx0bnZzXFxtb2JpbGUvc3JjXFxhcHBcXHJpZGVyXFxtYXBcXG1hcC5wYWdlLnNjc3MiLCJzcmMvYXBwL3JpZGVyL21hcC9tYXAucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtBQ0NKOztBREVFO0VBQ0UsZUFBQTtFQUNBLGFBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtBQ0NKOztBREVFO0VBQ0UsZ0JBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3JpZGVyL21hcC9tYXAucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI21hcCB7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICB9XG5cbiAgLm1hcmtlciB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHotaW5kZXg6IDEwMDA7XG4gICAgdG9wOjQ1JTtcbiAgICBsZWZ0OiA0NSU7O1xuICB9XG5cbiAgLmxvY2F0aW9uaW5wdXR7XG4gICAgYmFja2dyb3VuZDogI2VlZTtcbiAgICBib3JkZXI6MDtcbiAgICBvdXRsaW5lOjA7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogMC41cmVtO1xuICAgIGNvbG9yOiAjMzMzO1xuICAgIG1hcmdpbjogNHB4O1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIH0iLCIjbWFwIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4ubWFya2VyIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB6LWluZGV4OiAxMDAwO1xuICB0b3A6IDQ1JTtcbiAgbGVmdDogNDUlO1xufVxuXG4ubG9jYXRpb25pbnB1dCB7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG4gIGJvcmRlcjogMDtcbiAgb3V0bGluZTogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDAuNXJlbTtcbiAgY29sb3I6ICMzMzM7XG4gIG1hcmdpbjogNHB4O1xuICBmb250LXNpemU6IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/rider/map/map.page.ts":
  /*!***************************************!*\
    !*** ./src/app/rider/map/map.page.ts ***!
    \***************************************/

  /*! exports provided: MapPage */

  /***/
  function srcAppRiderMapMapPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MapPage", function () {
      return MapPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_place_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../services/place.service */
    "./src/app/services/place.service.ts");
    /* harmony import */


    var _services_trip_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/trip.service */
    "./src/app/services/trip.service.ts");
    /* harmony import */


    var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic-native/geolocation/ngx */
    "./node_modules/@ionic-native/geolocation/ngx/index.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    var MapPage =
    /*#__PURE__*/
    function () {
      function MapPage(router, geolocation, chRef, route, placeService, tripService) {
        _classCallCheck(this, MapPage);

        this.router = router;
        this.geolocation = geolocation;
        this.chRef = chRef;
        this.route = route;
        this.placeService = placeService;
        this.tripService = tripService;
      }

      _createClass(MapPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {} // Load map only after view is initialized

      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          this.loadMap();
        }
      }, {
        key: "loadMap",
        value: function loadMap() {
          var _this = this;

          // set current location as map center
          this.geolocation.getCurrentPosition().then(function (resp) {
            var latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
            _this.map = new google.maps.Map(document.getElementById('map'), {
              zoom: 16,
              center: latLng,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              mapTypeControl: false
            });
            _this.marker = new google.maps.Marker({
              map: _this.map,
              position: latLng
            });

            _this.marker.setMap(_this.map); // get center's address


            _this.findPlace(latLng);

            _this.map.addListener('center_changed', function (event) {
              var center = _this.map.getCenter();

              _this.findPlace(center);
            });
          }).catch(function (error) {
            console.log('Error getting location', error);
          });
          var nativeHomeInputBox = document.getElementById('searchbar');
          var options = {
            types: ['geocode'],
            componentRestrictions: {
              country: "ph"
            }
          };
          this.googleAutocomplete = new google.maps.places.Autocomplete(nativeHomeInputBox, options);
          google.maps.event.addListener(this.googleAutocomplete, 'place_changed', function () {
            _this.lat = _this.googleAutocomplete.getPlace().geometry.location.lat();
            _this.lng = _this.googleAutocomplete.getPlace().geometry.location.lng();

            _this.map.panTo(new google.maps.LatLng(_this.lat, _this.lng));

            _this.findPlace(new google.maps.LatLng(_this.lat, _this.lng));
          });
        } // find address by LatLng

      }, {
        key: "findPlace",
        value: function findPlace(latLng) {
          var _this2 = this;

          var geocoder = new google.maps.Geocoder();
          this.marker.setMap(null);
          this.marker = new google.maps.Marker({
            map: this.map,
            position: latLng
          });
          this.marker.setMap(this.map);
          geocoder.geocode({
            'latLng': latLng
          }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              _this2.address = results[0];

              _this2.chRef.detectChanges();
            }
          });
        } // choose address and go back to home page

      }, {
        key: "selectPlace",
        value: function selectPlace() {
          var _this3 = this;

          var address = this.placeService.formatAddress(this.address);
          this.route.queryParams.subscribe(function (data) {
            var type = data.type;

            if (type == 'origin') {
              _this3.tripService.setOrigin(address.vicinity, address.location.lat, address.location.lng);
            } else if (type == 'destination') {
              _this3.tripService.setDestination(address.vicinity, address.location.lat, address.location.lng);
            }

            _this3.router.navigateByUrl('/rider/home');
          });
        }
      }]);

      return MapPage;
    }();

    MapPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
      }, {
        type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__["Geolocation"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]
      }, {
        type: _services_place_service__WEBPACK_IMPORTED_MODULE_2__["PlaceService"]
      }, {
        type: _services_trip_service__WEBPACK_IMPORTED_MODULE_3__["TripService"]
      }];
    };

    MapPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-map',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./map.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/map/map.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./map.page.scss */
      "./src/app/rider/map/map.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_4__["Geolocation"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"], _services_place_service__WEBPACK_IMPORTED_MODULE_2__["PlaceService"], _services_trip_service__WEBPACK_IMPORTED_MODULE_3__["TripService"]])], MapPage);
    /***/
  }
}]);
//# sourceMappingURL=rider-map-map-module-es5.js.map