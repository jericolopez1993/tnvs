function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["driver-home-home-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/driver/home/home.page.html":
  /*!**********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/driver/home/home.page.html ***!
    \**********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppDriverHomeHomePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n      {{ 'HOME' | translate }}\n    </ion-title>\n  </ion-toolbar>\n  <ion-item lines=\"none\">\n    <ion-avatar slot=\"start\">\n      <img src=\"{{driver.photoURL}}\" onError=\"this.src='assets/img/default-dp.png'\">\n    </ion-avatar>\n    <ion-label>{{ (driver)?.first_name }} {{ (driver)?.last_name }}\n    </ion-label>\n    <ion-toggle [(ngModel)]=\"isDriverAvailable\" [disabled]=\"!driver.canRide\"  (ionChange)=\"changeAvailability()\" slot=\"end\"></ion-toggle>\n  </ion-item>\n</ion-header>\n\n<ion-content>\n    <div id=\"map\" style=\"height: 100%;width:100%;\"></div>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/driver/home/home.module.ts":
  /*!********************************************!*\
    !*** ./src/app/driver/home/home.module.ts ***!
    \********************************************/

  /*! exports provided: HomePageModule */

  /***/
  function srcAppDriverHomeHomeModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePageModule", function () {
      return HomePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./home.page */
    "./src/app/driver/home/home.page.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");

    var HomePageModule = function HomePageModule() {
      _classCallCheck(this, HomePageModule);
    };

    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([{
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
      }])],
      declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })], HomePageModule);
    /***/
  },

  /***/
  "./src/app/driver/home/home.page.scss":
  /*!********************************************!*\
    !*** ./src/app/driver/home/home.page.scss ***!
    \********************************************/

  /*! exports provided: default */

  /***/
  function srcAppDriverHomeHomePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".welcome-card img {\n  max-height: 35vh;\n  overflow: hidden;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZHJpdmVyL2hvbWUvQzpcXFVzZXJzXFxKZXJpY28gUGF1bG9cXFJ1YnltaW5lUHJvamVjdHNcXHRudnNcXG1vYmlsZS9zcmNcXGFwcFxcZHJpdmVyXFxob21lXFxob21lLnBhZ2Uuc2NzcyIsInNyYy9hcHAvZHJpdmVyL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9kcml2ZXIvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi53ZWxjb21lLWNhcmQgaW1nIHtcbiAgbWF4LWhlaWdodDogMzV2aDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbiIsIi53ZWxjb21lLWNhcmQgaW1nIHtcbiAgbWF4LWhlaWdodDogMzV2aDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/driver/home/home.page.ts":
  /*!******************************************!*\
    !*** ./src/app/driver/home/home.page.ts ***!
    \******************************************/

  /*! exports provided: HomePage */

  /***/
  function srcAppDriverHomeHomePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePage", function () {
      return HomePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/environments/environment.prod */
    "./src/environments/environment.prod.ts");
    /* harmony import */


    var _services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../services/auth.service */
    "./src/app/driver/services/auth.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_driver_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../services/driver.service */
    "./src/app/driver/services/driver.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _services_deal_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../services/deal.service */
    "./src/app/driver/services/deal.service.ts");
    /* harmony import */


    var _services_place_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../services/place.service */
    "./src/app/driver/services/place.service.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic-native/geolocation/ngx */
    "./node_modules/@ionic-native/geolocation/ngx/index.js");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _services_common_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../services/common.service */
    "./src/app/services/common.service.ts");

    var HomePage =
    /*#__PURE__*/
    function () {
      function HomePage(driverService, alertCtrl, dealService, authService, placeService, geolocation, translate, router, storage, common, menuCtrl) {
        _classCallCheck(this, HomePage);

        this.driverService = driverService;
        this.alertCtrl = alertCtrl;
        this.dealService = dealService;
        this.authService = authService;
        this.placeService = placeService;
        this.geolocation = geolocation;
        this.translate = translate;
        this.router = router;
        this.storage = storage;
        this.common = common;
        this.menuCtrl = menuCtrl;
        this.driver = {};
        this.isDriverAvailable = false;
        this.dealStatus = false;
        this.remainingTime = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["DEAL_TIMEOUT"];
      }

      _createClass(HomePage, [{
        key: "loadMap",
        value: function loadMap(lat, lng) {
          var latLng = new google.maps.LatLng(lat, lng);
          this.map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: latLng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            zoomControl: false,
            streetViewControl: false
          });
          new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: latLng
          });
        }
      }, {
        key: "changeAvailability",
        value: function changeAvailability() {
          var _this = this;

          clearInterval(this.positionTracking);

          if (this.isDriverAvailable == true) {
            this.common.showLoader('Loading...'); // get current location

            this.geolocation.getCurrentPosition().then(function (resp) {
              var latLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
              var geocoder = new google.maps.Geocoder();

              _this.loadMap(resp.coords.latitude, resp.coords.longitude); // find address from lat lng


              geocoder.geocode({
                'latLng': latLng
              }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                  // save locality
                  var locality = _this.placeService.setLocalityFromGeocoder(results);

                  console.log('locality', locality); // start tracking

                  _this.positionTracking = setInterval(function () {
                    // check for driver object, if it did not complete profile, stop updating location
                    if (!_this.driver || !_this.driver.type) {
                      return;
                    } // Periodic update after particular time intrvel


                    _this.geolocation.getCurrentPosition().then(function (resp) {
                      _this.driverService.updatePosition(_this.driver.uid, _this.driver.type, locality, resp.coords.latitude, resp.coords.longitude, _this.driver.rating, _this.driver.first_name + " " + _this.driver.last_name);
                    }, function (err) {
                      console.log(err);
                    });
                  }, src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["POSITION_INTERVAL"]);

                  _this.watchDeals();
                }
              });
            }, function (err) {
              console.log(err);
            });
            this.common.hideLoader();
          } else {
            clearInterval(this.positionTracking);

            if (this.dealSubscription) {
              // unsubscribe when leave this page
              this.dealSubscription.unsubscribe();
            }
          }
        }
      }, {
        key: "ionViewWillLeave",
        value: function ionViewWillLeave() {
          if (this.dealSubscription) {
            // unsubscribe when leave this page
            this.dealSubscription.unsubscribe();
          }
        } // count down

      }, {
        key: "countDown",
        value: function countDown() {
          var _this2 = this;

          var interval = setInterval(function () {
            _this2.remainingTime--;

            if (_this2.remainingTime == 0) {
              clearInterval(interval);

              _this2.cancelDeal();

              _this2.remainingTime = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["DEAL_TIMEOUT"];
            }
          }, 1000);
          this.confirmJob();
        }
      }, {
        key: "cancelDeal",
        value: function cancelDeal() {
          console.log("close");
          this.dealStatus = false;
          this.dealService.removeDeal(this.driver.uid);
        }
      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          var _this3 = this;

          this.menuCtrl.enable(true);

          if (this.authService.getUserData() != null) {
            this.driverService.getDriver().valueChanges().subscribe(function (snapshot) {
              if (snapshot != null) {
                _this3.driver = snapshot;
              }
            });
            this.storage.get('iondriver_settings').then(function (res) {
              if (res != null && res != undefined) {
                var data = JSON.parse(res);
                _this3.isDriverAvailable = data.alwaysOn;
              }
            }).catch(function (err) {
              return console.log(err);
            });
          } else {
            this.router.navigateByUrl('//driver/login');
          }
        }
      }, {
        key: "range",
        value: function range(n) {
          return new Array(Math.round(n));
        } // confirm a job

      }, {
        key: "confirmJob",
        value: function confirmJob() {
          var _this4 = this;

          var message = "<b>From:</b> (" + this.job.origin.distance + "km)<br/>" + this.job.origin.vicinity + "<br/><br/> <b>To:</b>(" + this.job.destination.distance + "km)<br>" + this.job.destination.vicinity + "<br>" + this.job.locationDetails + "<br/><br/> <b>Delivery Info:</b><br>" + this.job.recipientName + "<br>" + this.job.mobileNumber + "<br>" + this.job.itemCategory;
          this.alertCtrl.create({
            header: 'New Request',
            message: message,
            buttons: [{
              text: 'Reject',
              handler: function handler() {
                console.log('Disagree clicked');
                _this4.dealStatus = false;

                _this4.dealService.removeDeal(_this4.driver.uid);
              }
            }, {
              text: 'Accept',
              handler: function handler() {
                _this4.dealStatus = false; // this.dealService.getDeal(this.driver.uid).valueChanges().subscribe((snapshot) => {
                //     console.log(snapshot);
                // });

                _this4.dealService.acceptDeal(_this4.driver.uid, _this4.deal).then(function () {
                  _this4.router.navigateByUrl('/driver/pickup');
                });
              }
            }]
          }).then(function (r) {
            return r.present();
          });
          this.playAudio();
        } // listen to deals

      }, {
        key: "watchDeals",
        value: function watchDeals() {
          var _this5 = this;

          // listen to deals
          this.dealSubscription = this.dealService.getDeal(this.driver.uid).valueChanges().subscribe(function (snapshot) {
            if (snapshot != null || snapshot != undefined) {
              _this5.deal = snapshot;

              if (snapshot.status == src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["DEAL_STATUS_PENDING"]) {
                // if deal expired
                if (snapshot.createdAt < Date.now() - src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["DEAL_TIMEOUT"] * 1000) {
                  return _this5.dealService.removeDeal(_this5.driver.uid);
                }

                _this5.dealStatus = true;
                _this5.job = snapshot;

                _this5.geolocation.getCurrentPosition().then(function (resp) {
                  //resp.coords.longitude
                  _this5.job.origin.distance = _this5.placeService.calcCrow(resp.coords.latitude, resp.coords.longitude, _this5.job.origin.location.lat, _this5.job.origin.location.lng).toFixed(0);
                  _this5.job.destination.distance = _this5.placeService.calcCrow(resp.coords.latitude, resp.coords.longitude, _this5.job.destination.location.lat, _this5.job.destination.location.lng).toFixed(0);

                  _this5.countDown();
                }, function (err) {
                  console.log(err);
                });
              }
            } else {
              _this5.alertCtrl.dismiss();
            }
          });
        }
      }, {
        key: "playAudio",
        value: function playAudio() {
          if (src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["PLAY_AUDIO_ON_REQUEST"] == true) {
            var audio = new Audio(src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["AUDIO_PATH"]);
            audio.play();
          }
        }
      }]);

      return HomePage;
    }();

    HomePage.ctorParameters = function () {
      return [{
        type: _services_driver_service__WEBPACK_IMPORTED_MODULE_5__["DriverService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"]
      }, {
        type: _services_deal_service__WEBPACK_IMPORTED_MODULE_7__["DealService"]
      }, {
        type: _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
      }, {
        type: _services_place_service__WEBPACK_IMPORTED_MODULE_8__["PlaceService"]
      }, {
        type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__["Geolocation"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__["TranslateService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_11__["Storage"]
      }, {
        type: _services_common_service__WEBPACK_IMPORTED_MODULE_12__["CommonService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["MenuController"]
      }];
    };

    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-home',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./home.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/driver/home/home.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./home.page.scss */
      "./src/app/driver/home/home.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_driver_service__WEBPACK_IMPORTED_MODULE_5__["DriverService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"], _services_deal_service__WEBPACK_IMPORTED_MODULE_7__["DealService"], _services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"], _services_place_service__WEBPACK_IMPORTED_MODULE_8__["PlaceService"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__["Geolocation"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__["TranslateService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_11__["Storage"], _services_common_service__WEBPACK_IMPORTED_MODULE_12__["CommonService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["MenuController"]])], HomePage);
    /***/
  },

  /***/
  "./src/app/services/common.service.ts":
  /*!********************************************!*\
    !*** ./src/app/services/common.service.ts ***!
    \********************************************/

  /*! exports provided: CommonService */

  /***/
  function srcAppServicesCommonServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CommonService", function () {
      return CommonService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var CommonService =
    /*#__PURE__*/
    function () {
      function CommonService(toastCtrl, loadCtrl, alertCtrl) {
        _classCallCheck(this, CommonService);

        this.toastCtrl = toastCtrl;
        this.loadCtrl = loadCtrl;
        this.alertCtrl = alertCtrl;
      }

      _createClass(CommonService, [{
        key: "showToast",
        value: function showToast(message) {
          this.toastCtrl.create({
            message: message,
            duration: 3000
          }).then(function (res) {
            return res.present();
          });
        }
      }, {
        key: "showAlert",
        value: function showAlert(message) {
          this.alertCtrl.create({
            message: message,
            buttons: ['ok']
          }).then(function (res) {
            return res.present();
          });
        }
      }, {
        key: "showLoader",
        value: function showLoader(message) {
          var _this6 = this;

          this.loadCtrl.create({
            message: message
          }).then(function (res) {
            _this6.loader = res.present();
            setTimeout(function () {
              return _this6.loadCtrl.dismiss();
            }, 10000);
          });
        }
      }, {
        key: "hideLoader",
        value: function hideLoader() {
          this.loadCtrl.dismiss();
        }
      }]);

      return CommonService;
    }();

    CommonService.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
      }];
    };

    CommonService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])], CommonService);
    /***/
  }
}]);
//# sourceMappingURL=driver-home-home-module-es5.js.map