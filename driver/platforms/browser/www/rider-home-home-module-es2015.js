(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["rider-home-home-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/home/home.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/rider/home/home.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>Home</ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <div class=\"locationinput-holder\">\n    <input class=\"locationinput\" (click)=\"chooseOrigin()\" placeholder=\"Where do you want to pickup?\" type=\"text\"\n      value=\"{{origin ? origin.vicinity : '' }}\">\n    <input class=\"locationinput\" (click)=\"chooseDestination()\" placeholder=\"Where do you want to drop?\" type=\"text\"\n      value=\"{{ destination ? destination.vicinity : '' }}\">\n  </div>\n\n  <div id=\"{{ mapId }}\" [ngStyle]=\"{height: '100%'}\"></div>\n\n  <div class=\"align-bottom\">\n\n    <p class=\"distanceText\">\n      <span *ngIf=\"distanceText!=''\">Distance: {{ distanceText }}</span>\n      <span *ngIf=\"durationText!=''\">&nbsp; Duration: {{durationText}}</span>\n    </p>\n    <!--<ion-row [hidden]=\"!destination\">-->\n      <!--<ion-col (click)=\"choosePaymentMethod1()\">-->\n        <!--<ion-icon name=\"card\" color=\"gray\"></ion-icon>-->\n        <!--<span ion-text color=\"gray\">{{ getPaymentMethod() }}</span>-->\n      <!--</ion-col>-->\n      <!--<ion-col (click)=\"showPromoPopup()\" *ngIf=\"destination\">-->\n        <!--<ion-icon name=\"create\" color=\"gray\"></ion-icon>-->\n        <!--<span ion-text color=\"gray\">{{'PROMO' | translate}}</span>-->\n      <!--</ion-col>-->\n      <!--<ion-col (click)=\"showNotePopup()\">-->\n        <!--<ion-icon name=\"create\" color=\"gray\"></ion-icon>-->\n        <!--<span ion-text color=\"gray\">{{'NOTE' | translate}}</span>-->\n      <!--</ion-col>-->\n    <!--</ion-row>-->\n    <ion-row [hidden]=\"!destination\">\n      <ion-list lines=\"full\" class=\"ion-no-margin ion-no-padding\">\n        <ion-item>\n          <ion-input type=\"text\" [(ngModel)]=\"locationDetails\" placeholder=\"Location Details\" (click)=\"editLocationDetails()\" class=\"form-information-inputs\" readonly></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-input type=\"text\" [(ngModel)]=\"recipientName\"  placeholder=\"Recipient Name\" (click)=\"editRecipientName()\" class=\"form-information-inputs\" readonly></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-input type=\"text\" [(ngModel)]=\"mobileNumber\"  placeholder=\"Mobile Number\" (click)=\"editMobileNumber()\" class=\"form-information-inputs\" readonly></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-input type=\"text\" [(ngModel)]=\"itemCategory\"  placeholder=\"Type of Item\" (click)=\"editItemCategory()\" class=\"form-information-inputs\" readonly></ion-input>\n        </ion-item>\n      </ion-list>\n    </ion-row>\n\n    <ion-row [hidden]=\"!destination\">\n\n      <ion-col *ngFor=\"let vehicle of vehicles; let i = index\" [ngClass]=\"{'active': vehicle.active}\"\n        (click)=\"chooseVehicle(i)\">\n        <img src=\"{{ vehicle.icon }}\">\n        <p>{{ vehicle.name }}</p>\n        <p>{{currency }}{{ vehicle.fee }}</p>\n      </ion-col>\n\n    </ion-row>\n\n\n    <ion-button expand=\"block\" color=\"dark\" [hidden]=\"destination\" (click)=\"chooseDestination()\">\n      BOOK NOW</ion-button>\n    <ion-button expand=\"block\" color=\"dark\" [hidden]=\"!destination\" (click)=\"book()\">\n      {{ locateDriver == false ? 'BOOK NOW':'Locating Drivers'}} <ion-spinner name=\"dots\" color=\"light\"\n        [hidden]=\"!locateDriver\"></ion-spinner>\n    </ion-button>\n    <ion-button expand=\"block\" color=\"danger\" [hidden]=\"!locateDriver\" (click)=\"cancelBook()\">\n      Cancel Book\n    </ion-button>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/rider/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/rider/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/rider/home/home.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");








let HomePageModule = class HomePageModule {
};
HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                {
                    path: '',
                    component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                }
            ])
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/rider/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/rider/home/home.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".locationinput-holder {\n  padding: 0.2rem 1rem;\n  position: absolute;\n  background: transparent;\n  z-index: 999999;\n  top: 0;\n  width: 100%;\n}\n\n.locationinput {\n  background: #fff;\n  border: 1px solid #ccc;\n  outline: 0;\n  width: 100%;\n  padding: 0.5rem;\n  color: #333;\n  margin: 2px 0px;\n  font-size: 14px;\n  border-radius: 4px;\n}\n\n.distanceText {\n  font-size: 14px;\n  color: #555;\n  margin: 0;\n}\n\n.align-bottom {\n  position: fixed;\n  bottom: 0;\n  width: 100%;\n  padding: 5px;\n}\n\n.align-bottom p {\n  font-size: 14p;\n}\n\n.header-md:after {\n  background-image: none;\n}\n\nion-col {\n  text-align: center;\n}\n\nion-col img {\n  width: 40px;\n  padding: 5px;\n  border-radius: 100%;\n}\n\nion-col p {\n  margin: 0;\n}\n\nion-row {\n  background: #fff;\n  border-radius: 3px;\n  border: 1px solid #eee;\n}\n\n.list-md {\n  margin: -1px 0 0px;\n}\n\n.label-md {\n  margin: 13px -50px 13px 0;\n}\n\n.text-input-md {\n  font-size: 14px;\n  margin: 8px 5px;\n}\n\n#map {\n  width: 100%;\n}\n\n.active img {\n  background: #ffce00;\n}\n\nion-list {\n  width: 100%;\n}\n\nion-input {\n  width: 100%;\n}\n\n.form-information-inputs {\n  background: transparent !important;\n  border: none !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmlkZXIvaG9tZS9DOlxcVXNlcnNcXEplcmljbyBQYXVsb1xcUnVieW1pbmVQcm9qZWN0c1xcdG52c1xcbW9iaWxlL3NyY1xcYXBwXFxyaWRlclxcaG9tZVxcaG9tZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3JpZGVyL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0VBQ0EsTUFBQTtFQUNBLFdBQUE7QUNDRjs7QURDQTtFQUNFLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0VGOztBRENBO0VBQ0UsZUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0FDRUY7O0FEQUE7RUFDRSxlQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDR0Y7O0FEQUE7RUFDRSxjQUFBO0FDR0Y7O0FEREE7RUFDRSxzQkFBQTtBQ0lGOztBREZBO0VBQ0Usa0JBQUE7QUNLRjs7QURIQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUNNRjs7QURKQTtFQUFZLFNBQUE7QUNRWjs7QURQQTtFQUNFLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtBQ1VGOztBRFJBO0VBQ0Usa0JBQUE7QUNXRjs7QURUQTtFQUNFLHlCQUFBO0FDWUY7O0FEVkE7RUFDRSxlQUFBO0VBQ0EsZUFBQTtBQ2FGOztBRFhBO0VBQ0UsV0FBQTtBQ2NGOztBRFhBO0VBQ0UsbUJBQUE7QUNjRjs7QURaQTtFQUNFLFdBQUE7QUNlRjs7QURaQTtFQUNFLFdBQUE7QUNlRjs7QURaQTtFQUNFLGtDQUFBO0VBQ0EsdUJBQUE7QUNlRiIsImZpbGUiOiJzcmMvYXBwL3JpZGVyL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9jYXRpb25pbnB1dC1ob2xkZXJ7XG4gIHBhZGRpbmc6IDAuMnJlbSAxcmVtO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICB6LWluZGV4OiA5OTk5OTk7XG4gIHRvcDowO1xuICB3aWR0aDoxMDAlO1xufVxuLmxvY2F0aW9uaW5wdXR7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGJvcmRlcjoxcHggc29saWQgI2NjYztcbiAgb3V0bGluZTowO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMC41cmVtO1xuICBjb2xvcjogIzMzMztcbiAgbWFyZ2luOiAycHggMHB4O1xuICBmb250LXNpemU6IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbn1cblxuLmRpc3RhbmNlVGV4dHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogIzU1NTtcbiAgbWFyZ2luOiAwO1xufVxuLmFsaWduLWJvdHRvbSB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgYm90dG9tOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogNXB4O1xufVxuXG4uYWxpZ24tYm90dG9tIHB7XG4gIGZvbnQtc2l6ZTogMTRwXG59XG4uaGVhZGVyLW1kOmFmdGVye1xuICBiYWNrZ3JvdW5kLWltYWdlOiBub25lO1xufVxuaW9uLWNvbHtcbiAgdGV4dC1hbGlnbjogY2VudGVyXG59XG5pb24tY29sIGltZ3tcbiAgd2lkdGg6IDQwcHg7XG4gIHBhZGRpbmc6IDVweDtcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcbn1cbmlvbi1jb2wgcCB7IG1hcmdpbjogMH1cbmlvbi1yb3d7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDNweDtcbiAgYm9yZGVyOiAxcHggc29saWQgI2VlZTtcbn1cbi5saXN0LW1kIHtcbiAgbWFyZ2luOiAtMXB4IDAgMHB4O1xufVxuLmxhYmVsLW1ke1xuICBtYXJnaW46IDEzcHggLTUwcHggMTNweCAwO1xufVxuLnRleHQtaW5wdXQtbWR7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luOiA4cHggNXB4O1xufVxuI21hcCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uYWN0aXZlIGltZ3tcbiAgYmFja2dyb3VuZDogI2ZmY2UwMDtcbn1cbmlvbi1saXN0IHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbmlvbi1pbnB1dCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uZm9ybS1pbmZvcm1hdGlvbi1pbnB1dHMge1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xuICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcbn0iLCIubG9jYXRpb25pbnB1dC1ob2xkZXIge1xuICBwYWRkaW5nOiAwLjJyZW0gMXJlbTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgei1pbmRleDogOTk5OTk5O1xuICB0b3A6IDA7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ubG9jYXRpb25pbnB1dCB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNjY2M7XG4gIG91dGxpbmU6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAwLjVyZW07XG4gIGNvbG9yOiAjMzMzO1xuICBtYXJnaW46IDJweCAwcHg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xufVxuXG4uZGlzdGFuY2VUZXh0IHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogIzU1NTtcbiAgbWFyZ2luOiAwO1xufVxuXG4uYWxpZ24tYm90dG9tIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICBib3R0b206IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiA1cHg7XG59XG5cbi5hbGlnbi1ib3R0b20gcCB7XG4gIGZvbnQtc2l6ZTogMTRwO1xufVxuXG4uaGVhZGVyLW1kOmFmdGVyIHtcbiAgYmFja2dyb3VuZC1pbWFnZTogbm9uZTtcbn1cblxuaW9uLWNvbCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuaW9uLWNvbCBpbWcge1xuICB3aWR0aDogNDBweDtcbiAgcGFkZGluZzogNXB4O1xuICBib3JkZXItcmFkaXVzOiAxMDAlO1xufVxuXG5pb24tY29sIHAge1xuICBtYXJnaW46IDA7XG59XG5cbmlvbi1yb3cge1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBib3JkZXItcmFkaXVzOiAzcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNlZWU7XG59XG5cbi5saXN0LW1kIHtcbiAgbWFyZ2luOiAtMXB4IDAgMHB4O1xufVxuXG4ubGFiZWwtbWQge1xuICBtYXJnaW46IDEzcHggLTUwcHggMTNweCAwO1xufVxuXG4udGV4dC1pbnB1dC1tZCB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbWFyZ2luOiA4cHggNXB4O1xufVxuXG4jbWFwIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5hY3RpdmUgaW1nIHtcbiAgYmFja2dyb3VuZDogI2ZmY2UwMDtcbn1cblxuaW9uLWxpc3Qge1xuICB3aWR0aDogMTAwJTtcbn1cblxuaW9uLWlucHV0IHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5mb3JtLWluZm9ybWF0aW9uLWlucHV0cyB7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG4gIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/rider/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/rider/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_place_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/place.service */ "./src/app/services/place.service.ts");
/* harmony import */ var _services_setting_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/setting.service */ "./src/app/services/setting.service.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _services_deal_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/deal.service */ "./src/app/services/deal.service.ts");
/* harmony import */ var _services_driver_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/driver.service */ "./src/app/services/driver.service.ts");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/auth/es2015/index.js");
/* harmony import */ var _services_trip_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/trip.service */ "./src/app/services/trip.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! src/environments/environment.prod */ "./src/environments/environment.prod.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_16__);

















let HomePage = class HomePage {
    constructor(router, alertCtrl, placeService, geolocation, chRef, settingService, tripService, driverService, afAuth, authService, translate, dealService, common, menuCtrl) {
        this.router = router;
        this.alertCtrl = alertCtrl;
        this.placeService = placeService;
        this.geolocation = geolocation;
        this.chRef = chRef;
        this.settingService = settingService;
        this.tripService = tripService;
        this.driverService = driverService;
        this.afAuth = afAuth;
        this.authService = authService;
        this.translate = translate;
        this.dealService = dealService;
        this.common = common;
        this.menuCtrl = menuCtrl;
        this.mapId = Math.random() + 'map';
        this.mapHeight = 480;
        this.showModalBg = false;
        this.showVehicles = false;
        this.vehicles = [];
        this.note = '';
        this.promocode = '';
        this.distance = 0;
        this.duration = 0;
        this.paymentMethod = 'cash';
        this.activeDrivers = [];
        this.driverMarkers = [];
        this.locateDriver = false;
        this.user = {};
        this.isTrackDriverEnabled = true;
        this.discount = 0;
        this.distanceText = '';
        this.durationText = '';
    }
    ionViewDidEnter() {
        // this.common.showLoader('Loading...');
        this.menuCtrl.enable(true);
        this.afAuth.authState.subscribe(authData => {
            if (authData) {
                this.user = this.authService.getUserData();
            }
        });
        this.origin = this.tripService.getOrigin();
        this.destination = this.tripService.getDestination();
        this.loadMap();
        // this.common.hideLoader();
    }
    ngOnInit() {
        console.log("calling");
    }
    ionViewWillLeave() {
        clearInterval(this.driverTracking);
    }
    // get current payment method from service
    getPaymentMethod() {
        this.paymentMethod = this.tripService.getPaymentMethod();
        return this.paymentMethod;
    }
    choosePaymentMethod1() {
        this.alertCtrl.create({
            header: "Choose Payments",
            inputs: [
                { type: 'radio', label: "Cash", value: 'cash' },
                { type: 'radio', label: "Card", value: 'card' }
            ],
            buttons: [{
                    text: "Cancel"
                }, {
                    text: "Choose",
                    handler: (data) => {
                        if (data == 'card') {
                            this.authService.getCardSetting().valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["take"])(1)).subscribe((res) => {
                                if (res != null) {
                                    this.tripService.setPaymentMethod(data);
                                    this.paymentMethod = data;
                                    const exp = res.exp.split('/');
                                    Stripe.card.createToken({
                                        number: res.number,
                                        exp_month: exp[0],
                                        exp_year: exp[1],
                                        cvc: res.cvv
                                    }, (status, response) => {
                                        if (status == 200) {
                                            console.log("Card Ready");
                                            this.authService.updateCardSetting(res.number, res.exp, res.cvv, response.id);
                                        }
                                        else {
                                            this.common.showToast(response.error.message);
                                        }
                                    });
                                }
                                else
                                    this.common.showAlert("Invalid Card");
                            });
                        }
                        else if (data == 'cash') {
                            this.paymentMethod = data;
                            this.tripService.setPaymentMethod(data);
                        }
                    }
                }]
        }).then(res => res.present());
    }
    // toggle active vehicle
    chooseVehicle(index) {
        for (var i = 0; i < this.vehicles.length; i++) {
            this.vehicles[i].active = (i == index);
            // choose this vehicle type
            if (i == index) {
                this.tripService.setVehicle(this.vehicles[i]);
                this.currentVehicle = this.vehicles[i];
            }
        }
        // start tracking new driver type
        this.trackDrivers();
        this.toggleVehicles();
    }
    loadMap() {
        // this.common.showLoader("Loading..");
        // get current location
        return this.geolocation.getCurrentPosition().then((resp) => {
            if (this.origin)
                this.startLatLng = new google.maps.LatLng(this.origin.location.lat, this.origin.location.lng);
            else
                this.startLatLng = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
            let directionsDisplay;
            let directionsService = new google.maps.DirectionsService();
            directionsDisplay = new google.maps.DirectionsRenderer();
            this.map = new google.maps.Map(document.getElementById(this.mapId), {
                zoom: 15,
                center: this.startLatLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                zoomControl: false,
                streetViewControl: false,
                fullscreenControl: false
            });
            let mapx = this.map;
            directionsDisplay.setMap(mapx);
            // find map center address
            let geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': this.map.getCenter() }, (results, status) => {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (!this.origin) {
                        // set map center as origin
                        this.origin = this.placeService.formatAddress(results[0]);
                        this.tripService.setOrigin(this.origin.vicinity, this.origin.location.lat, this.origin.location.lng);
                        this.setOrigin();
                        this.chRef.detectChanges();
                    }
                    else {
                        this.setOrigin();
                    }
                    // save locality
                    let locality = this.placeService.setLocalityFromGeocoder(results);
                    console.log('locality', locality);
                    // load list vehicles
                    this.settingService.getPrices().valueChanges().subscribe((snapshot) => {
                        this.vehicles = [];
                        let obj = snapshot[locality] ? snapshot[locality] : snapshot.default;
                        this.currency = obj.currency;
                        this.tripService.setCurrency(this.currency);
                        // calculate price
                        Object.keys(obj.vehicles).forEach(id => {
                            obj.vehicles[id].id = id;
                            this.vehicles.push(obj.vehicles[id]);
                        });
                        // calculate distance between origin adn destination
                        if (this.destination) {
                            directionsService.route(request, (result, status) => {
                                if (status == google.maps.DirectionsStatus.OK && result.routes.length != 0) {
                                    this.distance = result.routes[0].legs[0].distance.value;
                                    this.distanceText = result.routes[0].legs[0].distance.text;
                                    this.durationText = result.routes[0].legs[0].duration.text;
                                    for (let i = 0; i < this.vehicles.length; i++) {
                                        var currentDay = new Date();
                                        // var nextDay = new Date(Date.now()+(1*24*60*60*1000));
                                        var startTime1 = new Date(currentDay.getFullYear(), currentDay.getMonth(), currentDay.getDate(), 21).getTime();
                                        var endTime1 = new Date(currentDay.getFullYear(), currentDay.getMonth(), currentDay.getDate(), 23, 59, 60).getTime();
                                        var startTime2 = new Date(currentDay.getFullYear(), currentDay.getMonth(), currentDay.getDate(), 0).getTime();
                                        var endTime2 = new Date(currentDay.getFullYear(), currentDay.getMonth(), currentDay.getDate(), 4, 59, 60).getTime();
                                        this.vehicles[i].fee = (this.distance * this.vehicles[i].price / 1000) + this.vehicles[i].first_fee;
                                        if (this.distance >= 15000 && this.distance <= 24999) {
                                            this.vehicles[i].fee = this.vehicles[i].fee + this.vehicles[i].second_fee;
                                        }
                                        else if (this.distance >= 25000 && this.distance <= 49999) {
                                            this.vehicles[i].fee = this.vehicles[i].fee + this.vehicles[i].third_fee;
                                        }
                                        else if (this.distance >= 50000) {
                                            this.vehicles[i].fee = this.vehicles[i].fee + this.vehicles[i].fourth_fee;
                                        }
                                        if ((currentDay.getTime() >= startTime1 && currentDay.getTime() <= endTime1) || (currentDay.getTime() >= startTime2 && currentDay.getTime() <= endTime2)) {
                                            this.vehicles[i].fee = this.vehicles[i].fee + this.vehicles[i].night_fee;
                                        }
                                        this.vehicles[i].fee = this.vehicles[i].fee.toFixed(2);
                                    }
                                }
                                else {
                                    console.log("error");
                                }
                            });
                        }
                        // set first device as default
                        this.vehicles[0].active = true;
                        this.currentVehicle = this.vehicles[0];
                        this.locality = locality;
                        if (this.isTrackDriverEnabled)
                            this.trackDrivers();
                    });
                }
            });
            // add destination to map
            if (this.destination) {
                this.destLatLng = new google.maps.LatLng(this.destination.location.lat, this.destination.location.lng);
                var bounds = new google.maps.LatLngBounds();
                bounds.extend(this.startLatLng);
                bounds.extend(this.destLatLng);
                mapx.fitBounds(bounds);
                var request = {
                    origin: this.startLatLng,
                    destination: this.destLatLng,
                    travelMode: google.maps.TravelMode.DRIVING
                };
                directionsService.route(request, function (response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setDirections(response);
                        directionsDisplay.setMap(mapx);
                    }
                    else {
                        console.log("error");
                    }
                });
            }
            // this.common.hideLoader();
        }).catch((error) => {
            // this.common.hideLoader();
            console.log('Error getting location', error);
        });
    }
    showPromoPopup() {
        this.alertCtrl.create({
            header: 'Enter Promo code',
            message: "",
            inputs: [
                {
                    name: 'promocode',
                    placeholder: 'Enter Promo Code'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: data => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Apply',
                    handler: data => {
                        //verifying promocode
                        firebase__WEBPACK_IMPORTED_MODULE_16__["database"]().ref('promocodes').orderByChild("code").equalTo(data.promocode).once('value', promocodes => {
                            let tmp = [];
                            promocodes.forEach(promo => {
                                tmp.push(Object.assign({ key: promo.key }, promo.val()));
                                return false;
                            });
                            tmp = tmp[0];
                            if (promocodes.val() != null || promocodes.val() != undefined) {
                                this.promocode = tmp.code;
                                this.discount = tmp.discount;
                                this.tripService.setPromo(tmp.code);
                                this.tripService.setDiscount(tmp.discount);
                            }
                        }, err => console.log(err));
                    }
                }
            ]
        }).then(prompt => prompt.present());
    }
    // Show note popup when click to 'Notes to user'
    showNotePopup() {
        this.alertCtrl.create({
            header: 'Notes to user',
            message: "",
            inputs: [
                { name: 'note', placeholder: 'Note' },
            ],
            buttons: [
                { text: 'Cancel' },
                {
                    text: 'Save',
                    handler: data => {
                        this.note = data;
                        this.tripService.setNote(data);
                        console.log('Saved clicked');
                    }
                }
            ]
        }).then(prompt => prompt.present());
    }
    ;
    // go to next view when the 'Book' button is clicked
    book() {
        this.locateDriver = true;
        // store detail
        this.tripService.setAvailableDrivers(this.activeDrivers);
        this.tripService.setDistance(this.distance);
        this.tripService.setFee(this.currentVehicle.fee);
        this.tripService.setIcon(this.currentVehicle.icon);
        this.tripService.setNote(this.note);
        this.tripService.setPromo(this.promocode);
        this.tripService.setDiscount(this.discount);
        this.tripService.setLocationDetails(this.locationDetails);
        this.tripService.setRecipientName(this.recipientName);
        this.tripService.setMobileNumber(this.mobileNumber);
        this.tripService.setItemCategory(this.itemCategory);
        // this.tripService.setPaymentMethod('');
        this.drivers = this.tripService.getAvailableDrivers();
        // sort by driver distance and rating
        this.drivers = this.dealService.sortDriversList(this.drivers);
        if (this.drivers && this.locationDetails && this.recipientName && this.mobileNumber && this.itemCategory) {
            this.retryDeal(0);
        }
        else {
            this.locateDriver = false;
            this.common.showAlert("Please input the textboxes. For Driver to understand more about the delivery.");
        }
    }
    cancelBook() {
        let driver = this.drivers[0];
        if (driver) {
            this.dealService.removeDeal(driver.key);
        }
        this.locateDriver = false;
    }
    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    retryDeal(index) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let i = 0;
            do {
                this.makeDeal(index);
                i = i + 1;
                yield this.sleep(5000);
                if (this.drivers[index] && this.dealAccepted) {
                    break;
                }
                if (!this.locateDriver) {
                    break;
                }
                if (i == 5) {
                    this.locateDriver = false;
                    this.common.showAlert("No Driver Found");
                }
            } while (i < 5);
        });
    }
    makeDeal(index) {
        let driver = this.drivers[index];
        this.dealAccepted = false;
        if (driver) {
            driver.status = 'Bidding';
            this.dealService.getDriverDeal(driver.key).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["take"])(1)).subscribe((snapshot) => {
                // if user is available
                if (snapshot == null) {
                    // create a record
                    this.dealService.makeDeal(driver.key, this.tripService.getOrigin(), this.tripService.getDestination(), this.tripService.getDistance(), this.tripService.getFee(), this.tripService.getCurrency(), this.tripService.getNote(), this.tripService.getPaymentMethod(), this.tripService.getPromo(), this.tripService.getDiscount(), this.tripService.getLocationDetails(), this.tripService.getRecipientName(), this.tripService.getMobileNumber(), this.tripService.getItemCategory()).then(() => {
                        let sub = this.dealService.getDriverDeal(driver.key).valueChanges().subscribe((snap) => {
                            // if record doesn't exist or is accepted
                            if (snap === null || snap.status != src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__["DEAL_STATUS_PENDING"]) {
                                sub.unsubscribe();
                                // if deal has been cancelled
                                if (snap === null) {
                                    this.nextDriver(index);
                                }
                                else if (snap.status == src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__["DEAL_STATUS_ACCEPTED"]) {
                                    // if deal is accepted
                                    this.dealAccepted = true;
                                    this.drivers = [];
                                    this.tripService.setId(snap.tripId);
                                    // go to user page
                                    this.locateDriver = false;
                                    this.router.navigateByUrl('/rider/tracking');
                                }
                            }
                        });
                    });
                }
                else {
                    this.nextDriver(index);
                }
            });
        }
    }
    // make deal to next driver
    nextDriver(index) {
        this.drivers.splice(index, 1);
        this.makeDeal(index);
    }
    // choose origin place
    chooseOrigin() {
        this.router.navigate(['rider/map'], {
            queryParams: {
                type: 'origin'
            }
        });
    }
    // choose destination place
    chooseDestination() {
        this.router.navigate(['rider/map'], {
            queryParams: {
                type: 'destination'
            }
        });
    }
    choosePaymentMethod() {
        this.router.navigateByUrl('/rider/payments');
    }
    // add origin marker to map
    setOrigin() {
        // add origin and destination marker
        let latLng = new google.maps.LatLng(this.origin.location.lat, this.origin.location.lng);
        let startMarker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: latLng
        });
        startMarker.setMap(this.map);
        if (this.destination)
            startMarker.setMap(null);
        // set map center to origin address
        this.map.setCenter(latLng);
    }
    // show or hide vehicles
    toggleVehicles() {
        this.showVehicles = !this.showVehicles;
        this.showModalBg = (this.showVehicles == true);
    }
    // track drivers
    trackDrivers() {
        this.showDriverOnMap(this.locality);
        clearInterval(this.driverTracking);
        this.driverTracking = setInterval(() => {
            this.showDriverOnMap(this.locality);
        }, src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__["POSITION_INTERVAL"]);
    }
    // show drivers on map
    showDriverOnMap(locality) {
        // get active drivers
        this.driverService.getActiveDriver(locality, this.currentVehicle.id).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_14__["take"])(1)).subscribe((snapshot) => {
            // clear vehicles
            this.clearDrivers();
            // only show near vehicle
            snapshot.forEach(vehicle => {
                // only show vehicle which has last active < 30 secs & distance < 5km
                let distance = this.placeService.calcCrow(vehicle.lat, vehicle.lng, this.origin.location.lat, this.origin.location.lng);
                // checking last active time and distance
                if (distance < src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__["SHOW_VEHICLES_WITHIN"] && Date.now() - vehicle.last_active < src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_13__["VEHICLE_LAST_ACTIVE_LIMIT"]) {
                    // create or update
                    let latLng = new google.maps.LatLng(vehicle.lat, vehicle.lng);
                    let marker = new google.maps.Marker({
                        map: this.map,
                        position: latLng,
                        icon: {
                            url: this.currentVehicle.map_icon,
                            size: new google.maps.Size(32, 32),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(16, 16),
                            scaledSize: new google.maps.Size(32, 32)
                        },
                    });
                    // add vehicle and marker to the list
                    vehicle.distance = distance;
                    this.driverMarkers.push(marker);
                    this.activeDrivers.push(vehicle);
                }
                else {
                    console.log('This vehicle is too far');
                }
            });
        });
    }
    // clear expired drivers on the map
    clearDrivers() {
        this.activeDrivers = [];
        this.driverMarkers.forEach((vehicle) => {
            vehicle.setMap(null);
        });
    }
    editLocationDetails() {
        this.alertCtrl.create({
            header: 'Location Details',
            inputs: [
                {
                    name: 'locationDetails',
                    type: 'text',
                    placeholder: 'Gate Color, Number, Etc.'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'OK',
                    handler: (data) => {
                        this.locationDetails = data.locationDetails;
                    }
                }
            ]
        }).then(r => r.present());
    }
    editRecipientName() {
        this.alertCtrl.create({
            header: 'Recipient Name',
            inputs: [
                {
                    name: 'recipientName',
                    type: 'text',
                    placeholder: ''
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'OK',
                    handler: (data) => {
                        this.recipientName = data.recipientName;
                    }
                }
            ]
        }).then(r => r.present());
    }
    editMobileNumber() {
        this.alertCtrl.create({
            header: 'Mobile Number',
            inputs: [
                {
                    name: 'mobileNumber',
                    type: 'text',
                    placeholder: 'Ex. 09xxxxxxxxx'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'OK',
                    handler: (data) => {
                        this.mobileNumber = data.mobileNumber;
                    }
                }
            ]
        }).then(r => r.present());
    }
    editItemCategory() {
        this.alertCtrl.create({
            header: 'Type of Item',
            inputs: [
                {
                    name: 'itemCategory',
                    type: 'text',
                    placeholder: 'Ex. Clothes, Toys, Etc.'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                    }
                },
                {
                    text: 'OK',
                    handler: (data) => {
                        this.itemCategory = data.itemCategory;
                    }
                }
            ]
        }).then(r => r.present());
    }
};
HomePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_12__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["AlertController"] },
    { type: _services_place_service__WEBPACK_IMPORTED_MODULE_2__["PlaceService"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_11__["Geolocation"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"] },
    { type: _services_setting_service__WEBPACK_IMPORTED_MODULE_3__["SettingService"] },
    { type: _services_trip_service__WEBPACK_IMPORTED_MODULE_9__["TripService"] },
    { type: _services_driver_service__WEBPACK_IMPORTED_MODULE_7__["DriverService"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_8__["AngularFireAuth"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"] },
    { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"] },
    { type: _services_deal_service__WEBPACK_IMPORTED_MODULE_6__["DealService"] },
    { type: _services_common_service__WEBPACK_IMPORTED_MODULE_15__["CommonService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["MenuController"] }
];
HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/rider/home/home.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home.page.scss */ "./src/app/rider/home/home.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_12__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["AlertController"],
        _services_place_service__WEBPACK_IMPORTED_MODULE_2__["PlaceService"],
        _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_11__["Geolocation"],
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
        _services_setting_service__WEBPACK_IMPORTED_MODULE_3__["SettingService"],
        _services_trip_service__WEBPACK_IMPORTED_MODULE_9__["TripService"],
        _services_driver_service__WEBPACK_IMPORTED_MODULE_7__["DriverService"],
        _angular_fire_auth__WEBPACK_IMPORTED_MODULE_8__["AngularFireAuth"],
        _services_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"],
        _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"],
        _services_deal_service__WEBPACK_IMPORTED_MODULE_6__["DealService"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_15__["CommonService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_10__["MenuController"]])
], HomePage);



/***/ }),

/***/ "./src/app/services/deal.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/deal.service.ts ***!
  \******************************************/
/*! exports provided: DealService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DealService", function() { return DealService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/environments/environment.prod */ "./src/environments/environment.prod.ts");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/es2015/index.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./auth.service */ "./src/app/services/auth.service.ts");





let DealService = class DealService {
    constructor(db, authService) {
        this.db = db;
        this.authService = authService;
    }
    // sort driver by rating & distance
    sortDriversList(drivers) {
        return drivers.sort((a, b) => {
            return (a.rating - a.distance / 5) - (b.rating - b.distance / 5);
        });
    }
    // make deal to driver
    makeDeal(driverId, origin, destination, distance, fee, currency, note, paymentMethod, promocode, discount, locationDetails, recipientName, mobileNumber, itemCategory) {
        let user = this.authService.getUserData();
        return this.db.object('deals/' + driverId).set({
            passengerId: user.uid,
            currency: currency,
            origin: origin,
            destination: destination,
            distance: distance,
            fee: fee,
            note: note,
            paymentMethod: paymentMethod,
            status: src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_2__["DEAL_STATUS_PENDING"],
            createdAt: Date.now(),
            promocode: promocode,
            discount: discount,
            locationDetails: locationDetails,
            recipientName: recipientName,
            mobileNumber: mobileNumber,
            itemCategory: itemCategory
        });
    }
    // get deal by driverId
    getDriverDeal(driverId) {
        return this.db.object('deals/' + driverId);
    }
    // remove deal
    removeDeal(driverId) {
        return this.db.object('deals/' + driverId).remove();
    }
};
DealService.ctorParameters = () => [
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_3__["AngularFireDatabase"] },
    { type: _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] }
];
DealService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_3__["AngularFireDatabase"], _auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
], DealService);



/***/ }),

/***/ "./src/app/services/setting.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/setting.service.ts ***!
  \*********************************************/
/*! exports provided: SettingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingService", function() { return SettingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/es2015/index.js");



let SettingService = class SettingService {
    constructor(db) {
        this.db = db;
    }
    getPrices() {
        return this.db.object('master_settings/prices');
    }
};
SettingService.ctorParameters = () => [
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"] }
];
SettingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]])
], SettingService);



/***/ })

}]);
//# sourceMappingURL=rider-home-home-module-es2015.js.map