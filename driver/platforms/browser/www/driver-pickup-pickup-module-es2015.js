(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["driver-pickup-pickup-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/driver/pickup/pickup.page.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/driver/pickup/pickup.page.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>{{'RIDE_INFORMATION' | translate}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card>\n    <ion-card-content>\n      <ion-list lines=\"none\">\n        <ion-item *ngIf=\"passenger.phoneNumber\">\n          <ion-label>\n            <ion-text>\n              <h2>{{(passenger)?.first_name }} {{(passenger)?.last_name }}</h2>\n            </ion-text>\n            <ion-text>\n              <p>{{(passenger)?.phoneNumber }}</p>\n            </ion-text>\n          </ion-label>\n          <ion-button slot=\"end\" href=\"tel:{{passenger.phoneNumber}}\">\n            <ion-icon name=\"call\"></ion-icon>&nbsp; {{'CALL' | translate}}\n          </ion-button>\n          <ion-button color=\"dark\" slot=\"end\"  (click)=\"showChat()\">\n            <ion-icon name=\"chatbubbles\"></ion-icon>&nbsp;Message\n          </ion-button>\n\n        </ion-item>\n        <ion-item>\n          <ion-label>\n            <ion-text>\n              <h2>{{'FROM' | translate}}</h2>\n            </ion-text>\n            <ion-text>\n              <p *ngIf=\"trip.origin != undefined\">{{ trip.origin.vicinity }}</p>\n            </ion-text>\n          </ion-label>\n          <ion-button slot=\"end\" fill=\"clear\" color=\"dark\"\n                      (click)=\"getDirection(trip.origin.location.lat,trip.origin.location.lng)\">\n            <ion-icon name=\"navigate\"></ion-icon>&nbsp; {{'NAVIGATE' | translate}}\n          </ion-button>\n        </ion-item>\n        <ion-item>\n          <ion-label>\n            <ion-text>\n              <h2>{{'TO' | translate}}</h2>\n            </ion-text>\n            <ion-text>\n              <p *ngIf=\"trip.destination != undefined\">{{ trip.destination.vicinity }}</p>\n              <p *ngIf=\"trip.destination != undefined\">{{ trip.locationDetails }}</p>\n            </ion-text>\n          </ion-label>\n          <ion-button slot=\"end\" fill=\"clear\" color=\"dark\"\n                      (click)=\"getDirection(trip.destination.location.lat,trip.destination.location.lng)\">\n            <ion-icon name=\"navigate\"></ion-icon>&nbsp; {{'NAVIGATE' | translate}}\n          </ion-button>\n        </ion-item>\n        <ion-item>\n          <ion-label>\n            <ion-text>\n              <h2>Delivery Information</h2>\n            </ion-text>\n            <ion-text>\n              <p>{{ trip.recipientName }}</p>\n              <p>{{ trip.mobileNumber }}</p>\n              <p>{{ trip.itemCategory }}</p>\n            </ion-text>\n          </ion-label>\n          <ion-button slot=\"end\" href=\"tel:{{trip.mobileNumber}}\" [hidden]=\"!isTripStarted\">\n            <ion-icon name=\"call\"></ion-icon>&nbsp; {{'CALL' | translate}}\n          </ion-button>\n        </ion-item>\n        <ion-item>\n          <ion-label>\n            <img [src]=\"trip.imageUrl\"  *ngIf=\"trip.imageUrl\"/>\n          </ion-label>\n          <ion-button slot=\"end\" (click)=\"upload()\" color=\"success\">\n            <ion-icon name=\"camera\"></ion-icon>&nbsp; Take a Picture\n          </ion-button>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n  <ion-badge>{{ trip.payment_method }}</ion-badge>\n</ion-content>\n<ion-footer>\n  <ion-toolbar>\n    <ion-button expand=\"block\" color=\"dark\" [hidden]=\"isTripStarted\" (click)=\"pickup()\">{{'PICKUP' | translate}}\n    </ion-button>\n    <ion-button expand=\"block\" color=\"danger\" [hidden]=\"!isTripStarted\" (click)=\"showPayment()\">\n      {{'COMPLETE_RIDE' | translate}}</ion-button>\n  </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ "./src/app/driver/pickup/pickup.module.ts":
/*!************************************************!*\
  !*** ./src/app/driver/pickup/pickup.module.ts ***!
  \************************************************/
/*! exports provided: PickupPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PickupPageModule", function() { return PickupPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _pickup_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pickup.page */ "./src/app/driver/pickup/pickup.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");








const routes = [
    {
        path: '',
        component: _pickup_page__WEBPACK_IMPORTED_MODULE_6__["PickupPage"]
    }
];
let PickupPageModule = class PickupPageModule {
};
PickupPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_pickup_page__WEBPACK_IMPORTED_MODULE_6__["PickupPage"]]
    })
], PickupPageModule);



/***/ }),

/***/ "./src/app/driver/pickup/pickup.page.scss":
/*!************************************************!*\
  !*** ./src/app/driver/pickup/pickup.page.scss ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("h2 {\n  font-weight: bolder;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZHJpdmVyL3BpY2t1cC9DOlxcVXNlcnNcXEplcmljbyBQYXVsb1xcUnVieW1pbmVQcm9qZWN0c1xcdG52c1xcbW9iaWxlL3NyY1xcYXBwXFxkcml2ZXJcXHBpY2t1cFxccGlja3VwLnBhZ2Uuc2NzcyIsInNyYy9hcHAvZHJpdmVyL3BpY2t1cC9waWNrdXAucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsbUJBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL2RyaXZlci9waWNrdXAvcGlja3VwLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImgyIHtcclxuICBmb250LXdlaWdodDogYm9sZGVyO1xyXG59IiwiaDIge1xuICBmb250LXdlaWdodDogYm9sZGVyO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/driver/pickup/pickup.page.ts":
/*!**********************************************!*\
  !*** ./src/app/driver/pickup/pickup.page.ts ***!
  \**********************************************/
/*! exports provided: PickupPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PickupPage", function() { return PickupPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _services_trip_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/trip.service */ "./src/app/driver/services/trip.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_deal_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/deal.service */ "./src/app/driver/services/deal.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../services/common.service */ "./src/app/driver/services/common.service.ts");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/es2015/index.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../environments/environment.prod */ "./src/environments/environment.prod.ts");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/es2015/index.js");














let PickupPage = class PickupPage {
    constructor(tripDriverService, alertCtrl, dealService, router, common, translate, db, menuCtrl, geolocation, camera, afStorage) {
        this.tripDriverService = tripDriverService;
        this.alertCtrl = alertCtrl;
        this.dealService = dealService;
        this.router = router;
        this.common = common;
        this.translate = translate;
        this.db = db;
        this.menuCtrl = menuCtrl;
        this.geolocation = geolocation;
        this.camera = camera;
        this.afStorage = afStorage;
        this.trip = {};
        this.passenger = {};
        this.isTripStarted = false;
        this.menuCtrl.enable(true);
    }
    ngOnInit() { }
    ionViewDidEnter() {
        this.trip = this.tripDriverService.getCurrentTrip();
        if (!this.trip.key) {
            this.tripDriverService.getTrips().valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1)).subscribe((trips) => {
                trips.forEach(trip => {
                    if (trip.status == _environments_environment_prod__WEBPACK_IMPORTED_MODULE_11__["TRIP_STATUS_WAITING"] || trip.status == _environments_environment_prod__WEBPACK_IMPORTED_MODULE_11__["TRIP_STATUS_GOING"]) {
                        this.tripDriverService.setCurrentTrip(trip.key);
                        this.trip = this.tripDriverService.getCurrentTrip();
                    }
                });
            });
        }
        let getTrips = this.tripDriverService.getTripStatus(this.trip.key).valueChanges().subscribe((trip) => {
            if (trip.status == 'canceled') {
                getTrips.unsubscribe();
                this.tripDriverService.cancel(this.trip.key);
                this.dealService.removeDeal(this.trip.driverId);
                this.common.showAlert("Trip Cancelled");
                this.router.navigateByUrl('/driver/home');
            }
        });
        this.tripDriverService.getPassenger(this.trip.passengerId).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1)).subscribe(snapshot => {
            this.passenger = snapshot;
        });
    }
    // pickup
    pickup() {
        this.alertCtrl.create({
            subHeader: "Please Enter OTP from customer",
            inputs: [{
                    name: 'otp',
                    placeholder: '4 digit OTP'
                }],
            buttons: [{
                    text: "Verify",
                    handler: (data) => {
                        this.db.object('trips/' + this.trip.key).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1)).subscribe((res) => {
                            if (res.otp != data.otp)
                                this.common.showAlert("Invalid OTP");
                            else {
                                this.isTripStarted = true;
                                this.tripDriverService.pickUp(this.trip.key);
                            }
                        });
                    }
                }]
        }).then(res => res.present());
    }
    getDirection(lat, lng) {
        console.log("call");
        this.geolocation.getCurrentPosition().then(geo => {
            geo.coords.latitude;
            let url = "https://www.google.com/maps/dir/?api=1&travelmode=driving&origin=" + geo.coords.latitude + "," + geo.coords.longitude + "&destination=" + lat + "," + lng;
            window.open(url);
        }).catch(err => {
            console.log("Error ");
        });
    }
    showPayment() {
        let final = this.trip.fee - (this.trip.fee * (parseInt(this.trip.discount) / 100));
        this.alertCtrl.create({
            message: '<h1>' + this.trip.currency + ' ' + final + '</h1> <p>Fee: ' + this.trip.fee + ' <br> Discount (%): ' + this.trip.discount + ' (' + this.trip.promocode + ')<br>Payment Method: ' + this.trip.paymentMethod + '</p>',
            buttons: [
                {
                    text: 'OK',
                    handler: () => {
                        this.tripDriverService.dropOff(this.trip.key);
                        this.dealService.removeDeal(this.trip.driverId);
                        this.router.navigateByUrl('/driver/home');
                    }
                }
            ]
        }).then(res => res.present());
    }
    showChat() {
        this.router.navigateByUrl('/driver/chat');
    }
    upload() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const options = {
                quality: 100,
                destinationType: this.camera.DestinationType.FILE_URI,
                encodingType: this.camera.EncodingType.JPEG,
                mediaType: this.camera.MediaType.PICTURE
            };
            const base64 = yield this.getPicture();
            const blob = this.b64toBlob(base64, 'image/jpeg', 512);
            const tempFilename = base64.substr(base64.lastIndexOf('/') + 1);
            const path = 'trips/' + Date.now() + `_${tempFilename}`;
            this.captureDataUrl = this.uploadToFirebase(blob, path);
        });
    }
    getPicture() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const options = {
                quality: 30,
                destinationType: this.camera.DestinationType.DATA_URL,
                encodingType: this.camera.EncodingType.JPEG,
                mediaType: this.camera.MediaType.PICTURE,
                targetWidth: 200
            };
            var base64 = yield this.camera.getPicture(options);
            return base64;
        });
    }
    //https://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
    b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }
    uploadToFirebase(blob, path) {
        const ref = this.afStorage.ref(path);
        let imageUrl = "";
        ref.put(blob).then(() => {
            ref.getDownloadURL().pipe().subscribe(data => {
                this.trip.imageUrl = data;
                this.updateTripImageUrl();
            });
        });
        console.log("imageUrl: " + imageUrl);
        return imageUrl;
    }
    updateTripImageUrl() {
        this.db.object('trips/' + this.trip.key).update(this.trip).then(data => {
            this.common.showToast("Updated successfully");
        });
    }
};
PickupPage.ctorParameters = () => [
    { type: _services_trip_service__WEBPACK_IMPORTED_MODULE_3__["TripDriverService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _services_deal_service__WEBPACK_IMPORTED_MODULE_5__["DealService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _services_common_service__WEBPACK_IMPORTED_MODULE_8__["CommonService"] },
    { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"] },
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_9__["AngularFireDatabase"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__["Geolocation"] },
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_12__["Camera"] },
    { type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_13__["AngularFireStorage"] }
];
PickupPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-pickup',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./pickup.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/driver/pickup/pickup.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./pickup.page.scss */ "./src/app/driver/pickup/pickup.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_trip_service__WEBPACK_IMPORTED_MODULE_3__["TripDriverService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"],
        _services_deal_service__WEBPACK_IMPORTED_MODULE_5__["DealService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_8__["CommonService"],
        _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"],
        _angular_fire_database__WEBPACK_IMPORTED_MODULE_9__["AngularFireDatabase"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"],
        _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__["Geolocation"],
        _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_12__["Camera"],
        _angular_fire_storage__WEBPACK_IMPORTED_MODULE_13__["AngularFireStorage"]])
], PickupPage);



/***/ })

}]);
//# sourceMappingURL=driver-pickup-pickup-module-es2015.js.map