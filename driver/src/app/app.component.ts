import { Component,ViewChildren, QueryList  } from '@angular/core';

import { Platform,AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from './services/auth.service';
import { TripService } from './services/trip.service';
import { TRIP_STATUS_WAITING, TRIP_STATUS_GOING } from 'src/environments/environment.prod';
import { AngularFireAuth } from '@angular/fire/auth';
import { DriverService } from './services/driver.service';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { IonRouterOutlet } from '@ionic/angular';

import { Storage } from '@ionic/storage';
import { CommonService } from './services/common.service';


@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {
    user: any = {};
    driver: any = {};
    isUser: any = false;
    isDriver: any = false;
    public driverPages = [
        {
            title: 'Home',
            url: '/home',
            icon: 'home'
        },
        {
            title: 'Profile',
            url: '/profile',
            icon: 'contact'
        },
        {
            title: 'Ride History',
            url: '/history',
            icon: 'time'
        },
        {
            title: 'Settings',
            url: '/settings',
            icon: 'settings'
        }

    ];
    @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private translate: TranslateService,
        private afAuth: AngularFireAuth,
        private tripService: TripService,
        private driverService: DriverService,
        private authService: AuthService,
        private router: Router,
        private alertCtrl: AlertController,
        // private bgmode: BackgroundMode,
        private storage: Storage,
        private common: CommonService
    ) {
        this.initializeApp();
        this.backButtonEvent();
    }
    getProfileUrl(){
        if (this.user.driver){
            return '/profile'
        }else{
            return '/rider/profile'
        }
    }
    backButtonEvent() {
        this.platform.backButton.subscribe(async () => {

            this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
                if (this.router.url === '/home' || this.router.url === '/tracking') {
                    this.alertCtrl.create({
                        header: 'Exit eShip',
                        message: "Are you sure?",
                        buttons: [
                            {
                                text: 'No',
                                handler: () => {
                                }
                            },
                            {
                                text: 'Yes',
                                handler: () => {
                                    navigator['app'].exitApp();
                                }
                            }
                        ]
                    }).then(r => r.present());
                } else if (outlet && outlet.canGoBack()) {
                    outlet.pop();
                }
            });
        });
    }
    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.translate.setDefaultLang('en');
            this.translate.use('en');

            // check for login stage, then redirect
            this.afAuth.authState.subscribe(authData => {
                if (authData) {
                    this.authService.getUser(authData.uid).snapshotChanges().pipe(take(1)).subscribe((snapshot: any) => {
                        this.isDriver = (snapshot != null && snapshot.key != undefined);
                        this.driver = { uid: snapshot.key, ...snapshot.payload.val() };
                        if (this.isDriver) {
                            let root: any = 'home';

                            // check for uncompleted trip
                            this.tripService.getTrips().valueChanges().pipe(take(1)).subscribe((trips: any) => {
                                trips.forEach(trip => {
                                    if (trip.status == TRIP_STATUS_WAITING || trip.status == TRIP_STATUS_GOING) {
                                        this.tripService.setCurrentTrip(trip.key);
                                        root = 'pickup';
                                    }
                                });

                                this.user = this.authService.getUserData();
                                this.driverService.setUser(this.user);
                                this.driverService.getDriver().valueChanges().subscribe(snapshot => {
                                    this.driver = snapshot;
                                });

                                this.router.navigateByUrl(root, { replaceUrl: true })
                            });
                        }
                    });
                } else {
                    this.router.navigateByUrl('login');
                    this.driver = null;
                }
            });

            // this.storage.get('iondriver_settings').then(data => {
            //     if (data != null && data != undefined) {
            //         let res = JSON.parse(data);
            //         if (res.bgmode) this.common.enableBgMode();
            //         else this.common.disableBgMode();
            //     }
            // });
        });
    }
}
