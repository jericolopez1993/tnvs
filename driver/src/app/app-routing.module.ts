import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
    {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
    },
    { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
    { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
    { path: 'history', loadChildren: './history/history.module#HistoryPageModule' },
    { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
    { path: 'wallet', loadChildren: './wallet/wallet.module#WalletPageModule' },
    { path: 'settings', loadChildren: './settings/settings.module#SettingsPageModule' },
    { path: 'pickup', loadChildren: './pickup/pickup.module#PickupPageModule' },
    { path: 'chat', loadChildren: './chat/chat.module#ChatPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
