import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TripService } from '../services/trip.service';
import { ReportService } from '../services/report.service';


@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage {
  // statistic
  public  stats: any = {
    today: 0,
    yesterday: 0,
    thisMonth: 0,
    lastMonth: 0,
    thisYear: 0,
    lastYear: 0
  };

  // list of records
  public trips = [];

  constructor(private tripDriverService: TripService,
    private reportService: ReportService,
    private translate: TranslateService) {

  }
  ngOnInit() { }

  ionViewDidEnter() {
    let today = new Date();
    let lastYear = today.getFullYear() - 1;
    let lastMonth = (today.getMonth() > 0) ? today.getMonth() : 12;
    let yesterday = new Date(Date.now() - 86400000);
    let thisYear = today.getFullYear();
    let thisMonth = today.getMonth() + 1;

    let todaySum = 0;
    let yesterdaySum = 0;
    let thisMonthSum = 0;
    let lastMonthSum = 0;
    let thisYearSum = 0;
    let lastYearSum = 0;



    console.log(this.stats.today);
    this.tripDriverService.getTrips().valueChanges().subscribe(snapshot => {
      if (snapshot != null) {
        this.trips = snapshot.reverse();
        console.log(this.stats.today);
        snapshot.forEach(function (trip){
          if (today.toDateString() == new Date(trip['droppedOffAt']).toDateString()) {
            todaySum = todaySum + trip['fee']
          }
          if (yesterday >= new Date(trip['droppedOffAt']) && today <= new Date(trip['droppedOffAt'])) {
            yesterdaySum = yesterdaySum + trip['fee']
          }
          if (thisMonth == new Date(trip['droppedOffAt']).getMonth() ) {
            thisMonthSum = thisMonthSum + trip['fee']
          }
          if (lastMonth == new Date(trip['droppedOffAt']).getMonth() ) {
            lastMonthSum = lastMonthSum + trip['fee']
          }
          if (thisYear == new Date(trip['droppedOffAt']).getFullYear() ) {
            thisYearSum = thisYearSum + trip['fee']
          }
          if (lastYear == new Date(trip['droppedOffAt']).getFullYear() ) {
            lastYearSum = lastYearSum + trip['fee']
          }
        });
        this.stats.today = todaySum;
        this.stats.yesterday = yesterdaySum;
        this.stats.thisMonth = thisMonthSum;
        this.stats.lastMonth = lastMonthSum;
        this.stats.thisYear = thisYearSum;
        this.stats.lastYear = lastYearSum;
        console.log(new Date(snapshot.reverse()[0]['droppedOffAt']).getDate());
      }

    });
  }

}
