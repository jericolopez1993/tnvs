import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {ENABLE_SIGNUP, TRIP_STATUS_GOING, TRIP_STATUS_WAITING} from 'src/environments/environment.prod'
import { CommonService } from '../services/common.service';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import {take} from "rxjs/operators";
import {TripService} from "../services/trip.service";
import {DriverService} from "../services/driver.service"

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    userInfo: any = {};
    isRegisterEnabled = ENABLE_SIGNUP;
    driver: any;
    isUser: any = false;
    isDriver: any = true;
    constructor(public translate: TranslateService,
                private common: CommonService,
                private auth: AuthService,
                private router: Router,
                private menuCtrl: MenuController,
                private tripDriverService: TripService,
                private driverService: DriverService
    ) {
        this.menuCtrl.enable(false);
    }

    ngOnInit() {
    }
    login() {
        if (this.userInfo.email.length == 0 || this.userInfo.password.length == 0) {
            this.common.showAlert("Invalid Credentials");
        }
        else {
            this.common.showLoader('Authenticating...');
            this.auth.login(this.userInfo.email, this.userInfo.password).then(authData => {
                if (authData) {
                    this.auth.getUser(authData.user.uid).snapshotChanges().pipe(take(1)).subscribe((snapshot: any) => {
                        if (snapshot.key) {
                            this.driver = { uid: snapshot.key, ...snapshot.payload.val() };
                            let root: any = 'home';

                            // check for uncompleted trip
                            this.tripDriverService.getTrips().valueChanges().pipe(take(1)).subscribe((trips: any) => {
                                trips.forEach(trip => {
                                    if (trip.status == TRIP_STATUS_WAITING || trip.status == TRIP_STATUS_GOING) {
                                        this.tripDriverService.setCurrentTrip(trip.key);
                                        root = 'pickup';
                                    }
                                });

                                this.driver = this.auth.getUserData();
                                this.driverService.setUser(this.driver);
                                this.driverService.getDriver().valueChanges().subscribe(snapshot => {
                                    this.driver = snapshot;
                                });

                                this.router.navigateByUrl(root)
                            });
                        }else{
                            this.auth.logout().then(() => {
                                this.common.hideLoader();
                                this.common.showToast("Driver doesn't exist. Please try again.");
                            });
                        }
                    });
                }
            }, error => {
                this.common.hideLoader();
                this.common.showToast(error.message);
            });
        }
    }


    reset() {
        if (this.userInfo.email) {
            this.auth.reset(this.userInfo.email)
                .then(data => this.common.showToast('Please Check your inbox'))
                .catch(err => this.common.showToast(err.message));
        }
        else
            this.common.showToast("Please Enter Email Address");
    }
}
