import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TripService } from '../services/trip.service';
import { Router } from '@angular/router';
import { DealService } from '../services/deal.service';
import { take } from 'rxjs/operators';
import {AlertController, MenuController} from '@ionic/angular';
import { CommonService } from '../services/common.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {TRIP_STATUS_GOING, TRIP_STATUS_WAITING} from "../../environments/environment.prod";
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AngularFireStorage } from '@angular/fire/storage';


@Component({
    selector: 'app-pickup',
    templateUrl: './pickup.page.html',
    styleUrls: ['./pickup.page.scss'],
})
export class PickupPage implements OnInit {
    trip: any = {};
    passenger: any = {};
    isTripStarted = false;
    captureDataUrl: any;
    constructor(
        private tripDriverService: TripService,
        private alertCtrl: AlertController,
        private dealService: DealService,
        private router: Router,
        private common: CommonService,
        private translate: TranslateService,
        private db: AngularFireDatabase,
        private menuCtrl: MenuController,
        private geolocation: Geolocation,
        private camera: Camera,
        private afStorage: AngularFireStorage,

    ) {
        this.menuCtrl.enable(true);
    }

    ngOnInit() { }

    ionViewDidEnter() {
        this.trip = this.tripDriverService.getCurrentTrip();
        if (!this.trip.key) {
            this.tripDriverService.getTrips().valueChanges().pipe(take(1)).subscribe((trips: any) => {
                trips.forEach(trip => {
                    if (trip.status == TRIP_STATUS_WAITING || trip.status == TRIP_STATUS_GOING) {
                        this.tripDriverService.setCurrentTrip(trip.key);
                        this.trip = this.tripDriverService.getCurrentTrip();
                    }
                });
            });
        }
        let getTrips = this.tripDriverService.getTripStatus(this.trip.key).valueChanges().subscribe((trip: any) => {
            if (trip.status == 'canceled') {
                getTrips.unsubscribe();
                this.tripDriverService.cancel(this.trip.key);
                this.dealService.removeDeal(this.trip.driverId);
                this.common.showAlert("Trip Cancelled");
                this.router.navigateByUrl('/home');
            }
        })
        this.tripDriverService.getPassenger(this.trip.passengerId).valueChanges().pipe(take(1)).subscribe(snapshot => {
            this.passenger = snapshot;
        })
    }

    // pickup
    pickup() {
        this.isTripStarted = true;
        this.tripDriverService.pickUp(this.trip.key);
        // this.alertCtrl.create({
        //     subHeader: "Please Enter OTP from customer",
        //     inputs: [{
        //         name: 'otp',
        //         placeholder: '4 digit OTP'
        //     }],
        //     buttons: [{
        //         text: "Verify",
        //         handler: (data) => {
        //             this.db.object('trips/' + this.trip.key).valueChanges().pipe(take(1)).subscribe((res: any) => {
        //                 if (res.otp != data.otp) this.common.showAlert("Invalid OTP");
        //                 else {
        //                     this.isTripStarted = true;
        //                     this.tripDriverService.pickUp(this.trip.key);
        //                 }
        //             })
        //         }
        //     }]
        // }).then(res => res.present());
    }

    getDirection(lat, lng) {
        this.geolocation.getCurrentPosition().then(geo => {
            geo.coords.latitude
            let url = "https://www.google.com/maps/dir/?api=1&travelmode=driving&origin=" + geo.coords.latitude + "," + geo.coords.longitude + "&destination=" + lat + "," + lng;
            window.open(url);
        }).catch(err => {
            console.log("Error ");
        })
    }

    showPayment() {
        let final = this.trip.fee - (this.trip.fee * (parseInt(this.trip.discount) / 100))
        this.alertCtrl.create({
            message: '<h1>' + this.trip.currency + ' ' + final + '</h1> <p>Fee: ' + this.trip.fee + ' <br> Discount (%): ' + this.trip.discount + ' (' + this.trip.promocode + ')<br>Payment Method: ' + this.trip.paymentMethod + '</p>',
            buttons: [
                {
                    text: 'OK',
                    handler: () => {
                        this.tripDriverService.dropOff(this.trip.key);
                        this.dealService.removeDeal(this.trip.driverId);
                        this.router.navigateByUrl('/home');
                    }
                }
            ]
        }).then(res => res.present());
    }

    showChat() {
        this.router.navigateByUrl('/chat');
    }
    async upload() {
        const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        }
        const base64 = await this.getPicture();
        const blob = this.b64toBlob(base64, 'image/jpeg', 512);
        const tempFilename = base64.substr(base64.lastIndexOf('/') + 1);
        const path = 'trips/' + Date.now() + `_${tempFilename}`;
        this.captureDataUrl = this.uploadToFirebase(blob, path);
    }
    async getPicture() {
        const options: CameraOptions = {
            quality: 30,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            targetWidth: 200
        }
        var base64 = await this.camera.getPicture(options);
        return base64;
    }
    //https://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
    b64toBlob(b64Data: string, contentType: string, sliceSize: number) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    uploadToFirebase(blob: Blob, path: string) {
        const ref = this.afStorage.ref(path);
        let imageUrl = "";
        ref.put(blob).then(() => {
            ref.getDownloadURL().pipe().subscribe(data => {
                this.trip.imageUrl = data;
                this.updateTripImageUrl();
            });
        })
        console.log("imageUrl: " + imageUrl);
        return imageUrl;
    }

    updateTripImageUrl() {
        this.db.object('trips/' + this.trip.key).update(this.trip).then(data => {
            this.common.showToast("Updated successfully");
        });
    }


}
