(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["profile-profile-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.page.html ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>{{ user.first_name }} {{ user.last_name }}</ion-title>\n    <ion-buttons slot=\"end\">\n\n      <ion-button (click)=\"logout()\">\n        <ion-icon name=\"log-out\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-segment [(ngModel)]=\"tabs\">\n    <ion-segment-button value=\"profile\">\n      {{'BASIC_PROFILE' | translate}}\n    </ion-segment-button>\n    <ion-segment-button value=\"carinfo\">\n      {{'CAR_INFO' | translate }}\n    </ion-segment-button>\n  </ion-segment>\n  <div [ngSwitch]=\"tabs\" class=\"ion-padding\">\n    <div *ngSwitchCase=\"'profile'\">\n      <div style=\"text-align: center;\">\n        <img src=\"{{ user.photoURL }}\" onError=\"this.src='assets/img/default-dp.png'\" style=\"width:50px;height:50px;border-radius:100px\" (click)=\"chooseFile()\">\n        <form ngNoForm>\n          <input id=\"avatar\" name=\"file\" type=\"file\" (change)=\"upload()\">\n        </form>\n      </div>\n      <ion-list lines=\"none\">\n        <ion-item>\n          <ion-label position=\"stacked\" color=\"primary\">{{'FIRST_NAME' | translate}}</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"user.first_name\"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\" color=\"primary\">{{'MIDDLE_NAME' | translate}}</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"user.middle_name\"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\" color=\"primary\">{{'LAST_NAME' | translate}}</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"user.last_name\"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\" color=\"primary\">{{'BIRTH_DATE' | translate}}</ion-label>\n          <ion-datetime displayFormat=\"MM/DD/YYYY\" placeholder=\"Select Date\" [(ngModel)]=\"user.birth_date\"></ion-datetime>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\" color=\"primary\">{{'GENDER' | translate}}</ion-label>\n          <ion-select placeholder=\" Select gender\" [(ngModel)]=\"user.gender\">\n            <ion-select-option value=\"female\">Female</ion-select-option>\n            <ion-select-option value=\"male\">Male</ion-select-option>\n          </ion-select>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\" color=\"primary\">{{'ADDRESS' | translate}}</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"user.address\"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\">{{'EMAIL_ADDRESS' | translate}}</ion-label>\n          <ion-input type=\"email\" [(ngModel)]=\"user.email\" disabled placeholder=\"{{'EMAIL_ADDRESS' | translate}}\">\n          </ion-input>\n          <ion-button ion-button item-right clear *ngIf=\"!user.isEmailVerified\" (click)=\"verifyEmail()\">\n            {{'VERIFY' | translate}}</ion-button>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\">{{'PHONE_NUMBER' | translate}}</ion-label>\n          <ion-input type=\"tel\" [(ngModel)]=\"user.phoneNumber\" [disabled]=\"user.isPhoneVerified\"\n            placeholder=\"{{'PHONE_NUMBER' | translate}}\"></ion-input>\n        </ion-item>\n      </ion-list>\n    </div>\n    <div *ngSwitchCase=\"'carinfo'\">\n      <ion-list lines=\"none\">\n        <ion-item>\n          <ion-label position=\"stacked\">{{'CAR_BRAND' | translate}}</ion-label>\n          <span>{{user.brand}}</span>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\">{{'CAR_MODEL' | translate}}</ion-label>\n          <span>{{user.brand}}</span>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"stacked\">{{'PLATE_NUMBER' | translate}}</ion-label>\n          <span>{{user.plate}}</span>\n        </ion-item>\n        <ion-item *ngIf=\"types\">\n          <ion-label position=\"stacked\">{{'CAR_TYPE' | translate}}</ion-label>\n          <span>{{user.type}}</span>\n        </ion-item>\n      </ion-list>\n    </div>\n  </div>\n</ion-content>\n<ion-footer class=\"ion-padding\">\n  <ion-button expand=\"block\" (click)=\"save()\">{{'SAVE' | translate}}</ion-button>\n</ion-footer>");

/***/ }),

/***/ "./src/app/profile/profile.module.ts":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.module.ts ***!
  \*******************************************/
/*! exports provided: ProfilePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profile.page */ "./src/app/profile/profile.page.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");








const routes = [
    {
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]
    }
];
let ProfilePageModule = class ProfilePageModule {
};
ProfilePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_6__["ProfilePage"]]
    })
], ProfilePageModule);



/***/ }),

/***/ "./src/app/profile/profile.page.scss":
/*!*******************************************!*\
  !*** ./src/app/profile/profile.page.scss ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2ZpbGUvcHJvZmlsZS5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/profile/profile.page.ts":
/*!*****************************************!*\
  !*** ./src/app/profile/profile.page.ts ***!
  \*****************************************/
/*! exports provided: ProfilePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePage", function() { return ProfilePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment.prod */ "./src/environments/environment.prod.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_setting_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services/setting.service */ "./src/app/services/setting.service.ts");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/storage/es2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");











let ProfilePage = class ProfilePage {
    constructor(authService, settingService, common, platform, translate, router, afStorage) {
        this.authService = authService;
        this.settingService = settingService;
        this.common = common;
        this.platform = platform;
        this.translate = translate;
        this.router = router;
        this.afStorage = afStorage;
        this.user = {};
        this.support = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["CUSTOMER_CARE"];
        this.tripCount = 0;
        this.totalEarning = 0;
        this.rating = 5;
        this.types = [];
        this.tabs = 'profile';
    }
    ngOnInit() {
        // this.user = this.authService.getUser(this.authService.getUserData().uid);
        this.authService.getUser(this.authService.getUserData().uid).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["take"])(1)).subscribe((snapshot) => {
            this.user = snapshot;
        });
        this.currency = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["CURRENCY_SYMBOL"];
        this.settingService.getVehicleType().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["take"])(1)).subscribe((snapshot) => {
            if (snapshot === null) {
                this.settingService.getDefaultVehicleType().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["take"])(1)).subscribe((snapshot) => {
                    this.types = Object.keys(snapshot).map(function (key) {
                        return snapshot[key];
                    });
                });
            }
            else {
                this.types = Object.keys(snapshot).map(function (key) {
                    return snapshot[key];
                });
            }
        });
    }
    save() {
        this.authService.getUser(this.user.uid).update(this.user).then(data => {
            this.common.showToast("Updated successfully");
            this.router.navigateByUrl('/home');
        });
    }
    chooseFile() { document.getElementById('avatar').click(); }
    upload() {
        // Create a root reference
        this.common.showLoader('Uploading..');
        for (let selectedFile of [document.getElementById('avatar').files[0]]) {
            let path = '/users/' + Date.now() + `_${selectedFile.name}`;
            let ref = this.afStorage.ref(path);
            ref.put(selectedFile).then(() => {
                ref.getDownloadURL().subscribe(data => { this.user.photoURL = data; });
                this.common.hideLoader();
            }).catch(err => {
                this.common.hideLoader();
                console.log(err);
            });
        }
    }
    // code for uploading licence image
    chooseDocs() { document.getElementById('docsPDF').click(); }
    uploadDocs() {
        this.common.showLoader('Uploading..');
        for (let selectedFile of [document.getElementById('docsPDF').files[0]]) {
            let path = '/users/' + Date.now() + `${selectedFile.name}`;
            let ref = this.afStorage.ref(path);
            ref.put(selectedFile).then(() => {
                ref.getDownloadURL().subscribe(url => this.user.docsURL = url);
                this.common.hideLoader();
            }).catch(err => {
                this.common.hideLoader();
                console.log(err);
            });
        }
    }
    logout() {
        this.authService.logout().then(() => this.router.navigateByUrl('/login', { skipLocationChange: true }));
    }
    verifyEmail() {
        this.authService.sendVerification();
    }
};
ProfilePage.ctorParameters = () => [
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"] },
    { type: _services_setting_service__WEBPACK_IMPORTED_MODULE_6__["SettingService"] },
    { type: _services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"] },
    { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"] },
    { type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_9__["AngularFireStorage"] }
];
ProfilePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profile',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./profile.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/profile/profile.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./profile.page.scss */ "./src/app/profile/profile.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
        _services_setting_service__WEBPACK_IMPORTED_MODULE_6__["SettingService"],
        _services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["Platform"],
        _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"],
        _angular_fire_storage__WEBPACK_IMPORTED_MODULE_9__["AngularFireStorage"]])
], ProfilePage);



/***/ }),

/***/ "./src/app/services/setting.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/setting.service.ts ***!
  \*********************************************/
/*! exports provided: SettingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingService", function() { return SettingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/database/es2015/index.js");
/* harmony import */ var _place_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./place.service */ "./src/app/services/place.service.ts");




let SettingService = class SettingService {
    constructor(db, placeService) {
        this.db = db;
        this.placeService = placeService;
    }
    getVehicleType() {
        return this.db.object('master_settings/prices/' + this.placeService.getLocality() + '/vehicles').valueChanges();
    }
    getDefaultVehicleType() {
        return this.db.object('master_settings/prices/default/vehicles').valueChanges();
    }
};
SettingService.ctorParameters = () => [
    { type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"] },
    { type: _place_service__WEBPACK_IMPORTED_MODULE_3__["PlaceService"] }
];
SettingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"], _place_service__WEBPACK_IMPORTED_MODULE_3__["PlaceService"]])
], SettingService);



/***/ })

}]);
//# sourceMappingURL=profile-profile-module-es2015.js.map