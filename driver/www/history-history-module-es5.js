function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["history-history-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/history/history.page.html":
  /*!*********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/history/history.page.html ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppHistoryHistoryPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>{{'HISTORY' | translate}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-row style=\"background: #f1f1f1\">\n    <ion-col size=\"4\">\n      <h3>{{ stats.today }}</h3>\n      <p>Today</p>\n    </ion-col>\n    <ion-col size=\"4\">\n      <h3>{{ stats.yesterday }}</h3>\n      <p>Yesterday</p>\n    </ion-col>\n    <ion-col size=\"4\">\n      <h3>{{ stats.thisMonth }}</h3>\n      <p>This Month</p>\n    </ion-col>\n    <ion-col size=\"4\">\n      <h3>{{ stats.lastMonth }}</h3>\n      <p>Last Month</p>\n    </ion-col>\n    <ion-col size=\"4\">\n      <h3>{{ stats.thisYear }}</h3>\n      <p>This Year</p>\n    </ion-col>\n    <ion-col size=\"4\">\n      <h3>{{ stats.lastYear }}</h3>\n      <p>Last Year</p>\n    </ion-col>\n  </ion-row>\n  <p *ngIf=\"trips.length == 0\" style=\"margin-top: 5px; text-align: center\">No Trips Found</p>\n  <ion-list>\n    <ion-item *ngFor=\"let trip of trips\">\n      <ion-label>\n        <ion-text>\n          <p>ID: {{ trip.createdAt }} - {{trip.status}}</p>\n        </ion-text>\n        <ion-text>\n          <h3><span style=\"color:#4CAF50\">•</span> {{ trip.origin.vicinity }}</h3>\n          <p>{{ trip.pickedUpAt | date: 'medium'}}</p>\n        </ion-text>\n        <ion-text>\n          <h3><span style=\"color:#F44336\">•</span> {{ trip.destination.vicinity }}</h3>\n          <p>{{ trip.droppedOffAt | date: 'medium'}}</p>\n        </ion-text>\n        <ion-text>\n          <ion-row>\n            <ion-col>\n              <p>{{'FEE' | translate}}: {{trip.currency}} {{trip.fee}}</p>\n            </ion-col>\n            <ion-col *ngIf=\"trip.discount != 0\">\n              <p>Discount: {{trip.discount}} ({{trip.promocode}})</p>\n            </ion-col>\n            <ion-col>\n              <p>Payment Mode: {{ trip.paymentMethod }}</p>\n            </ion-col>\n          </ion-row>\n        </ion-text>\n      </ion-label>\n      <ion-badge slot=\"end\">\n        {{trip.currency}} {{ (trip.fee - (trip.fee * trip.discount / 100)).toFixed(2) }}\n      </ion-badge>\n    </ion-item>\n  </ion-list>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/history/history.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/history/history.module.ts ***!
    \*******************************************/

  /*! exports provided: HistoryPageModule */

  /***/
  function srcAppHistoryHistoryModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HistoryPageModule", function () {
      return HistoryPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _history_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./history.page */
    "./src/app/history/history.page.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var angular2_moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! angular2-moment */
    "./node_modules/angular2-moment/index.js");
    /* harmony import */


    var angular2_moment__WEBPACK_IMPORTED_MODULE_8___default =
    /*#__PURE__*/
    __webpack_require__.n(angular2_moment__WEBPACK_IMPORTED_MODULE_8__);

    var routes = [{
      path: '',
      component: _history_page__WEBPACK_IMPORTED_MODULE_6__["HistoryPage"]
    }];

    var HistoryPageModule = function HistoryPageModule() {
      _classCallCheck(this, HistoryPageModule);
    };

    HistoryPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"], angular2_moment__WEBPACK_IMPORTED_MODULE_8__["MomentModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_history_page__WEBPACK_IMPORTED_MODULE_6__["HistoryPage"]]
    })], HistoryPageModule);
    /***/
  },

  /***/
  "./src/app/history/history.page.scss":
  /*!*******************************************!*\
    !*** ./src/app/history/history.page.scss ***!
    \*******************************************/

  /*! exports provided: default */

  /***/
  function srcAppHistoryHistoryPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "h3 {\n  font-weight: 500;\n}\n\np {\n  color: #777;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGlzdG9yeS9DOlxcVXNlcnNcXEplcmljbyBQYXVsb1xcUnVieW1pbmVQcm9qZWN0c1xcdG52c1xcZHJpdmVyL3NyY1xcYXBwXFxoaXN0b3J5XFxoaXN0b3J5LnBhZ2Uuc2NzcyIsInNyYy9hcHAvaGlzdG9yeS9oaXN0b3J5LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdCQUFBO0FDQ0o7O0FEQ0E7RUFDSSxXQUFBO0FDRUoiLCJmaWxlIjoic3JjL2FwcC9oaXN0b3J5L2hpc3RvcnkucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaDN7XG4gICAgZm9udC13ZWlnaHQ6IDUwMFxufVxucHtcbiAgICBjb2xvcjogIzc3N1xufSIsImgzIHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxucCB7XG4gIGNvbG9yOiAjNzc3O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/history/history.page.ts":
  /*!*****************************************!*\
    !*** ./src/app/history/history.page.ts ***!
    \*****************************************/

  /*! exports provided: HistoryPage */

  /***/
  function srcAppHistoryHistoryPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HistoryPage", function () {
      return HistoryPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _services_trip_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../services/trip.service */
    "./src/app/services/trip.service.ts");
    /* harmony import */


    var _services_report_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../services/report.service */
    "./src/app/services/report.service.ts");

    var HistoryPage =
    /*#__PURE__*/
    function () {
      function HistoryPage(tripDriverService, reportService, translate) {
        _classCallCheck(this, HistoryPage);

        this.tripDriverService = tripDriverService;
        this.reportService = reportService;
        this.translate = translate; // statistic

        this.stats = {
          today: 0,
          yesterday: 0,
          thisMonth: 0,
          lastMonth: 0,
          thisYear: 0,
          lastYear: 0
        }; // list of records

        this.trips = [];
      }

      _createClass(HistoryPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this = this;

          this.reportService.getAll().valueChanges().subscribe(function (snapshot) {
            var today = new Date();
            var lastYear = today.getFullYear() - 1;
            var lastMonth = today.getMonth() > 0 ? today.getMonth() : 12;
            var yesterday = new Date(Date.now() - 86400000);
            var thisYear = today.getFullYear();
            var thisMonth = today.getMonth() + 1; // get current

            if (snapshot[thisYear]) {
              _this.stats.thisYear = snapshot[thisYear].total;

              if (snapshot[thisYear][thisMonth]) {
                _this.stats.thisMonth = snapshot[thisYear][thisMonth].total;

                if (snapshot[thisYear][thisMonth][today.getDate()]) {
                  _this.stats.today = snapshot[thisYear][thisMonth][today.getDate()].total;
                }
              }

              if (lastMonth != 12 && snapshot[thisYear][lastMonth]) {
                _this.stats.lastMonth = snapshot[thisYear][lastMonth].total;
              }
            } // get last year & last month data


            if (snapshot[lastYear]) {
              _this.stats.lastYear = snapshot[lastYear].total;

              if (lastMonth == 12 && snapshot[lastYear][lastMonth]) {
                _this.stats.lastMonth = snapshot[lastYear][lastMonth].total;
              }
            } // get yesterday's data


            if (snapshot[yesterday.getFullYear()] && snapshot[yesterday.getFullYear()][yesterday.getMonth() + 1] && snapshot[yesterday.getFullYear()][yesterday.getMonth() + 1][yesterday.getDate()]) {
              _this.stats.yesterday = snapshot[yesterday.getFullYear()][yesterday.getMonth() + 1][yesterday.getDate()].total;
            }
          });
          this.tripDriverService.getTrips().valueChanges().subscribe(function (snapshot) {
            if (snapshot != null) _this.trips = snapshot.reverse();
          });
        }
      }]);

      return HistoryPage;
    }();

    HistoryPage.ctorParameters = function () {
      return [{
        type: _services_trip_service__WEBPACK_IMPORTED_MODULE_3__["TripService"]
      }, {
        type: _services_report_service__WEBPACK_IMPORTED_MODULE_4__["ReportService"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"]
      }];
    };

    HistoryPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-history',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./history.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/history/history.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./history.page.scss */
      "./src/app/history/history.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_trip_service__WEBPACK_IMPORTED_MODULE_3__["TripService"], _services_report_service__WEBPACK_IMPORTED_MODULE_4__["ReportService"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"]])], HistoryPage);
    /***/
  },

  /***/
  "./src/app/services/report.service.ts":
  /*!********************************************!*\
    !*** ./src/app/services/report.service.ts ***!
    \********************************************/

  /*! exports provided: ReportService */

  /***/
  function srcAppServicesReportServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ReportService", function () {
      return ReportService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/fire/database */
    "./node_modules/@angular/fire/database/es2015/index.js");
    /* harmony import */


    var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./auth.service */
    "./src/app/services/auth.service.ts");

    var ReportService =
    /*#__PURE__*/
    function () {
      function ReportService(db, authService) {
        _classCallCheck(this, ReportService);

        this.db = db;
        this.authService = authService;
      }

      _createClass(ReportService, [{
        key: "getAll",
        value: function getAll() {
          var user = this.authService.getUserData();
          return this.db.object('reports/' + user.uid);
        }
      }]);

      return ReportService;
    }();

    ReportService.ctorParameters = function () {
      return [{
        type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]
      }, {
        type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
      }];
    };

    ReportService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"], _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])], ReportService);
    /***/
  }
}]);
//# sourceMappingURL=history-history-module-es5.js.map