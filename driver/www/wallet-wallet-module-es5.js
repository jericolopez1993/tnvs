function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["wallet-wallet-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/wallet/wallet.page.html":
  /*!*******************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/wallet/wallet.page.html ***!
    \*******************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppWalletWalletPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title>{{'WALLET' | translate}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content class=\"wallet\">\n  <div class=\"widhdraw-header\">\n    <h2 style=\"font-size: 4rem;\">{{currency}} {{ (driver)?.balance }}</h2>\n    <ion-button color=\"light\" (click)=\"withdraw()\">{{'WITHDRAW' | translate}}</ion-button>\n  </div>\n  <ion-list>\n    <ion-item-divider>\n      {{'HISTORY' | translate}}\n    </ion-item-divider>\n    <ion-item *ngFor=\"let record of records\">\n      <ion-label>\n        <ion-text color=\"medium\">\n          <p>{{ record.createdAt | amDateFormat: 'YYYY-MM-DD HH:mm'}}</p>\n        </ion-text>\n        <ion-text>\n          <p>You've request of <strong>{{currency}} {{ record.amount }}</strong>\n            is <code>{{ record.status}}</code>\n          </p>\n          <p>Ref: {{record.createdAt }} / key: {{record.$key}} / Type: {{ record.type }} </p>\n        </ion-text>\n      </ion-label>\n    </ion-item>\n  </ion-list>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/services/transaction.service.ts":
  /*!*************************************************!*\
    !*** ./src/app/services/transaction.service.ts ***!
    \*************************************************/

  /*! exports provided: TransactionService */

  /***/
  function srcAppServicesTransactionServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TransactionService", function () {
      return TransactionService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/fire/database */
    "./node_modules/@angular/fire/database/es2015/index.js");
    /* harmony import */


    var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./auth.service */
    "./src/app/services/auth.service.ts");
    /* harmony import */


    var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/environments/environment.prod */
    "./src/environments/environment.prod.ts");

    var TransactionService =
    /*#__PURE__*/
    function () {
      function TransactionService(db, authService) {
        _classCallCheck(this, TransactionService);

        this.db = db;
        this.authService = authService;
      }

      _createClass(TransactionService, [{
        key: "getTransactions",
        value: function getTransactions() {
          var user = this.authService.getUserData();
          return this.db.list('transactions', function (ref) {
            return ref.orderByChild('userId').equalTo(user.uid);
          });
        }
      }, {
        key: "widthDraw",
        value: function widthDraw(amount, balance) {
          var user = this.authService.getUserData();
          return this.db.list('transactions/').push({
            userId: user.uid,
            name: user.displayName,
            amount: amount,
            createdAt: Date.now(),
            type: src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_4__["TRANSACTION_TYPE_WITHDRAW"],
            status: 'PENDING'
          });
        }
      }]);

      return TransactionService;
    }();

    TransactionService.ctorParameters = function () {
      return [{
        type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"]
      }, {
        type: _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
      }];
    };

    TransactionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_fire_database__WEBPACK_IMPORTED_MODULE_2__["AngularFireDatabase"], _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]])], TransactionService);
    /***/
  },

  /***/
  "./src/app/wallet/wallet.module.ts":
  /*!*****************************************!*\
    !*** ./src/app/wallet/wallet.module.ts ***!
    \*****************************************/

  /*! exports provided: WalletPageModule */

  /***/
  function srcAppWalletWalletModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "WalletPageModule", function () {
      return WalletPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _wallet_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./wallet.page */
    "./src/app/wallet/wallet.page.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var angular2_moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! angular2-moment */
    "./node_modules/angular2-moment/index.js");
    /* harmony import */


    var angular2_moment__WEBPACK_IMPORTED_MODULE_8___default =
    /*#__PURE__*/
    __webpack_require__.n(angular2_moment__WEBPACK_IMPORTED_MODULE_8__);

    var routes = [{
      path: '',
      component: _wallet_page__WEBPACK_IMPORTED_MODULE_6__["WalletPage"]
    }];

    var WalletPageModule = function WalletPageModule() {
      _classCallCheck(this, WalletPageModule);
    };

    WalletPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"], angular2_moment__WEBPACK_IMPORTED_MODULE_8__["MomentModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_wallet_page__WEBPACK_IMPORTED_MODULE_6__["WalletPage"]]
    })], WalletPageModule);
    /***/
  },

  /***/
  "./src/app/wallet/wallet.page.scss":
  /*!*****************************************!*\
    !*** ./src/app/wallet/wallet.page.scss ***!
    \*****************************************/

  /*! exports provided: default */

  /***/
  function srcAppWalletWalletPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3dhbGxldC93YWxsZXQucGFnZS5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/wallet/wallet.page.ts":
  /*!***************************************!*\
    !*** ./src/app/wallet/wallet.page.ts ***!
    \***************************************/

  /*! exports provided: WalletPage */

  /***/
  function srcAppWalletWalletPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "WalletPage", function () {
      return WalletPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_driver_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../services/driver.service */
    "./src/app/services/driver.service.ts");
    /* harmony import */


    var _services_transaction_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../services/transaction.service */
    "./src/app/services/transaction.service.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/environments/environment.prod */
    "./src/environments/environment.prod.ts");
    /* harmony import */


    var _services_common_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../services/common.service */
    "./src/app/services/common.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");

    var WalletPage =
    /*#__PURE__*/
    function () {
      function WalletPage(transactionService, translate, driverService, common, alertCtrl) {
        var _this = this;

        _classCallCheck(this, WalletPage);

        this.transactionService = transactionService;
        this.translate = translate;
        this.driverService = driverService;
        this.common = common;
        this.alertCtrl = alertCtrl;
        this.currency = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["CURRENCY_SYMBOL"]; // get transactions from service

        transactionService.getTransactions().valueChanges().subscribe(function (snapshot) {
          if (snapshot != null) _this.records = snapshot.reverse();
        });
        this.driverService.getDriver().valueChanges().subscribe(function (snapshot) {
          _this.driver = snapshot;
        });
      }

      _createClass(WalletPage, [{
        key: "withdraw",
        value: function withdraw() {
          var _this2 = this;

          this.alertCtrl.create({
            header: "Make a Withdraw",
            inputs: [{
              name: 'amount',
              placeholder: 'Amount'
            }],
            buttons: [{
              text: 'Cancel',
              handler: function handler(data) {
                console.log('Cancel clicked');
              }
            }, {
              text: 'Submit',
              handler: function handler(data) {
                if (parseFloat(data.amount) > parseFloat(_this2.driver.balance)) {
                  _this2.common.showAlert("Insufficient Balance");
                } else {
                  _this2.transactionService.widthDraw(data.amount, _this2.driver.balance).then(function () {
                    _this2.common.showToast('withdraw Requested');
                  });
                }
              }
            }]
          }).then(function (x) {
            return x.present();
          });
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return WalletPage;
    }();

    WalletPage.ctorParameters = function () {
      return [{
        type: _services_transaction_service__WEBPACK_IMPORTED_MODULE_3__["TransactionService"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"]
      }, {
        type: _services_driver_service__WEBPACK_IMPORTED_MODULE_2__["DriverService"]
      }, {
        type: _services_common_service__WEBPACK_IMPORTED_MODULE_6__["CommonService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
      }];
    };

    WalletPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-wallet',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./wallet.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/wallet/wallet.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./wallet.page.scss */
      "./src/app/wallet/wallet.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_transaction_service__WEBPACK_IMPORTED_MODULE_3__["TransactionService"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"], _services_driver_service__WEBPACK_IMPORTED_MODULE_2__["DriverService"], _services_common_service__WEBPACK_IMPORTED_MODULE_6__["CommonService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]])], WalletPage);
    /***/
  }
}]);
//# sourceMappingURL=wallet-wallet-module-es5.js.map