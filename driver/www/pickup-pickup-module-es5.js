function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pickup-pickup-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pickup/pickup.page.html":
  /*!*******************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pickup/pickup.page.html ***!
    \*******************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPickupPickupPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-title>{{'RIDE_INFORMATION' | translate}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card>\n    <ion-card-content>\n      <ion-list lines=\"none\">\n        <ion-item *ngIf=\"passenger.phoneNumber\">\n          <ion-label>\n            <ion-text>\n              <h2>{{(passenger)?.first_name }} {{(passenger)?.last_name }}</h2>\n            </ion-text>\n            <ion-text>\n              <p>{{(passenger)?.phoneNumber }}</p>\n            </ion-text>\n          </ion-label>\n          <ion-button slot=\"end\" href=\"tel:{{passenger.phoneNumber}}\">\n            <ion-icon name=\"call\"></ion-icon>&nbsp; {{'CALL' | translate}}\n          </ion-button>\n          <ion-button color=\"dark\" slot=\"end\"  (click)=\"showChat()\">\n            <ion-icon name=\"chatbubbles\"></ion-icon>&nbsp;Message\n          </ion-button>\n\n        </ion-item>\n        <ion-item>\n          <ion-label>\n            <ion-text>\n              <ion-badge color=\"primary\" *ngIf=\"trip.status == 'accepted'\">Accepted</ion-badge>\n              <ion-badge color=\"secondary\" *ngIf=\"trip.status == 'going'\">Going</ion-badge>\n              <ion-badge color=\"success\" *ngIf=\"trip.status == 'finished'\">Completed</ion-badge>\n              <ion-badge color=\"danger\" *ngIf=\"trip.status == 'canceled'\">Canceled</ion-badge>\n              <ion-badge color=\"light\" *ngIf=\"trip.status == 'pending'\">Pending</ion-badge>\n              <ion-badge color=\"medium\" *ngIf=\"trip.status == 'waiting'\">Waiting</ion-badge>\n            </ion-text>\n          </ion-label>\n          <ion-label>\n            <ion-text>\n              <h2>{{'FROM' | translate}}</h2>\n            </ion-text>\n            <ion-text>\n              <p *ngIf=\"trip.origin != undefined\">{{ trip.origin.vicinity }}</p>\n            </ion-text>\n          </ion-label>\n          <ion-button slot=\"end\" fill=\"clear\" color=\"dark\"\n                      (click)=\"getDirection(trip.origin.location.lat,trip.origin.location.lng)\">\n            <ion-icon name=\"navigate\"></ion-icon>&nbsp; {{'NAVIGATE' | translate}}\n          </ion-button>\n        </ion-item>\n        <ion-item>\n          <ion-label>\n            <ion-text>\n              <h2>{{'TO' | translate}}</h2>\n            </ion-text>\n            <ion-text>\n              <p *ngIf=\"trip.destination != undefined\">{{ trip.destination.vicinity }}</p>\n              <p *ngIf=\"trip.destination != undefined\">{{ trip.locationDetails }}</p>\n            </ion-text>\n          </ion-label>\n          <ion-button slot=\"end\" fill=\"clear\" color=\"dark\"\n                      (click)=\"getDirection(trip.destination.location.lat,trip.destination.location.lng)\">\n            <ion-icon name=\"navigate\"></ion-icon>&nbsp; {{'NAVIGATE' | translate}}\n          </ion-button>\n        </ion-item>\n        <ion-item>\n          <ion-label>\n            <ion-text>\n              <h2>Delivery Information</h2>\n            </ion-text>\n            <ion-text>\n              <p>{{ trip.recipientName }}</p>\n              <p>{{ trip.mobileNumber }}</p>\n              <p>{{ trip.itemCategory }}</p>\n            </ion-text>\n          </ion-label>\n          <ion-button slot=\"end\" href=\"tel:{{trip.mobileNumber}}\" [hidden]=\"!isTripStarted\">\n            <ion-icon name=\"call\"></ion-icon>&nbsp; {{'CALL' | translate}}\n          </ion-button>\n        </ion-item>\n        <ion-item>\n          <ion-label>\n            <img [src]=\"trip.imageUrl\"  *ngIf=\"trip.imageUrl\"/>\n          </ion-label>\n          <ion-button slot=\"end\" (click)=\"upload()\" color=\"success\">\n            <ion-icon name=\"camera\"></ion-icon>&nbsp; Take a Picture\n          </ion-button>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n  <ion-badge>{{ trip.payment_method }}</ion-badge>\n</ion-content>\n<ion-footer>\n  <ion-toolbar>\n    <ion-button expand=\"block\" color=\"dark\" [hidden]=\"isTripStarted\" (click)=\"pickup()\">{{'PICKUP' | translate}}\n    </ion-button>\n    <ion-button expand=\"block\" color=\"danger\" [hidden]=\"!isTripStarted\" (click)=\"showPayment()\">\n      {{'COMPLETE_RIDE' | translate}}</ion-button>\n  </ion-toolbar>\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/pickup/pickup.module.ts":
  /*!*****************************************!*\
    !*** ./src/app/pickup/pickup.module.ts ***!
    \*****************************************/

  /*! exports provided: PickupPageModule */

  /***/
  function srcAppPickupPickupModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PickupPageModule", function () {
      return PickupPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _pickup_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./pickup.page */
    "./src/app/pickup/pickup.page.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");

    var routes = [{
      path: '',
      component: _pickup_page__WEBPACK_IMPORTED_MODULE_6__["PickupPage"]
    }];

    var PickupPageModule = function PickupPageModule() {
      _classCallCheck(this, PickupPageModule);
    };

    PickupPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_pickup_page__WEBPACK_IMPORTED_MODULE_6__["PickupPage"]]
    })], PickupPageModule);
    /***/
  },

  /***/
  "./src/app/pickup/pickup.page.scss":
  /*!*****************************************!*\
    !*** ./src/app/pickup/pickup.page.scss ***!
    \*****************************************/

  /*! exports provided: default */

  /***/
  function srcAppPickupPickupPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "h2 {\n  font-weight: bolder;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGlja3VwL0M6XFxVc2Vyc1xcSmVyaWNvIFBhdWxvXFxSdWJ5bWluZVByb2plY3RzXFx0bnZzXFxkcml2ZXIvc3JjXFxhcHBcXHBpY2t1cFxccGlja3VwLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGlja3VwL3BpY2t1cC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxtQkFBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvcGlja3VwL3BpY2t1cC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoMiB7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcclxufSIsImgyIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pickup/pickup.page.ts":
  /*!***************************************!*\
    !*** ./src/app/pickup/pickup.page.ts ***!
    \***************************************/

  /*! exports provided: PickupPage */

  /***/
  function srcAppPickupPickupPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PickupPage", function () {
      return PickupPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _services_trip_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../services/trip.service */
    "./src/app/services/trip.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_deal_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../services/deal.service */
    "./src/app/services/deal.service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _services_common_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../services/common.service */
    "./src/app/services/common.service.ts");
    /* harmony import */


    var _angular_fire_database__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/fire/database */
    "./node_modules/@angular/fire/database/es2015/index.js");
    /* harmony import */


    var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @ionic-native/geolocation/ngx */
    "./node_modules/@ionic-native/geolocation/ngx/index.js");
    /* harmony import */


    var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../../environments/environment.prod */
    "./src/environments/environment.prod.ts");
    /* harmony import */


    var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @ionic-native/camera/ngx */
    "./node_modules/@ionic-native/camera/ngx/index.js");
    /* harmony import */


    var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @angular/fire/storage */
    "./node_modules/@angular/fire/storage/es2015/index.js");

    var PickupPage =
    /*#__PURE__*/
    function () {
      function PickupPage(tripDriverService, alertCtrl, dealService, router, common, translate, db, menuCtrl, geolocation, camera, afStorage) {
        _classCallCheck(this, PickupPage);

        this.tripDriverService = tripDriverService;
        this.alertCtrl = alertCtrl;
        this.dealService = dealService;
        this.router = router;
        this.common = common;
        this.translate = translate;
        this.db = db;
        this.menuCtrl = menuCtrl;
        this.geolocation = geolocation;
        this.camera = camera;
        this.afStorage = afStorage;
        this.trip = {};
        this.passenger = {};
        this.isTripStarted = false;
        this.menuCtrl.enable(true);
      }

      _createClass(PickupPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          var _this = this;

          this.trip = this.tripDriverService.getCurrentTrip();

          if (!this.trip.key) {
            this.tripDriverService.getTrips().valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1)).subscribe(function (trips) {
              trips.forEach(function (trip) {
                if (trip.status == _environments_environment_prod__WEBPACK_IMPORTED_MODULE_11__["TRIP_STATUS_WAITING"] || trip.status == _environments_environment_prod__WEBPACK_IMPORTED_MODULE_11__["TRIP_STATUS_GOING"]) {
                  _this.tripDriverService.setCurrentTrip(trip.key);

                  _this.trip = _this.tripDriverService.getCurrentTrip();
                }
              });
            });
          }

          var getTrips = this.tripDriverService.getTripStatus(this.trip.key).valueChanges().subscribe(function (trip) {
            if (trip.status == 'canceled') {
              getTrips.unsubscribe();

              _this.tripDriverService.cancel(_this.trip.key);

              _this.dealService.removeDeal(_this.trip.driverId);

              _this.common.showAlert("Trip Cancelled");

              _this.router.navigateByUrl('/home');
            }
          });
          this.tripDriverService.getPassenger(this.trip.passengerId).valueChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1)).subscribe(function (snapshot) {
            _this.passenger = snapshot;
          });
        } // pickup

      }, {
        key: "pickup",
        value: function pickup() {
          this.isTripStarted = true;
          this.tripDriverService.pickUp(this.trip.key); // this.alertCtrl.create({
          //     subHeader: "Please Enter OTP from customer",
          //     inputs: [{
          //         name: 'otp',
          //         placeholder: '4 digit OTP'
          //     }],
          //     buttons: [{
          //         text: "Verify",
          //         handler: (data) => {
          //             this.db.object('trips/' + this.trip.key).valueChanges().pipe(take(1)).subscribe((res: any) => {
          //                 if (res.otp != data.otp) this.common.showAlert("Invalid OTP");
          //                 else {
          //                     this.isTripStarted = true;
          //                     this.tripDriverService.pickUp(this.trip.key);
          //                 }
          //             })
          //         }
          //     }]
          // }).then(res => res.present());
        }
      }, {
        key: "getDirection",
        value: function getDirection(lat, lng) {
          this.geolocation.getCurrentPosition().then(function (geo) {
            geo.coords.latitude;
            var url = "https://www.google.com/maps/dir/?api=1&travelmode=driving&origin=" + geo.coords.latitude + "," + geo.coords.longitude + "&destination=" + lat + "," + lng;
            window.open(url);
          }).catch(function (err) {
            console.log("Error ");
          });
        }
      }, {
        key: "showPayment",
        value: function showPayment() {
          var _this2 = this;

          var final = this.trip.fee - this.trip.fee * (parseInt(this.trip.discount) / 100);
          this.alertCtrl.create({
            message: '<h1>' + this.trip.currency + ' ' + final + '</h1> <p>Fee: ' + this.trip.fee + ' <br> Discount (%): ' + this.trip.discount + ' (' + this.trip.promocode + ')<br>Payment Method: ' + this.trip.paymentMethod + '</p>',
            buttons: [{
              text: 'OK',
              handler: function handler() {
                _this2.tripDriverService.dropOff(_this2.trip.key);

                _this2.dealService.removeDeal(_this2.trip.driverId);

                _this2.router.navigateByUrl('/home');
              }
            }]
          }).then(function (res) {
            return res.present();
          });
        }
      }, {
        key: "showChat",
        value: function showChat() {
          this.router.navigateByUrl('/chat');
        }
      }, {
        key: "upload",
        value: function upload() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee() {
            var options, base64, blob, tempFilename, path;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    options = {
                      quality: 100,
                      destinationType: this.camera.DestinationType.FILE_URI,
                      encodingType: this.camera.EncodingType.JPEG,
                      mediaType: this.camera.MediaType.PICTURE
                    };
                    _context.next = 3;
                    return this.getPicture();

                  case 3:
                    base64 = _context.sent;
                    blob = this.b64toBlob(base64, 'image/jpeg', 512);
                    tempFilename = base64.substr(base64.lastIndexOf('/') + 1);
                    path = 'trips/' + Date.now() + "_".concat(tempFilename);
                    this.captureDataUrl = this.uploadToFirebase(blob, path);

                  case 8:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        }
      }, {
        key: "getPicture",
        value: function getPicture() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0,
          /*#__PURE__*/
          regeneratorRuntime.mark(function _callee2() {
            var options, base64;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    options = {
                      quality: 30,
                      destinationType: this.camera.DestinationType.DATA_URL,
                      encodingType: this.camera.EncodingType.JPEG,
                      mediaType: this.camera.MediaType.PICTURE,
                      targetWidth: 200
                    };
                    _context2.next = 3;
                    return this.camera.getPicture(options);

                  case 3:
                    base64 = _context2.sent;
                    return _context2.abrupt("return", base64);

                  case 5:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        } //https://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript

      }, {
        key: "b64toBlob",
        value: function b64toBlob(b64Data, contentType, sliceSize) {
          contentType = contentType || '';
          sliceSize = sliceSize || 512;
          var byteCharacters = atob(b64Data);
          var byteArrays = [];

          for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);

            for (var i = 0; i < slice.length; i++) {
              byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
          }

          var blob = new Blob(byteArrays, {
            type: contentType
          });
          return blob;
        }
      }, {
        key: "uploadToFirebase",
        value: function uploadToFirebase(blob, path) {
          var _this3 = this;

          var ref = this.afStorage.ref(path);
          var imageUrl = "";
          ref.put(blob).then(function () {
            ref.getDownloadURL().pipe().subscribe(function (data) {
              _this3.trip.imageUrl = data;

              _this3.updateTripImageUrl();
            });
          });
          console.log("imageUrl: " + imageUrl);
          return imageUrl;
        }
      }, {
        key: "updateTripImageUrl",
        value: function updateTripImageUrl() {
          var _this4 = this;

          this.db.object('trips/' + this.trip.key).update(this.trip).then(function (data) {
            _this4.common.showToast("Updated successfully");
          });
        }
      }]);

      return PickupPage;
    }();

    PickupPage.ctorParameters = function () {
      return [{
        type: _services_trip_service__WEBPACK_IMPORTED_MODULE_3__["TripService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
      }, {
        type: _services_deal_service__WEBPACK_IMPORTED_MODULE_5__["DealService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
      }, {
        type: _services_common_service__WEBPACK_IMPORTED_MODULE_8__["CommonService"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"]
      }, {
        type: _angular_fire_database__WEBPACK_IMPORTED_MODULE_9__["AngularFireDatabase"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"]
      }, {
        type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__["Geolocation"]
      }, {
        type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_12__["Camera"]
      }, {
        type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_13__["AngularFireStorage"]
      }];
    };

    PickupPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-pickup',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./pickup.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pickup/pickup.page.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./pickup.page.scss */
      "./src/app/pickup/pickup.page.scss")).default]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_trip_service__WEBPACK_IMPORTED_MODULE_3__["TripService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"], _services_deal_service__WEBPACK_IMPORTED_MODULE_5__["DealService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _services_common_service__WEBPACK_IMPORTED_MODULE_8__["CommonService"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"], _angular_fire_database__WEBPACK_IMPORTED_MODULE_9__["AngularFireDatabase"], _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["MenuController"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__["Geolocation"], _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_12__["Camera"], _angular_fire_storage__WEBPACK_IMPORTED_MODULE_13__["AngularFireStorage"]])], PickupPage);
    /***/
  }
}]);
//# sourceMappingURL=pickup-pickup-module-es5.js.map