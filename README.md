# Transportation Network Vehicle Service (TNVS)

## Libraries/Platforms Used

* ruby 2.6.0p0
* Rails 5.2.3
* Gem 2.7.9
* MySQL 14.14
* Ionic v4: (https://ionicframework.com/docs/installation/cli)

## Installation

##### Admin
* Clone this project repo.
* Go to the project directory (tnvs/admin) via console/terminal
* Type 'bundle install'
* Then run 'rails s'


##### Mobile
* Go to the project directory (tnvs/mobile) via console/terminal
* npm install -g ionic
* npm install
* ionic serve
* Go to chrome browser: (http://localhost:8100) 

###### Generate Splash & Icon
* ionic cordova resources

###### Generate APK (Android)
* Debug: ionic cordova build android --prod
* Release: ionic cordova build android --prod --release

## Deploying in Heroku

* git subtree push --prefix admin heroku master
